<?php

Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/admin', 'AdminController@admin');

Route::get(__('/editar-perfil'), 'UsuariosController@perfil')->name('usuario.perfil');
Route::post(__('/editar-perfil'), 'UsuariosController@update')->name('usuario.update');
Route::post(__('/perfil_update_password'), 'UsuariosController@update_password')->name('usuario.update_password');
Route::post(__('/perfil_update_avatar'), 'UsuariosController@update_avatar')->name('usuario.update_avatar');

/* Languages */
Route::get('languages', 'LanguageTranslationController@index')->name('languages');
Route::post('translations/update', 'LanguageTranslationController@transUpdate')->name('translation.update.json');
Route::post('translations/updateKey', 'LanguageTranslationController@transUpdateKey')->name('translation.update.json.key');
Route::delete('translations/destroy/{key}', 'LanguageTranslationController@destroy')->name('translations.destroy');
Route::post('translations/create', 'LanguageTranslationController@store')->name('translations.create');

//Route::resource('calendari', 'CalendariController');
Route::get(__('/calendari'), 'CalendariController@index')->name('calendari.index');
Route::post(__('/calendari') . '/store', 'CalendariController@store')->name('calendari.store');

Route::resource('calendario-mensajes', 'CalendarioMensajesController');

//Route::resource('contactar', 'MensajesController');
Route::get(__('/contact'), 'MensajesController@index')->name('contactar.index');
Route::post('/contact/store', 'MensajesController@store')->name('contactar.store');