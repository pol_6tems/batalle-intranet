<?php

Route::post('opciones/updateValue', 'AdminOpcionesController@updateValue')->name('opciones.update_value');
Route::post('opciones/updateKey', 'AdminOpcionesController@updateKey')->name('opciones.update_key');
Route::post('opciones/updateDesc', 'AdminOpcionesController@updateDesc')->name('opciones.update_desc');
Route::resource('opciones', 'AdminOpcionesController');


Route::resource('/configuracion/menus', 'AdminMenusController');
Route::resource('/configuracion/librerias', 'AdminLibreriasController');
Route::resource('calendario-vacaciones', 'AdminCalendarioVacacionesController');

// Usuarios
//Route::get('perfil', 'AdminUsuariosController@perfil');
//Route::post('perfil', 'AdminUsuariosController@update_avatar');
Route::post('configuracion/usuarios/years/update/{id}', 'AdminUsuariosController@years_update')->name('configuracion.usuarios.years.update');
Route::post('configuracion/usuarios/years/store', 'AdminUsuariosController@years_store')->name('configuracion.usuarios.years.store');
Route::get('configuracion/usuarios/years/edit/{id}', 'AdminUsuariosController@years_edit')->name('configuracion.usuarios.years.edit');
Route::delete('configuracion/usuarios/years/destroy/{id}/{user_id}', 'AdminUsuariosController@years_destroy')->name('configuracion.usuarios.years.destroy');
Route::get('configuracion/usuarios/years/create/{calendario_nivel_id}', 'AdminUsuariosController@years_create')->name('configuracion.usuarios.years.create');
Route::get('configuracion/usuarios/exportar', 'AdminUsuariosController@exportar')->name('configuracion.usuarios.exportar');
Route::resource('/configuracion/usuarios', 'AdminUsuariosController');

// Posts
Route::put('posts/{id}/restore', 'PostsController@restore')->name('posts.restore');
Route::delete('posts/{id}/destroy_permanent', 'PostsController@destroy_permanent')->name('posts.destroy_permanent');
Route::delete('posts/mass_destroy', 'PostsController@massDestroy')->name('posts.mass_destroy');
Route::resource('posts', 'PostsController');

// Empleados
Route::delete('empleados/mass_destroy', 'AdminEmpleadosController@massDestroy')->name('empleados.mass_destroy');
Route::post('empleados/importar', 'AdminEmpleadosController@importar')->name('empleados.importar');
Route::resource('empleados/categorias', 'CategoriasEmpleadoController');
Route::resource('empleados', 'AdminEmpleadosController');

Route::post('mensajes/visto', 'AdminMensajesController@visto')->name('mensajes.visto');
Route::resource('mensajes', 'AdminMensajesController');