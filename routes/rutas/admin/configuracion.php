<?php

// Rols
Route::resource('rols', 'AdminRolsController');

// Menus
Route::resource('menus', 'AdminMenusController');
// Menus - Submenus
Route::get('menus/subs/create/{menu_id}', 'AdminMenusController@submenu_create')->name('menus.subs.create');
Route::post('menus/subs/store', 'AdminMenusController@submenu_store')->name('menus.subs.store');
Route::get('menus/subs/edit/{id}', 'AdminMenusController@submenu_edit')->name('menus.subs.edit');
Route::post('menus/subs/update/{id}', 'AdminMenusController@submenu_update')->name('menus.subs.update');
Route::delete('menus/subs/destroy/{id}', 'AdminMenusController@submenu_destroy')->name('menus.subs.destroy');