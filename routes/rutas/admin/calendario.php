<?php

// Niveles - Years
Route::get('niveles/years/create/{calendario_nivel_id}', 'AdminCalendarioNivelesController@years_create')->name('niveles.years.create');
Route::post('niveles/years/store', 'AdminCalendarioNivelesController@years_store')->name('niveles.years.store');
Route::get('niveles/years/edit/{id}', 'AdminCalendarioNivelesController@years_edit')->name('niveles.years.edit');
Route::post('niveles/years/update/{id}', 'AdminCalendarioNivelesController@years_update')->name('niveles.years.update');
// Niveles - Weeks
Route::get('niveles/weeks/create/{calendario_nivel_id}', 'AdminCalendarioNivelesController@weeks_create')->name('niveles.weeks.create');
Route::post('niveles/weeks/store', 'AdminCalendarioNivelesController@weeks_store')->name('niveles.weeks.store');
Route::get('niveles/weeks/edit/{id}', 'AdminCalendarioNivelesController@weeks_edit')->name('niveles.weeks.edit');
Route::post('niveles/weeks/update/{id}', 'AdminCalendarioNivelesController@weeks_update')->name('niveles.weeks.update');
// Niveles - Encarregat
Route::get('niveles/encarregat/create/{calendario_nivel_id}', 'AdminCalendarioNivelesController@encarregat_create')->name('niveles.encarregat.create');
Route::post('niveles/encarregat/store', 'AdminCalendarioNivelesController@encarregat_store')->name('niveles.encarregat.store');
Route::get('niveles/encarregat/edit/{id}', 'AdminCalendarioNivelesController@encarregat_edit')->name('niveles.encarregat.edit');
Route::post('niveles/encarregat/update/{id}', 'AdminCalendarioNivelesController@encarregat_update')->name('niveles.encarregat.update');
Route::delete('niveles/encarregat/destroy/{id}', 'AdminCalendarioNivelesController@encarregat_destroy')->name('niveles.encarregat.destroy');

Route::delete('niveles/mass_destroy', 'AdminCalendarioNivelesController@massDestroy')->name('niveles.mass_destroy');
Route::resource('niveles', 'AdminCalendarioNivelesController');
// SubNiveles - Years
Route::get('subniveles/years/create/{calendario_nivel_id}', 'AdminCalendarioSubnivelesController@years_create')->name('subniveles.years.create');
Route::post('subniveles/years/store', 'AdminCalendarioSubnivelesController@years_store')->name('subniveles.years.store');
Route::get('subniveles/years/edit/{id}', 'AdminCalendarioSubnivelesController@years_edit')->name('subniveles.years.edit');
Route::post('subniveles/years/update/{id}', 'AdminCalendarioSubnivelesController@years_update')->name('subniveles.years.update');
// SubNiveles - Weeks
Route::get('subniveles/weeks/create/{calendario_nivel_id}', 'AdminCalendarioSubnivelesController@weeks_create')->name('subniveles.weeks.create');
Route::post('subniveles/weeks/store', 'AdminCalendarioSubnivelesController@weeks_store')->name('subniveles.weeks.store');
Route::get('subniveles/weeks/edit/{id}', 'AdminCalendarioSubnivelesController@weeks_edit')->name('subniveles.weeks.edit');
Route::post('subniveles/weeks/update/{id}', 'AdminCalendarioSubnivelesController@weeks_update')->name('subniveles.weeks.update');

Route::delete('subniveles/mass_destroy', 'AdminCalendarioSubnivelesController@massDestroy')->name('subniveles.mass_destroy');
Route::resource('subniveles', 'AdminCalendarioSubnivelesController');
// Festivos
Route::resource('festivos', 'AdminCalendarioFestivosController');
//Vetados
Route::resource('vetados', 'AdminCalendarioVetadosController');

// Mensajes
Route::post('mensajes/visto', 'AdminCalendarioMensajesController@visto')->name('mensajes.visto');
Route::get('mensajes/contestar/{id}', 'AdminCalendarioMensajesController@contestar')->name('mensajes.contestar');
Route::resource('mensajes', 'AdminCalendarioMensajesController');