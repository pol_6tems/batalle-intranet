<?php

Route::get(__('/access'),'AccesController@index')->name('acces.index');
Route::get(__('/check_dni'),'AccesController@check_dni')->name('acces.dni');
Route::get(__('/register'),'AccesController@registre')->name('acces.registre');
Route::get(__('/welcome'),'AccesController@benvingut')->name('acces.benvingut');
Route::post(__('/create_user'),'AccesController@crear_user')->name('acces.crear_user');
Route::get(__('/politica-de-privacitat'), 'HomeController@politica_de_privacitat');