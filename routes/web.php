<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
/*
Route::get('/', function () {
    return view('welcome');
});
*/

// Busca el locale a partir de les traduccions per saber l'idioma de la traducció de la URL
$locales = config('app.locales');

$url_trobada = false;
if (strpos(\Request::getPathInfo(), '/locale/') === false) {
    foreach ( $locales as $locale_aux ) {
        if ( file_exists("../resources/lang/$locale_aux.json") ) {
            $translations = json_decode(file_get_contents("../resources/lang/$locale_aux.json"), true);
            if ( !empty($translations) ) {
                $key = array_search(Request::getPathInfo(), $translations);
                if ( !empty($key) ) {
                    app()->setLocale($locale_aux);
                    $url_trobada = true;
                    break;
                }
            }
        }
    }
}

/* SHOW POSTS BY post_name */
if ( !$url_trobada ) {
    $post = \DB::table('post_translations')->where('post_name', \Request::getPathInfo())->first();
    if ( !empty($post) ) {
        Route::get(\Request::getPathInfo(), array('as' => 'show', function(){
            return App::call('App\Http\Controllers\PostsController@show', ['post_name' => \Request::getPathInfo()] );
        }));
    }
}
/* end SHOW POSTS BY post_name */

Route::get('locale/{locale?}', array('as'=>'set-locale', 'uses'=>'LanguageController@setLocale'));

Route::group(['middleware' => ['web']], function () {
    include('rutas/no_auth.php');
});

Route::group(['middleware' => ['auth', 'web']], function () {
    include('rutas/auth.php');
});

// Admin
Route::group(['middleware' => ['admin', 'can_access'], 'prefix' => 'admin'], function () {
    include('rutas/admin.php');

    // Admin Calendario
    Route::group(['prefix' => 'calendario', 'as' => 'calendario.'], function () {
        include('rutas/admin/calendario.php');
    });

    // Admin Configuración
    Route::group(['prefix' => 'configuracion', 'as' => 'configuracion.'], function () {
        include('rutas/admin/configuracion.php');
    });
});

Auth::routes();