<?php

use Illuminate\Database\Seeder;

class CreateCategoriasEmpleado extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $table = 'categorias_empleado';
        \DB::table($table)->insert(['id' => 0,'nombre' => 'Sin categoría']);
    }
}
