<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('posts') ) {
            Schema::create('posts', function (Blueprint $table) {
                $table->increments('id');
                $table->string('type');
                $table->string('status');
                $table->timestamps();
                $table->softDeletes();
            });
        }
        if ( !Schema::hasTable('post_translations') ) {
            Schema::create('post_translations', function(Blueprint $table)
            {
                $table->increments('id');
                $table->integer('post_id')->unsigned();
                $table->string('title')->nullable();
                $table->text('description')->nullable();
                $table->string('post_name')->nullable();
                $table->string('locale')->index();

                $table->unique(['post_id','locale']);
                $table->foreign('post_id')->references('id')->on('posts')->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('post_translations');
        Schema::dropIfExists('posts');
    }
}
