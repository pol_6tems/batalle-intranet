<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarioFestivosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_festivos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('year')->unsigned()->default( date("Y") );
            $table->string('days')->nullable();
            $table->integer('calendario_nivel_id')->unsigned()->default( 0 );
            $table->integer('calendario_subnivel_id')->unsigned()->default( 0 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_festivos');
    }
}
