<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxLimitToCalendarioNiveles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendario_niveles', function (Blueprint $table) {
            $table->integer('max_limit')->unsigned()->default( 100 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendario_niveles', function (Blueprint $table) {
            $table->dropColumn('max_limit');
        });
    }
}
