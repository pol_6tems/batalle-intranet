<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddVistoToCalMensajes extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendario_mensajes', function (Blueprint $table) {
            $table->boolean('visto')->default( false );
        });

        Schema::table('mensajes', function (Blueprint $table) {
            $table->boolean('visto')->default( false );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendario_mensajes', function (Blueprint $table) {
            $table->dropColumn('visto');
        });

        Schema::table('mensajes', function (Blueprint $table) {
            $table->dropColumn('visto');
        });
    }
}
