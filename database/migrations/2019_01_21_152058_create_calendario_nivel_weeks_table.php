<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarioNivelWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_nivel_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendario_nivel_id')->unsigned();
            $table->integer('year')->unsigned()->default( date("Y") );
            $table->integer('week')->unsigned()->default( date("W") );
            $table->integer('max_limit')->unsigned()->default( 100 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_nivel_weeks');
    }
}
