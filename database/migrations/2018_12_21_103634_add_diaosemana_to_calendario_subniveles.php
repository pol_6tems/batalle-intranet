<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddDiaosemanaToCalendarioSubniveles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendario_subniveles', function (Blueprint $table) {
            $table->string('sel_dia_o_semana')->default('dia');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendario_subniveles', function (Blueprint $table) {
            $table->dropColumn('sel_dia_o_semana');
        });
    }
}
