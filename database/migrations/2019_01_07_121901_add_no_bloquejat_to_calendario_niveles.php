<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddNoBloquejatToCalendarioNiveles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendario_niveles', function (Blueprint $table) {
            $table->integer('no_bloquejat')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendario_niveles', function (Blueprint $table) {
            $table->dropColumn('no_bloquejat');
        });
    }
}
