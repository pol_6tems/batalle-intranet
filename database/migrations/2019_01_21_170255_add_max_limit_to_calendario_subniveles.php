<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddMaxLimitToCalendarioSubniveles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('calendario_subniveles', function (Blueprint $table) {
            $table->integer('max_limit')->unsigned()->default( 100 );
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('calendario_subniveles', function (Blueprint $table) {
            $table->dropColumn('max_limit');
        });
    }
}
