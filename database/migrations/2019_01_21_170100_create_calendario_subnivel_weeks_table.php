<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCalendarioSubnivelWeeksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('calendario_subnivel_weeks', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('calendario_subnivel_id')->unsigned();
            $table->integer('year')->unsigned()->default( date("Y") );
            $table->integer('week')->unsigned()->default( date("W") );
            $table->integer('max_limit')->unsigned()->default( 100 );
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendario_subnivel_weeks');
    }
}
