<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoriasEmpleadoTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('categorias_empleado') ) {
            Schema::create('categorias_empleado', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre');
                $table->timestamps();
            });
            \DB::table('categorias_empleado')->insert(['id' => 0,'nombre' => 'Sin categoría']);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categorias_empleado');
    }
}
