<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMenusTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('rol_id')->unsigned();
            $table->string('name');
            $table->string('icon');
            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();
        });
        
        Schema::create('menus_sub', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('menu_id')->unsigned();
            $table->integer('rol_id')->unsigned();
            $table->string('name');
            $table->string('url_type');
            $table->string('url');
            $table->string('requests');
            $table->string('controller')->nullable();
            $table->integer('order')->unsigned()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('menus_sub');
        Schema::dropIfExists('menus');
    }
}
