<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateEmpleados extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if ( !Schema::hasTable('tablempleados_name') ) {
            Schema::create('empleados', function (Blueprint $table) {
                $table->increments('id');
                $table->string('nombre')->nullable();
                $table->date('fecha_alta')->nullable();
                $table->string('dni')->nullable();
                $table->string('sexo')->nullable();
                $table->date('fecha_nacimiento')->nullable();
                $table->date('fecha_baja')->nullable();
                $table->integer('categoria_id')->unsigned()->nullable();
                $table->integer('user_id')->unsigned()->nullable();
                $table->string('codigo')->nullable();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
