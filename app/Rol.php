<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Rol extends Model
{
    protected $table = 'rols';
    
    protected $fillable = ['name', 'level'];

    public function users()
    {
        return $this->hasMany(User::class, 'role', 'name');
    }
}
