<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoriasEmpleado extends Model
{
    protected $table = 'categorias_empleado';
	
    protected $fillable = [
        'id', 'nombre'
    ];
}
