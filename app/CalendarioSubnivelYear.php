<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioSubnivel;

class CalendarioSubnivelYear extends Model
{
    protected $fillable = ['calendario_subnivel_id', 'year', 'days'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }
}
