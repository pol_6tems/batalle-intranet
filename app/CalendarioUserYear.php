<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarioUserYear extends Model
{
    protected $table = 'calendario_users_years';
    protected $fillable = ['user_id', 'year', 'days'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
