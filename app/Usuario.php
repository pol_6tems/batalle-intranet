<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Usuario extends Model
{
    protected $table = 'users';
    public $timestamps = false;
	
    protected $fillable = [
        'name', 'email', 'password', 'user'
    ];
	
	protected $hidden = [
        'password', 'remember_token',
    ];

}
