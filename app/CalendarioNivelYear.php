<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioNivel;

class CalendarioNivelYear extends Model
{    
    protected $fillable = ['calendario_nivel_id', 'year', 'days'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }
}
