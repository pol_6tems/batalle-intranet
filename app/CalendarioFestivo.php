<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioFestivo;

class CalendarioFestivo extends Model
{
    protected $fillable = ['year', 'days', 'calendario_nivel_id', 'calendario_subnivel_id'];
}
