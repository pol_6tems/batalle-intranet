<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Notifications\MyResetPassword;
use App\CalendarioNivel;
use App\CalendarioSubnivel;
use App\CalendarioVacacion;
use App\Rol;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'calendario_nivel_id', 'calendario_subnivel_id',
        'lastname',
        'phone',
        'mobile',
        'address',
        'codigo_postal',
        'city',
        'idioma',
        'fecha_nacimiento',
        'lopd',
        'consentiment_naixement',
        'consentiment_privacitat',
        'role',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class, 'calendario_nivel_id');
    }
    
    public function subnivel()
    {
        return $this->belongsTo(CalendarioSubnivel::class, 'calendario_subnivel_id');
    }
    
    public function vacaciones()
    {
        return $this->hasMany(CalendarioVacacion::class, 'user_id');
    }
    
    public function empleado()
    {
        return $this->belongsTo(Empleado::class, 'dni', 'dni');
    }
    
    public function years()
    {
        return $this->hasMany(CalendarioUserYear::class);
    }
    
    public function mensajes()
    {
        return $this->hasMany(CalendarioMensaje::class, 'user_id');
    }
    
    public function contactar_mensajes()
    {
        return $this->hasMany(Mensaje::class, 'user_id');
    }
    
    public function fullname($first_lastname = false) {
        if ( $first_lastname )
            return ( !empty($this->lastname) ? $this->lastname . ', ' : '' ) . $this->name;
        else
            return $this->name . ( !empty($this->lastname) ? ' ' . $this->lastname : '' );
    }
    
    public function sendPasswordResetNotification($token)
    {
        $notificacio = new MyResetPassword($token);
        $notificacio->user_fullname = $this->fullname();
        $this->notify();
    }
    
    public function rol()
    {
        return $this->belongsTo(Rol::class, 'role', 'name');
    }
    
    public function isAdmin()
    {
        return $this->rol->name === 'admin';
    }
}
