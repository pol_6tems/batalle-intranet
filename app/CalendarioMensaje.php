<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CalendarioMensaje extends Model
{
    protected $table = 'calendario_mensajes';
    
    protected $fillable = ['user_id', 'from', 'mensaje', 'visto'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
