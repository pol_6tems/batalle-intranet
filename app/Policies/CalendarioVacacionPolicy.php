<?php

namespace App\Policies;

use App\User;
use App\CalendarioVacacion;
use Illuminate\Auth\Access\HandlesAuthorization;

class CalendarioVacacionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the post.
     *
     * @param  \App\User  $user
     * @param  \App\CalendarioVacacion  $item
     * @return mixed
     */
    public function view(User $user, CalendarioVacacion $item)
    {
        //
    }

    /**
     * Determine whether the user can create posts.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return $user->role == 'admin';
    }

    /**
     * Determine whether the user can update the post.
     *
     * @param  \App\User  $user
     * @param  \App\CalendarioVacacion  $item
     * @return mixed
     */
    public function update(User $user)
    {
        return $user->role == 'admin';
    }

    /**
     * Determine whether the user can delete the post.
     *
     * @param  \App\User  $user
     * @param  \App\CalendarioVacacion  $item
     * @return mixed
     */
    public function delete(User $user)
    {
        return $user->role == 'admin';
    }
}
