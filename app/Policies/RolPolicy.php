<?php

namespace App\Policies;

use Auth;
use App\User;
use App\Rol;
use App\Menu;
use App\MenuSub;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolPolicy
{
    use HandlesAuthorization;

    public function view(User $user)
    {
        $as = str_replace('App\\Http\\Controllers\\','', request()->route()->getAction()['controller']);
        $pos = strpos($as, '@');
        if ( $pos > 0 ) $as = substr($as, 0, $pos);
        //echo $as;die();
        return MenuSub::whereHas('rol', function ($q) {
			$q->where('level', '<=', Auth::user()->rol->level);
		})->where('controller', '=', $as)->exists();
    }
}
