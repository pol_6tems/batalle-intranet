<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioNivel;

class CalendarioNivelWeek extends Model
{
    protected $fillable = ['calendario_nivel_id', 'year', 'week', 'max_limit'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }
}
