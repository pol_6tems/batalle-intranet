<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Rol;

class Menu extends Model
{
    protected $table = 'menus';
    
    protected $fillable = ['rol_id', 'name', 'icon', 'order'];

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'rol_id', 'id');
    }

    public function submenus()
    {
        return $this->hasMany(MenuSub::class, 'menu_id', 'id')->orderBy('order');
    }
}
