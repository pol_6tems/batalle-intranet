<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\CalendarioNivel;
use App\CalendarioSubnivelYear;
use App\User;
use App\CalendarioSubnivelWeek;

class CalendarioSubnivel extends Model
{
    use SoftDeletes;

    protected $table = 'calendario_subniveles';
    
    protected $fillable = ['name', 'status', 'calendario_nivel_id', 'sel_dia_o_semana', 'no_bloquejat', 'max_limit'];
    
    protected $dates = ['deleted_at'];

    public function years()
    {
        return $this->hasMany(CalendarioSubnivelYear::class);
    }

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class, 'calendario_nivel_id');
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function weeks()
    {
        return $this->hasMany(CalendarioSubnivelWeek::class);
    }

    public function get_persones_max()
    {
        $persones_max = array('general' => $this->max_limit);
        if ( !empty($this->weeks) ) {
            foreach ($this->weeks as $item) {
                $persones_max[$item->year . '-' . $item->week] = $item->max_limit;
            }
        }
        return $persones_max;
    }
}
