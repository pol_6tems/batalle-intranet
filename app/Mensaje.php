<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mensaje extends Model
{    
    protected $fillable = ['user_id', 'from', 'mensaje', 'fitxer', 'visto'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function from()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
