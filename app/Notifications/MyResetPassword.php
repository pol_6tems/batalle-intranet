<?php

namespace App\Notifications;

use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Auth\Notifications\ResetPassword;

class MyResetPassword extends ResetPassword
{
    public $user_fullname;

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject( __('Reset the password') )
            ->greeting( __('Dear') . $this->user_fullname . ',' )
            ->line( __('reset_password_mail_text') )
            ->action( __('Reset the password'), route('password.reset', $this->token) )
            ->salutation( __('Regards') . ',<br>'. config('app.name') );
    }
}
