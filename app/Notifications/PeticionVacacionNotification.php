<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class PeticionVacacionNotification extends Notification
{
    use Queueable;

    protected $usuario;
    protected $peticiones;
    protected $isFront;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario, $peticiones, $isFront)
    {
        $this->usuario = $usuario;
        $this->peticiones = $peticiones;
        $this->isFront = $isFront;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $message = new MailMessage;
        if ( $this->isFront ) {
            $message->subject( __('New holiday request') );
            $message->greeting( __('Welcome') . ' ' . $this->usuario->fullname() . ',' );
            $message->line( __('Thank you for sending your holiday request shortly. Batallé will review and respond to your request.') );
            $message->line( __('The requested days are') . ':' );
            foreach ($this->peticiones as $item) {
                $start_str = \Carbon\Carbon::parse($item->start)->format('d/m/Y');
                $end_str = \Carbon\Carbon::parse($item->end)->format('d/m/Y');
                $start = strtotime($item->start);
                $end = strtotime($item->end);
                $diff = $end - $start;
                if ( $diff > 1 ) $message->line( $start_str );
                else $message->line( __('From') . ' ' . $start_str .  __('to') . ' ' . $end_str);
            }
        } else {
            $message->subject( __('Response to the vacation request') );
            $message->greeting( __('Welcome') . ' ' . $this->usuario->fullname() . ',' );
            // Aprovats
            $hi_han_aprovats = false;
            foreach ($this->peticiones as $item) {
                if ( $item->status == 'approved' ) {
                    $hi_han_aprovats = true;
                    break;
                }
            }
            if ( $hi_han_aprovats ) {
                $message->line( __('The following days have been approved') . ':' );
                foreach ($this->peticiones as $item) {
                    if ( $item->status == 'approved' ) {
                        $start_str = \Carbon\Carbon::parse($item->start)->format('d/m/Y');
                        $end_str = \Carbon\Carbon::parse($item->end)->format('d/m/Y');
                        $start = strtotime($item->start);
                        $end = strtotime($item->end);
                        $diff = $end - $start;
                        if ( $diff > 1 ) $message->line( $start_str );
                        else $message->line( __('From') . ' ' . $start_str .  __('to') . ' ' . $end_str);
                    }
                }
            }
            // Denegats
            $hi_han_denegats = false;
            foreach ($this->peticiones as $item) {
                if ( $item->status == 'denied' ) {
                    $hi_han_denegats = true;
                    break;
                }
            }
            if ( $hi_han_denegats ) {
                $message->line( __('The following days have been denied') . ':' );
                foreach ($this->peticiones as $item) {
                    if ( $item->status == 'denied' ) {
                        $start_str = \Carbon\Carbon::parse($item->start)->format('d/m/Y');
                        $end_str = \Carbon\Carbon::parse($item->end)->format('d/m/Y');
                        $start = strtotime($item->start);
                        $end = strtotime($item->end);
                        $diff = $end - $start;
                        if ( $diff > 1 ) $message->line( $start_str );
                        else $message->line( __('From') . ' ' . $start_str .  __('to') . ' ' . $end_str);
                    }
                }
            }
        }
        
        $message->salutation( __('Regards') . ',<br>'. config('app.name') );
        
        return $message;
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
