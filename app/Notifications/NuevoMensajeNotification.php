<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevoMensajeNotification extends Notification
{
    use Queueable;

    protected $usuario;
    protected $mensaje;
    protected $from;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario, $mensaje, $from = null)
    {
        $this->usuario = $usuario;
        $this->mensaje = $mensaje;
        $this->from = $from;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        $from_str = '';
        $from = $this->from;
        if ( !empty($from) ) {
            $from_str = $from->fullname();
        } else if ( !empty( $this->mensaje->from() ) && !empty( $this->mensaje->from()->first() ) ) {
            $from_str =$this->mensaje->from()->first()->fullname();
        }

        $url = (empty($from)) ? route('mensajes.index') : route('contactar.index');

        return (new MailMessage)
            ->subject( __('New Message') )
            ->greeting( __('Welcome') . ' ' . $this->usuario->fullname() . ',' )
            ->line( __('There is a new message from') . ' ' . $from_str )
            ->line( __('Message') . ':' )
            ->line( $this->mensaje->mensaje )
            ->action( __('Review messages'), $url )
            ->salutation( __('Regards') . ',<br>'. config('app.name') );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
