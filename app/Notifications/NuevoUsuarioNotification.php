<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;

class NuevoUsuarioNotification extends Notification
{
    use Queueable;

    protected $usuario;
    protected $contrasenya;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($usuario, $contrasenya)
    {
        $this->usuario = $usuario;
        $this->contrasenya = $contrasenya;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject( __('Benvingut a Batallé - Àrea privada') )
            ->greeting( __('Welcome') . ' ' . $this->usuario->fullname() . ',' )
            ->line( __('email-benvingut-text') )
            ->line( __('email-benvingut-text-2') )
            ->line( __('User') . ': ' . $this->usuario->dni )
            ->line( __('Password') . ': ' . $this->contrasenya )
            ->action( __('Accedir'), route('login') )
            ->salutation( __('Regards') . ',<br>'. config('app.name') );
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
