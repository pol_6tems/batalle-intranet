<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioNivel;
use App\CategoriasEmpleado;
use App\User;

class Empleado extends Model
{
    protected $table = 'empleados';
	
    protected $fillable = [
        'id', 'nombre', 'fecha_alta', 'dni', 'sexo', 'fecha_nacimiento', 'fecha_baja', 
        'categoria_id', 'user_id', 'codigo', 'calendario_nivel_id'
    ];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class, 'calendario_nivel_id');
    }

    public function categoria()
    {
        return $this->belongsTo(CategoriasEmpleado::class, 'categoria_id');
    }
    
    public function usuario()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
    
}
