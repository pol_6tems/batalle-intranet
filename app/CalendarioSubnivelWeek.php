<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CalendarioNivel;

class CalendarioSubnivelWeek extends Model
{
    protected $fillable = ['calendario_subnivel_id', 'year', 'week', 'max_limit'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioSubnivel::class);
    }
}
