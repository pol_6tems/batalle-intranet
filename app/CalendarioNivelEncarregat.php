<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\CalendarioNivel;

class CalendarioNivelEncarregat extends Model
{
    protected $fillable = ['calendario_nivel_id', 'user_id'];

    public function nivel()
    {
        return $this->belongsTo(CalendarioNivel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
