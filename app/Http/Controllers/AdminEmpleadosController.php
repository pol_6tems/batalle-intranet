<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Empleado;
use App\CategoriasEmpleado;
use App\CalendarioNivel;
use App\User;
use App\Opcion;
use Validator;
use Auth;

class AdminEmpleadosController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortby = Input::get('sortby', 'codigo');
        $order = Input::get('order', 'ASC');
        $search = Input::get('search', '');
        if ( empty($search) ) {
            $empleados = Empleado::orderby($sortby, $order)->paginate(20);
        } else {
            $empleados = Empleado::where(function($q) use ($search) {
                $q->orWhere('dni', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('nombre', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('codigo', 'LIKE', '%'.strtoupper($search).'%');
            })->orderby($sortby, $order)->paginate(20);
        }            
        
        $links_appends = array('sortby' => $sortby, 'order' => $order);
        if ( !empty($search) ) $links_appends['search'] = $search;
        
        $links = $empleados->appends($links_appends)->links();
        
        return View('empleados.index', compact('empleados', 'links', 'sortby', 'order', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categorias = CategoriasEmpleado::get()->sortby('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $users = User::get()->sortby('id');
        return View('empleados.crear', compact('categorias', 'niveles', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $data = $request->all();
        Empleado::create($data);
        return redirect()->route('empleados.index')->with(['message' => __('Employee added successfully.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Empleado::find($id);
        $categoria = CategoriasEmpleado::where('id', $item->categoria_id)->first();
        if ( empty($categoria) ) {
            $categoria = new CategoriasEmpleado();
            $categoria->nombre = 'Sin categoría';
        }

        $item->categoria = $categoria;
        if ( !empty($item->fecha_nacimiento) ) $item->fecha_nacimiento = new \Carbon\Carbon($item->fecha_nacimiento); 
        if ( !empty($item->fecha_alta) ) $item->fecha_alta = new \Carbon\Carbon($item->fecha_alta); 
        if ( !empty($item->fecha_baja) ) $item->fecha_baja = new \Carbon\Carbon($item->fecha_baja); 
        return View('empleados.show', compact('item', $item));
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Empleado::find($id);
        $categorias = CategoriasEmpleado::get()->sortby('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $users = User::get()->sortby('id');
        return View('empleados.edit', compact('item', 'categorias', 'niveles', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Empleado::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('empleados.index')->with(['message' => __('Employee updated successfully.')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rows = Empleado::destroy($id);
		if ( $rows > 0 ) return redirect()->back()->with('message', 'Empleado eliminado correctamente.');
		else return redirect()->back()->with('error', 'Ha ocurrido un error al eliminar el empleado.');
    }

    public function massDestroy(Request $request)
    {
        $empleados = explode(',', $request->input('ids'));
        foreach ($empleados as $empleado_id) {
            $empleado = Empleado::findOrFail($empleado_id);
            $empleado->delete();
        }
        return redirect()->route('empleados.index')->with(['message' => __('Employee deleted succesfully.')]);
    }

    public function importar(Request $request) {

        if ( !empty(Opcion::where('key', 'calendario_sin_nivel_id')->first()) )
            $calendario_sin_nivel_id = Opcion::where('key', 'calendario_sin_nivel_id')->first()->value;
        else $calendario_sin_nivel_id = 0;

        $arxiu = request()->importar_empleados_file;
        if ( empty($arxiu) || (!empty($arxiu) && $arxiu->getClientOriginalExtension() != 'csv') ) {
            return back()->with('error', 'Empty or incorrect file.');
        }
        if ( empty($arxiu->path()) ) {
            return back()->with('error', 'Empty or incorrect file.');
        }

        $csv = $this->csv_to_array($arxiu->path(), ';', false);
        $csv = $this->utf8_converter($csv);
        
        $error = false;
        foreach ($csv as $key => $row) {
            $empleado_codigo = $row[0];
            $empleado = Empleado::where('codigo', $empleado_codigo)->first();
            if ( empty($empleado) ) $empleado = new Empleado();
            
            $empleado->codigo = $empleado_codigo;

            if ( !empty($row[1]) ) $empleado->nombre = $row[1];
            if ( !empty($row[2]) ) {
                $fecha_alta = \DateTime::createFromFormat('d-m-Y', $row[2]);
                $empleado->fecha_alta = $fecha_alta->format('Y-m-d H:i:s');
            }
            if ( !empty($row[3]) ) $empleado->dni = $row[3];
            if ( !empty($row[4]) ) $empleado->sexo = $row[4];
            if ( !empty($row[5]) ) {
                $fecha_nacimiento = \DateTime::createFromFormat('d-m-Y', $row[5]);
                $empleado->fecha_nacimiento = $fecha_nacimiento->format('Y-m-d H:i:s');
            }
            if ( !empty($row[6]) ) {
                $fecha_baja = \DateTime::createFromFormat('d-m-Y', $row[6]);
                $empleado->fecha_baja = $fecha_baja->format('Y-m-d H:i:s');
            }
            if ( !empty($row[7]) ) {
                $empleado->categoria_id = $row[7];

                $categoria = CategoriasEmpleado::where('nombre', $empleado->categoria_id)->first();
                if ( empty($categoria) ) {
                    $categoria = new CategoriasEmpleado();
                    $categoria->nombre = $empleado->categoria_id;
                    if ( $categoria->save() ) {
                        $empleado->categoria_id = $categoria->id;
                    }
                } else $empleado->categoria_id = $categoria->id;
            }
            if ( !empty($row[8]) ) {
                $empleado->calendario_nivel_id = $row[8];
                $nivel = CalendarioNivel::where('name', $empleado->calendario_nivel_id)->first();
                if ( empty($nivel) ) {
                    $nivel = new CalendarioNivel();
                    $nivel->name = $empleado->calendario_nivel_id;
                    if ( $nivel->save() ) {
                        $empleado->calendario_nivel_id = $nivel->id;
                    }
                } else $empleado->calendario_nivel_id = $nivel->id;
            }

            if ( empty($empleado->calendario_nivel_id) || $empleado->calendario_nivel_id == 0 ) {
                $empleado->calendario_nivel_id = 1;
            }

            if ( !$empleado->save() ) {
                if (!$error) $error = true;
            }
        }
        
        if ( !$error ) {
            return redirect()->back()->with('message', 'Enhorabuena importación realizada correctamente.');
        } else {
            return redirect()->back()->with('error', 'Ha ocurrido un error al guardar los datos.');
        }
    }

    public function utf8_converter($array) {
		array_walk_recursive($array, function(&$item, $key) {
			if ( !mb_detect_encoding($item, 'utf-8', true) ) {
				$item = utf8_encode($item);
			}
		});
		return $array;
    }
    
    public function csv_to_array($filename='', $delimiter=',', $with_header = true) {
        if ( !file_exists($filename) || !is_readable($filename) ) return false;
    
        $header = NULL;
        $data = array();
        if ( ($handle = fopen($filename, 'r')) !== false ) {
            while ( ($row = fgetcsv($handle, 1000, $delimiter)) !== false ) {
                if ( !$header )
                    $header = $row;
                else if ( $with_header )
                    $data[] = array_combine($header, $row);
                else
                    $data[] = $row;
            }
            fclose($handle);
        }
        return $data;
    }

}
