<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

use App\Mensaje;
use App\Opcion;
use App\Notifications\NuevoMensajeNotification;
use App\User;

class MensajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        $mensajes = Mensaje::where('user_id', $user->id)->get()->sortby('created_at');
        return view('contactar.index', compact('mensajes'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Auth::user();
        $data = $request->all();

        $mensaje = new Mensaje();

        $mensaje->user_id = $data['user_id'];
        $mensaje->from = $data['user_id'];
        $mensaje->mensaje = $data['mensaje'];

        $moderadors_calendari_contacte = null;
        if ( !empty(Opcion::where('key', 'moderadors_calendari_contacte')->first()) ) {
            $moderadors_calendari_contacte = Opcion::where('key', 'moderadors_calendari_contacte')->first()->value;
            if ( !empty($moderadors_calendari_contacte) ) {
                $moderadors = explode('#', $moderadors_calendari_contacte);
                foreach ($moderadors as $mod_email) {
                    $mod_user = User::where('email', $mod_email)->first();
                    if ( !empty($mod_user) ) {
                        $notificacion = new NuevoMensajeNotification($mod_user, $mensaje);
                        $mod_user->notify( $notificacion );
                    }
                }
            }
        }

        if (isset($request->fitxer) && !empty($request->fitxer) && gettype($request->fitxer) == 'object' ) {
            $filename = $user->id.'_f_'.time().'.'.$request->fitxer->getClientOriginalExtension();
            $path = $request->fitxer->storeAs('fitxers', $filename);
            $mensaje->fitxer = $path;
        } else $mensaje->fitxer = '';

        if ( $id = $mensaje->save() ) {
            echo 'OK';
        } else echo 'KO';

        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
