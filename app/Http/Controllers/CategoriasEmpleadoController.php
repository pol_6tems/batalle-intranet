<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empleado;
use App\CategoriasEmpleado;
use App\User;
use Validator;
use Auth;

class CategoriasEmpleadoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = CategoriasEmpleado::all();
        return View('empleados.categorias.index', compact('categorias', $categorias));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('empleados.categorias.crear');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ( $request->isMethod('post') ) {
			//Roles de validación
			$rules = [
				'nombre' => 'required|min:3|max:16|regex:/^[a-záéíóúàèìòùäëïöüñ\s]+$/i',
			];

			//Posibles mensajes de error de validación
			$messages = [
				'nombre.required' => 'El campo es requerido',
				'nombre.min' => 'El mínimo de caracteres permitidos son 3',
				'nombre.max' => 'El máximo de caracteres permitidos son 16',
			];

			$validator = Validator::make($request->all(), $rules, $messages);

			//Si la validación no es correcta redireccionar al formulario con los errores
			if ( $validator->fails() ){
				return redirect()->back()->withErrors($validator);
			}
			else { // De los contrario guardar al usuario
				$empleado = new Empleado;
				$empleado->nombre = $request->nombre;

				if ( $empleado->save() ) {
					return redirect()->back()->with('message', 'Enhorabuena nueva categoría creada correctamente.');
				} else {
					return redirect()->back()->with('error', 'Ha ocurrido un error al guardar los datos.');
				}
			}
		}
		return View('empleados.categorias.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rows = CategoriasEmpleado::destroy($id);
		if ( $rows > 0 ) return redirect()->back()->with('message', 'Categoría eliminada correctamente.');
		else return redirect()->back()->with('error', 'Ha ocurrido un error al eliminar la categoría.');
    }

}
