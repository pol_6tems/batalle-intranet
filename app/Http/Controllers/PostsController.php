<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StorePostRequest;
use App\Post;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$posts = Post::all();
        //$posts = Post::paginate(20);
        $posts = Post::paginate(20);
        $posts_trash = Post::onlyTrashed()->paginate(20);
        return view('admin.posts.index', compact('posts', 'posts_trash'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', Post::class);
        return view('admin.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request)
    {
        $this->authorize('create', Post::class);
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $data[$lang->code]['post_name'] = '/' . $sanitize_title;
            }
        }
        Post::create($data);
        return redirect()->route('posts.index')->with(['message' => __('Post added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  string  $post_name
     * @return \Illuminate\Http\Response
     */
    public function show($post_name)
    {
        $post_translation = \DB::table('post_translations')->where('post_name', $post_name)->first();
        if ( empty($post_translation) ) abort(404);
        
        $post = Post::find( $post_translation->post_id );

        if ( empty($post) ) abort(404);
        
        \App::setLocale($post_translation->locale);
        session(['locale' => $post_translation->locale]);
        app()->setLocale($post_translation->locale);

        return view('posts.show', compact('post'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', Post::class);
        $post = Post::findOrFail($id);
        return view('admin.posts.edit', compact('post'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(StorePostRequest $request, $id)
    {
        $this->authorize('update', Post::class);
        $post = Post::findOrFail($id);
        $data = $request->all();
        foreach ($this->_languages as $lang) {
            if ( !empty($data[$lang->code]) && empty( $data[$lang->code]['post_name'] ) ) {
                $sanitize_title = str_slug($data[$lang->code]['title'], '-');
                $data[$lang->code]['post_name'] = '/' . $sanitize_title;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $data[$lang->code]['post_name']);
            } else if ( !empty($data[$lang->code]) && !empty( $data[$lang->code]['post_name'] ) ) {
                $sanitize_title = str_slug($data[$lang->code]['post_name'], '-');
                $data[$lang->code]['post_name'] = '/' . $sanitize_title;
                $data[$lang->code]['post_name'] = str_replace('//', '/', $data[$lang->code]['post_name']);
            }
        }
        $post->update($data);
        return redirect()->route('posts.index')->with(['message' => __('Post updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::findOrFail($id);
        $post->status = 'trash';
        $post->update();
        $post->delete();
        
        return redirect()->route('posts.index')->with(['message' => __('Post sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $this->authorize('delete', Post::class);
        $posts = explode(',', $request->input('ids'));
        foreach ($posts as $post_id) {
            $post = Post::findOrFail($post_id);
            $post->status = 'trash';
            $post->update();
            $post->delete();
        }
        return redirect()->route('posts.index')->with(['message' => __('Post sent to trash')]);
    }

    public function restore($id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::withTrashed()->find($id);
        $post->status = 'publish';
        $post->update();
        $post->restore();
        
        return redirect()->route('posts.index')->with(['message' => __('Post restored successfully.')]);
    }

    public function destroy_permanent($id)
    {
        $this->authorize('delete', Post::class);
        $post = Post::withTrashed()->find($id);
        $post->forceDelete();
        
        return redirect()->route('posts.index')->with(['message' => __('Post deleted successfully.')]);
    }
}
