<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\CalendarioVacacion;
use App\CalendarioFestivo;
use App\CalendarioVetado;
use App\Opcion;
use App\CalendarioMensaje;
use App\Notifications\PeticionVacacionNotification;

class CalendariController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();

        // Defaults
        $any = date('Y') - 1;
        $pinks = $blues = $yellows = $greens = $reds = $greys = array();
        $reds_num_any = $dies_pendents_restats = 0;
        $sel_dia_o_semana = 'dia';
        $dies_pendents[$any] = 0;
        $dies_pendents[$any + 1] = 22;
        $no_bloquejat = false;
        // \Defaults
        
        /**
         * Primera assignació
         * Aqui les variables per defecte agafen els valors determinats a la pàgina d'opcions
         * var $dies_pendents
         * var $sel_dia_o_semana
         */
        if ( !empty(Opcion::where('key', 'dies_pendents_any_' . $any)->first()) )
            $dies_pendents[$any] = Opcion::where('key', 'dies_pendents_any_' . $any)->first()->value;

        if ( !empty(Opcion::where('key', 'dies_pendents_any_' . ($any + 1))->first()) )
            $dies_pendents[$any + 1] = Opcion::where('key', 'dies_pendents_any_' . ($any + 1))->first()->value;

        if ( !empty(Opcion::where('key', 'sel_dia_o_semana')->first()) )
            $sel_dia_o_semana = Opcion::where('key', 'sel_dia_o_semana')->first()->value;
        /** Fi Primera Assignació */

        /**
         * Segona assignació
         * Aqui les variables es sobreescriuen per els valors assignats als subnivells
         * o bé pel seu nivell
         */
        if ( $user->subnivel()->exists() ) {
            $subnivel = $user->subnivel()->first();
            $sel_dia_o_semana = $subnivel->sel_dia_o_semana;
            if ( $subnivel->years()->exists() ) {
                $aux = $subnivel->years()->where('year', $any)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any] = $aux->days;
                $aux = $subnivel->years()->where('year', $any + 1)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 1] = $aux->days;
            }
        } else if ( $user->nivel()->exists() ) {
            $nivel = $user->nivel()->first();
            $sel_dia_o_semana = $nivel->sel_dia_o_semana;
            if ( $nivel->years()->exists() ) {
                $aux = $nivel->years()->where('year', $any)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any] = $aux->days;
                $aux = $nivel->years()->where('year', $any + 1)->first();
                if ( !empty($aux) && !empty($aux->days) ) $dies_pendents[$any + 1] = $aux->days;
            }
        }

        /**
         * Tercera assignació
         * Aqui les variables agafen el valor seleccionat a la personalització anual del nivell
         */
        if ( $user->years()->exists() ) {
            $aux = $user->years()->where('year', $any)->first();
            if ( !empty($aux) && !empty($aux->days) && $aux->days > 0 ) $dies_pendents[$any] = $aux->days;
            $aux = $user->years()->where('year', $any + 1)->first();
            if ( !empty($aux) && !empty($aux->days) && $aux->days > 0 ) $dies_pendents[$any + 1] = $aux->days;
        }
        /* end Dies pendents */

        // Peticions pendents aquest any
        $dies_pink = CalendarioVacacion::where([['year', '=', $any], ['user_id', '=', Auth::user()->id], ['status', '=', 'pending']])->get();
        // Peticions pendents any següent
        $dies_blue = CalendarioVacacion::where([['year', '=', $any + 1], ['user_id', '=', Auth::user()->id], ['status', '=', 'pending']])->get();
        // Peticions denegades aquest any i següent
        $dies_yellow = CalendarioVacacion::where([['year', '>=', $any], ['year', '<=', $any + 1], ['user_id', '=', Auth::user()->id], ['status', '=', 'denied']])->get();
        // Peticions aprovades aquest any i següent
        $dies_green = CalendarioVacacion::where([['year', '>=', $any], ['year', '<=', $any + 1], ['user_id', '=', Auth::user()->id], ['status', '=', 'approved']])->get();
        // Festius
        $dies_red_where = array(['year', '>=', $any], ['year', '<=', $any + 2]);
        
        if ( !empty($user->subnivel) ) $dies_red_where[] = array('calendario_subnivel_id', '=', $user->subnivel->id);
        else if ( !empty($user->nivel) ) $dies_red_where[] = array('calendario_nivel_id', '=', $user->nivel->id);
        else $dies_red_where[] = array('calendario_nivel_id', '=', 0);
        $dies_red = CalendarioFestivo::where( $dies_red_where )->get();

        /* Preparar arrays */
        foreach ( $dies_pink as $dia_pink ) $pinks[] = array('start' => $dia_pink->start, 'end' => $dia_pink->end);
        foreach ( $dies_blue as $dia_blue ) $blues[] = array('start' => $dia_blue->start, 'end' => $dia_blue->end);
        foreach ( $dies_yellow as $dia_yellow ) $yellows[] = array('start' => $dia_yellow->start, 'end' => $dia_yellow->end);
        foreach ( $dies_green as $dia_green ) $greens[] = array('start' => $dia_green->start, 'end' => $dia_green->end);
        foreach ( $dies_red as $dia_red ) {
            foreach ( explode(',', $dia_red->days) as $dia_red_day ) {
                $reds[] = array('start' => $dia_red_day, 'end' => $dia_red_day);
                if ( $dia_red->year == $any + 1 ) $reds_num_any++;
            }
        }
        
        $dies_pendents_restats = $dies_pendents;
        foreach ( $pinks as $pink ) {
            $dStart = new \DateTime($pink['start']);
            $dEnd  = new \DateTime($pink['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        foreach ( $blues as $blue ) {
            $dStart = new \DateTime($blue['start']);
            $dEnd  = new \DateTime($blue['end']);
            $dDiff = $dStart->diff($dEnd);
            $dies_pendents_restats[$any + 1] -= ($dDiff->days > 0 ? $dDiff->days : 1);
        }
        /* end Preparar arrays */

        // Arrays pendents
        $total_pendents = 0;
        foreach($blues as $dia) {
            $start = new \Carbon\Carbon($dia['start']);
            $end = new \Carbon\Carbon($dia['end']);
            $diff = $start->diff($end)->days;
            if ($diff) $total_pendents += $diff;
            else $total_pendents++;
        }
        
        // Si tenen la peticio bloquejada, els blaus són dies solicitats i pendents d'acceptacio
        // per tant no s'han de comptar amb el total dels disponibles
        $bloquejar_enviar_peticio = !empty($pinks) || !empty($blues);
        if (!$bloquejar_enviar_peticio) $dies_pendents[$any + 1] += $total_pendents;
        else $dies_pendents[$any + 1] -= $total_pendents;

        $total_green = 0;
        foreach($greens as $dia) {
            $start = new \Carbon\Carbon($dia['start']);
            $end = new \Carbon\Carbon($dia['end']);
            $diff = $start->diff($end)->days;
            if ($diff) $total_green += $diff;
            else $total_green++;
        }

        $dies_pendents[$any+1] -= $total_green;

        /* Dies vetats */
        $vetats_args = array(['year', '>=', $any], ['year', '<=', $any + 2]);
        if ( !empty($user->subnivel->id) ) {
            $vetats_args[] = ['calendario_subnivel_id', '=', $user->subnivel->id];
        } else if ( !empty($user->nivel->id) ) {
            $vetats_args[] = ['calendario_nivel_id', '=', $user->nivel->id];
        } else {
            $vetats_args[] = ['calendario_nivel_id', '=', 0];
        }
        $vetats = CalendarioVetado::where($vetats_args)->get()->sortByDesc("year");
        
        foreach ( $vetats as $vetat ) {
            foreach ( explode(',', $vetat->days) as $vetat_day ) {
                $greys[] = array('start' => $vetat_day, 'end' => $vetat_day);
            }
        }
        /* end Dies vetats */

        /* Get Mensajes del usuario */
        $mensajes = CalendarioMensaje::where('user_id', $user->id)->get()->sortby('created_at');
        /* end Get Mensajes del usuario */

        // Bloquejar "Enviar petició" si té dies pendents (pink o blue)
        if ( $user->subnivel()->exists() ) {
            $no_bloquejat = $user->subnivel()->first()->no_bloquejat == 1;
        } else if ( $user->nivel()->exists() ) {
            $no_bloquejat = $user->nivel()->first()->no_bloquejat == 1;
        }

        $last_update = null;
        $last_updates = CalendarioVacacion::where([['year', '>=', $any], ['year', '<=', $any + 2], ['user_id', '=', Auth::user()->id], ['status', '=', 'pending']])->get()->sortByDesc('created_at');
        if ( !empty($last_updates) && !empty($last_updates->first()) ) {
            $last_update = $last_updates->first()->created_at;
        }

        return view('calendari.index', array(
            'dies_pendents_totals' => $dies_pendents_restats,
            'dies_pendents' => $dies_pendents,
            'pinks' => $pinks,
            'blues' => $blues,
            'yellows' => $yellows,
            'greens' => $greens,
            'reds' => $reds,
            'greys' => $greys,
            'sel_dia_o_semana' => $sel_dia_o_semana,
            'reds_num' => $reds_num_any,
            'aquest_any' => $any,
            'mensajes' => $mensajes,
            'bloquejar_enviar_peticio' => $bloquejar_enviar_peticio,
            'no_bloquejat' => $no_bloquejat,
            'last_update' => $last_update,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $any = date('Y') - 1;
        $data = $request->all();

        $ePink = json_decode($data['ePink']);
        $eBlue = json_decode($data['eBlue']);
        
        if ( !empty($ePink) ) {
            //CalendarioVacacion::where([['year', '=', $any], ['status', '=', 'pending'], ['user_id', '=', Auth::user()->id]])->delete();
            $this->eliminar_duplicats($ePink);
        }
        if ( !empty($eBlue) ) {
            //CalendarioVacacion::where([['year', '=', $any + 1], ['status', '=', 'pending'], ['user_id', '=', Auth::user()->id]])->delete();
            $this->eliminar_duplicats($eBlue);
        }

        foreach ( $ePink as $dia ) {
            $peticion = array(
                'user_id' => Auth::user()->id,
                'year' => $any,
                'start' => $dia->start,
                'end' => $dia->end,
            );
            CalendarioVacacion::create($peticion);
        }

        foreach ( $eBlue as $dia ) {
            $peticion = array(
                'user_id' => Auth::user()->id,
                'year' => $any + 1,
                'start' => $dia->start,
                'end' => $dia->end,
            );
            CalendarioVacacion::create($peticion);
        }

        $peticiones = array_merge($ePink, $eBlue);
        usort($peticiones, function($a, $b) {
            return strtotime($a->start) - strtotime($b->start);
        });

        $curr_user = Auth::user();
        $notificacion = new PeticionVacacionNotification($curr_user, $peticiones, true);
        $curr_user->notify( $notificacion );

        return redirect()->route('calendari.index')->with(['message' => __('Thank you for sending your holiday request shortly. Batallé will review and respond to your request.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /* Elimina els dies sense tenir en compte l'any per si una petició pink ara és blue.
     * Les peticions eliminades són SoftDeletes/Trashed (deleted_at).
    */
    private function eliminar_duplicats($peticions)
    {
        $where_general = [
            ['user_id', '=', Auth::user()->id],
            ['status', '=', 'pending']
        ];
        foreach ( $peticions as $peticio ) {
            $where_general[] = ['start', '=', $peticio->start];
            $where_general[] = ['end', '=', $peticio->end];
            $dies_pink = CalendarioVacacion::where( $where_general )->delete();
        }

    }

}
