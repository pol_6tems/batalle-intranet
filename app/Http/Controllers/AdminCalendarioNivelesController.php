<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarioNivel;
use App\CalendarioNivelYear;
use App\CalendarioNivelWeek;
use App\CalendarioNivelEncarregat;
use App\User;

class AdminCalendarioNivelesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CalendarioNivel::paginate(20);
        return view('admin.calendario.nivel.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', CalendarioNivel::class);
        return view('admin.calendario.nivel.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioNivel::create($data);
        return redirect()->route('calendario.niveles.index')->with(['message' => __('Level added successfully')]);
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivel::findOrFail($id);
        $years = CalendarioNivelYear::where('calendario_nivel_id', "=", $id)->orderBy('year', 'DESC')->paginate(20);
        $weeks = CalendarioNivelWeek::where('calendario_nivel_id', "=", $id)->orderBy('year', 'DESC')->orderBy('week', 'DESC')->paginate(20);
        $encarregats = CalendarioNivelEncarregat::where('calendario_nivel_id', "=", $id)->orderBy('user_id', 'ASC')->paginate(20);
        return view('admin.calendario.nivel.edit', compact('item', 'years', 'weeks', 'encarregats'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivel::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.niveles.index')->with(['message' => __('Level updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', CalendarioNivel::class);
        $item = CalendarioNivel::findOrFail($id);
        $item->status = 'trash';
        $item->update();
        $item->delete();
        
        return redirect()->route('calendario.niveles.index')->with(['message' => __('Level sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $this->authorize('delete', CalendarioNivel::class);
        $items = explode(',', $request->input('ids'));
        foreach ($items as $id) {
            $item = CalendarioNivel::findOrFail($id);
            $item->status = 'trash';
            $item->update();
            $item->delete();
        }
        return redirect()->route('calendario.niveles.index')->with(['message' => __('Levels sent to trash')]);
    }

    /* CalendarioNivelYear */
    public function years_create($id)
    {
        $this->authorize('create', CalendarioNivel::class);
        return view('admin.calendario.nivel.years.create', compact('id'));
    }

    public function years_store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioNivelYear::create($data);
        return redirect()->route('calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelYear::findOrFail($id);
        return view('admin.calendario.nivel.years.edit', array('item' => $item));
    }

    public function years_update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Year updated successfully')]);
    }
    /* end CalendarioNivelYear */

    /* CalendarioNivelWeek */
    public function weeks_create($id)
    {
        $this->authorize('create', CalendarioNivel::class);
        return view('admin.calendario.nivel.weeks.create', compact('id'));
    }

    public function weeks_store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioNivelWeek::create($data);
        return redirect()->route('calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Week added successfully')]);
    }

    public function weeks_edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelWeek::findOrFail($id);
        return view('admin.calendario.nivel.weeks.edit', array('item' => $item));
    }

    public function weeks_update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelWeek::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Week updated successfully')]);
    }
    /* end CalendarioNivelWeek */
    
    /* CalendarioNivelEncarregat */
    public function encarregat_create($id)
    {
        $this->authorize('create', CalendarioNivel::class);
        $users = User::get()->sortby('id');
        return view('admin.calendario.nivel.encarregat.create', compact('id', 'users'));
    }

    public function encarregat_store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioNivelEncarregat::create($data);
        return redirect()->route('calendario.niveles.edit', $data['calendario_nivel_id'])->with(['message' => __('Manager added successfully')]);
    }

    public function encarregat_edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $users = User::get()->sortby('id');
        return view('admin.calendario.nivel.encarregat.edit', compact('item', 'users'));
    }

    public function encarregat_update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.niveles.edit', $item->calendario_nivel_id)->with(['message' => __('Manager updated successfully')]);
    }

    public function encarregat_destroy($id)
    {
        $this->authorize('delete', CalendarioNivel::class);
        $item = CalendarioNivelEncarregat::findOrFail($id);
        $item->delete();
        return redirect()->back()->with(['message' => __('Manager deleted successfully')]);
    }
    /* end CalendarioNivelEncarregat */
}
