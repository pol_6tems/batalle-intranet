<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarioNivel;
use App\CalendarioSubnivel;
use App\CalendarioVetado;

class AdminCalendarioVetadosController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $any = date('Y');
        $items = CalendarioVetado::where([['year', '>=', $any], ['year', '<=', $any + 1]])->get()->sortByDesc("year");
        $vetados = array();
        foreach ($items as $item) {
            if ( $item->calendario_subnivel_id > 0 )
                $vetados[$item->year]['subnivel-' . $item->calendario_subnivel_id] = explode(',', $item->days);
            else if ( $item->calendario_nivel_id > 0 )
                $vetados[$item->year]['nivel-' . $item->calendario_nivel_id] = explode(',', $item->days);
            else $vetados[$item->year]['nivel-1'] = explode(',', $item->days);
        }
        // Niveles -> Subniveles -> Empleados
        $niveles = CalendarioNivel::where('status', '=', 'publish')->get();
        foreach ($niveles as $key => $nivel) {
            $subniveles = CalendarioSubnivel::where('status', '=', 'publish')->where('calendario_nivel_id', '=', $nivel->id)->get();
            $nivel->subniveles = $subniveles;

            $niveles[$key] = $nivel;
        }
        
        return view('admin.calendario.vetado', compact('vetados', 'niveles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $any = date('Y');
        $data = $request->all();
        $dias = json_decode($data['dies']);
        $vetados = array();

        CalendarioVetado::where([['year', '=', $any]])->delete();
        CalendarioVetado::where([['year', '=', $any + 1]])->delete();

        foreach ( $dias as $dia ) {
            $nivel_id = $subnivel_id = 0;
            if ( !array_key_exists($dia->any, $vetados) ) $vetados[$dia->any] = array();
            if ( !array_key_exists($dia->nivel, $vetados[$dia->any]) ) $vetados[$dia->any][$dia->nivel] = array();
            $vetados[$dia->any][$dia->nivel][] = $dia->fecha;
        }
        foreach ($vetados as $any => $niveles) {
            foreach ($niveles as $nivel => $dias) {
                $nivel_parts = explode('-', $nivel);
                $item = array(
                    'year' => $any,
                    'days' => implode(',', $dias),
                    'calendario_' . $nivel_parts[0] . '_id' => $nivel_parts[1],
                );
                CalendarioVetado::create($item);
            }
        }
        return redirect()->route('calendario.vetados.index')->with(['message' => __('Days banned assigned successfully.')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
