<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarioNivel;
use App\CalendarioSubnivel;
use App\CalendarioSubnivelYear;
use App\CalendarioSubnivelWeek;

class AdminCalendarioSubnivelesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = CalendarioSubnivel::paginate(20);
        return view('admin.calendario.subnivel.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', CalendarioNivel::class);
        $niveles = CalendarioNivel::get();
        return view('admin.calendario.subnivel.create', compact('niveles'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioSubnivel::create($data);
        return redirect()->route('calendario.subniveles.index')->with(['message' => __('Level added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivel::findOrFail($id);
        $niveles = CalendarioNivel::get();
        $years = CalendarioSubnivelYear::where('calendario_subnivel_id', "=", $id)->orderBy('year', 'DESC')->paginate(20);
        $weeks = CalendarioSubnivelWeek::where('calendario_subnivel_id', "=", $id)->orderBy('year', 'DESC')->orderBy('week', 'DESC')->paginate(20);
        return view('admin.calendario.subnivel.edit', compact('item', 'years', 'niveles', 'weeks'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivel::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.subniveles.index')->with(['message' => __('Level updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->authorize('delete', CalendarioNivel::class);
        $item = CalendarioSubnivel::findOrFail($id);
        $item->status = 'trash';
        $item->update();
        $item->delete();
        
        return redirect()->route('calendario.subniveles.index')->with(['message' => __('Level sent to trash')]);
    }

    public function massDestroy(Request $request)
    {
        $this->authorize('delete', CalendarioNivel::class);
        $items = explode(',', $request->input('ids'));
        foreach ($items as $id) {
            $item = CalendarioSubnivel::findOrFail($id);
            $item->status = 'trash';
            $item->update();
            $item->delete();
        }
        return redirect()->route('calendario.subniveles.index')->with(['message' => __('Levels sent to trash')]);
    }

    /* CalendarioSubnivelYear */
    public function years_create($id)
    {
        $this->authorize('create', CalendarioNivel::class);
        return view('admin.calendario.subnivel.years.create', compact('id'));
    }

    public function years_store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioSubnivelYear::create($data);
        return redirect()->route('calendario.subniveles.edit', $data['calendario_subnivel_id'])->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivelYear::findOrFail($id);
        return view('admin.calendario.subnivel.years.edit', array('item' => $item));
    }

    public function years_update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivelYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.subniveles.edit', $item->calendario_subnivel_id)->with(['message' => __('Year updated successfully')]);
    }
    /* end CalendarioSubnivelYear */

    /* CalendarioSubnivelWeek */
    public function weeks_create($id)
    {
        $this->authorize('create', CalendarioNivel::class);
        return view('admin.calendario.subnivel.weeks.create', compact('id'));
    }

    public function weeks_store(Request $request)
    {
        $this->authorize('create', CalendarioNivel::class);
        $data = $request->all();
        CalendarioSubnivelWeek::create($data);
        return redirect()->route('calendario.subniveles.edit', $data['calendario_subnivel_id'])->with(['message' => __('Week added successfully')]);
    }

    public function weeks_edit($id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivelWeek::findOrFail($id);
        return view('admin.calendario.subnivel.weeks.edit', array('item' => $item));
    }

    public function weeks_update(Request $request, $id)
    {
        $this->authorize('update', CalendarioNivel::class);
        $item = CalendarioSubnivelWeek::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('calendario.subniveles.edit', $item->calendario_subnivel_id)->with(['message' => __('Week updated successfully')]);
    }
    /* end CalendarioSubnivelWeek */
}
