<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Menu;
use App\MenuSub;
use App\Rol;

class AdminMenusController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Menu::orderby('order')->paginate(20);
        return View('admin.configuracion.menus.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $rols = Rol::get()->sortby('name');
        return View('admin.configuracion.menus.create', compact('rols'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Menu::create($data);
        return redirect()->route('configuracion.menus.index')->with(['message' => __('Menu added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Menu::findOrFail($id);
        $rols = Rol::get()->sortby('name');
        $subs = MenuSub::where('menu_id', "=", $id)->orderby('order')->paginate(20);
        return view('admin.configuracion.menus.edit', compact('item', 'rols', 'subs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Menu::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('configuracion.menus.index')->with(['message' => __('Menu updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Menu::findOrFail($id);
        $item->delete();
        return redirect()->route('configuracion.menus.index')->with(['message' => __('Menu deleted successfully')]);
    }

    /* MenuSub */
    public function submenu_create($id)
    {
        $rols = Rol::get()->sortby('name');
        return view('admin.configuracion.menus.subs.create', compact('id', 'rols'));
    }

    public function submenu_store(Request $request)
    {
        $data = $request->all();
        if (empty($data['order'])) $data['order'] = 0;
        MenuSub::create($data);
        return redirect()->route('configuracion.menus.edit', $data['menu_id'])->with(['message' => __('Menu added successfully')]);
    }

    public function submenu_edit($id)
    {
        $item = MenuSub::findOrFail($id);
        $rols = Rol::get()->sortby('name');
        return view('admin.configuracion.menus.subs.edit', compact('item', 'rols'));
    }

    public function submenu_update(Request $request, $id)
    {
        $item = MenuSub::findOrFail($id);
        $data = $request->all();
        if (empty($data['order'])) $data['order'] = 0;
        $item->update($data);
        return redirect()->route('configuracion.menus.edit', $item->menu_id)->with(['message' => __('Menu updated successfully')]);
    }
    /* end MenuSub */
}
