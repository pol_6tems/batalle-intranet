<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreAccesRequest;
use Session;
use App\Empleado;
use App\User;
use App\Opcion;
use App\Notifications\NuevoUsuarioNotification;

class AccesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('acces.index');
    }

    public function benvingut()
    {
        return view('acces.benvingut');
    }

    public function check_dni(Request $request)
    {
        if ( empty($request->dni) ) {
            return view('acces.index');
        }
        $dni = $request->dni;
        $usuario = User::where('dni', $dni)->first();
        if ( !empty($usuario) ) {
            return redirect()->route('login')->with('message', 'El dni ja existeix com a usuari');
        }

        $empleado = Empleado::where('dni', $dni)->first();
        if ( empty($empleado) ) {
            return redirect()->route('acces.index')->with('message', 'El dni no existeix com a empleat');
        }

        return redirect()->route('acces.registre', ['dni' => $dni])->with(['pot_continuar' => 'ok', 'dni' => $dni]);   
    }

    public function registre()
    {
        if ( empty( $_REQUEST['dni']) ) return view('errors.403');
        $dni = $_REQUEST['dni'];
        $usuario = User::where('dni', $dni)->first();
        if ( !empty($usuario) ) {
            return redirect()->route('login')->with('message', 'El dni ja existeix com a usuari');
        }
        $empleado = Empleado::where('dni', $dni)->first();
        if ( empty($empleado) ) {
            return redirect()->route('acces.index')->with('message', 'El dni no existeix com a empleat');
        }
        
        $nombres = explode(',', $empleado->nombre);
        if ( count($nombres) > 1 ) {
            $empleado->apellido = !empty($nombres[0]) ? ucwords(strtolower(trim($nombres[0]))) : '';
            $empleado->nombre = !empty($nombres[1]) ? ucwords(strtolower(trim($nombres[1]))) : '';
        } else {
            $empleado->apellido = '';
            $empleado->nombre = ucwords(strtolower(trim($empleado->nombre)));
        }
        $empleado->fecha_nacimiento = new \Carbon\Carbon($empleado->fecha_nacimiento); 
        return View('acces.registre', compact('empleado', $empleado));
    }

    public function crear_user(StoreAccesRequest $request)
    {
        if ( !empty(Opcion::where('key', 'calendario_sin_nivel_id')->first()) )
            $calendario_sin_nivel_id = Opcion::where('key', 'calendario_sin_nivel_id')->first()->value;
        else $calendario_sin_nivel_id = 0;

        $data = $request->all();
        $fecha_nacimiento = new \Carbon\Carbon($data['any'] . '-' . $data['mes'] . '-' . $data['dia']);
        
        $empleado = Empleado::where('dni', $data['dni'])->first();
        

        //$randomPassword = bin2hex(random_bytes(5));
        $usuaris_exists = User::where('dni', '=', $data['dni'])->get();
        if ( count($usuaris_exists) > 0 ) {
            return redirect()->route('acces.dni')->with('error', __('User already exists.'));
        }

        $user = new User;
        $user->password = bcrypt($data['password']);
        $user->remember_token = str_random(100);
        $user->name = $data['name'];
        $user->lastname = $data['lastname'];
        $user->dni = $data['dni'];
        $user->email = $data['email'];
        $user->phone = str_replace(__('Phone'), '', $data['phone']);
        $user->mobile = str_replace(__('Mobile'), '', $data['mobile']);
        $user->address = str_replace(__('Address'), '', $data['address']);
        $user->codigo_postal = str_replace(__('ZIP zone'), '', $data['codigo_postal']);
        $user->city = str_replace(__('City'), '', $data['city']);
        $user->idioma = $data['idioma'];
        $user->fecha_nacimiento = $fecha_nacimiento;
        $user->lopd = $data['lopd'] ? '1' : '0';
        $user->role = 'user';
        $user->calendario_nivel_id = !empty($empleado) ? $empleado->calendario_nivel_id : $calendario_sin_nivel_id;

        $user->consentiment_naixement = !empty($data['consentiment_naixement_si']) ? '1' : '0';
        $user->consentiment_privacitat = !empty($data['dono_consentiment_si']) ? '1' : '0';

        if ( $user->save() ) {

            $empleado->user_id = $user->id;
            $empleado->update();

            $user->notify(new NuevoUsuarioNotification($user, $data['password']));
            
            return redirect()->route('acces.benvingut');
        } else {
            return redirect()->route('acces.dni')->with('error', 'Ha ocurrido un error al guardar los datos.');
        }
    }
}
