<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CalendarioMensaje;
use App\User;
use App\Empleado;
use Auth;
use App\Notifications\NuevoMensajeNotification;
use DB;

class AdminCalendarioMensajesController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $users = User::whereHas(
            'mensajes', function ($query) {
                $query->whereNotNull('mensaje');
            }
        )
        ->leftJoin('calendario_mensajes', 'users.id', '=', 'calendario_mensajes.user_id')
        ->groupBy(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->orderByRaw('max(calendario_mensajes.updated_at) desc')
        ->select(['users.id', 'users.name', 'users.email', 'users.avatar'])
        ->paginate(20);
        
        return view('admin.calendario.mensaje.index', compact('users'));
    }

    public function contestar($user_id)
    {
        $user = User::find($user_id);
        $item = CalendarioMensaje::where('user_id', $user_id)->get()->sortByDesc('created_at');
        
        return view('admin.calendario.mensaje.contestar', compact('item', 'user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        $mensaje = new CalendarioMensaje();
        $mensaje->user_id = $data['user_id'];
        $mensaje->from = Auth::user()->id;
        $mensaje->mensaje = $data['mensaje'];
        
        if ( $mensaje->save() ) {
            
            $usuario = User::find( $mensaje->user_id );
            $notificacion = new NuevoMensajeNotification($usuario,  $mensaje, Auth::user());
            $usuario->notify( $notificacion );

            echo 'OK';
        }
        else echo 'KO';

        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function visto(Request $request)
    {
        $user_id = $request->user_id;
        DB::table('calendario_mensajes')->where('user_id', $user_id)->update(['visto' => true]);
        return response()->json(['success' => true]);
    }
}
