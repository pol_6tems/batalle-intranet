<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\CalendarioVacacion;
use App\CalendarioNivel;
use App\CalendarioSubnivel;
use App\Notifications\PeticionVacacionNotification;
use App\CalendarioNivelEncarregat;

class AdminCalendarioVacacionesController extends AdminController
{

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        /* CalendarioNivelEncarregat */
        $nivells_encarregats = array();
        $r = CalendarioNivelEncarregat::where('user_id', '=', Auth::user()->id)->get();
        if ( !$r->isEmpty() ) foreach ( $r as $r1 ) $nivells_encarregats[] = $r1->calendario_nivel_id;
        else if ( !Auth::user()->isAdmin() ) return abort(403);
        /* end CalendarioNivelEncarregat */
        
        $any = date('Y');

        // Pendientes
        $pendientes = CalendarioVacacion::where([['year', '>=', $any - 1], ['year', '<=', $any + 1],['status', '=', 'pending']])->get();
        foreach ($pendientes as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $pendientes[$key] = $item;
        }

        // Aprovados
        $aprovados = CalendarioVacacion::where([['year', '>=', $any - 1], ['year', '<=', $any + 1],['status', '=', 'approved']])->get();
        foreach ($aprovados as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $aprovados[$key] = $item;
        }

        // Denegados
        $denegados = CalendarioVacacion::where([['year', '>=', $any - 1], ['year', '<=', $any + 1],['status', '=', 'denied']])->get();
        foreach ($denegados as $key => $item) {
            $user_aux = User::find($item->user_id);
            if ( empty($user_aux) ) continue;
            $item->user_name = $user_aux->name . " " . $user_aux->lastname;
            $item->user_level_id = 'nivel-1';
            if ( $user_aux->calendario_subnivel_id > 0 ) $item->user_level_id = 'subnivel-' . $user_aux->calendario_subnivel_id;
            else if ( $user_aux->calendario_nivel_id > 0 ) $item->user_level_id = 'nivel-' . $user_aux->calendario_nivel_id;
            $denegados[$key] = $item;
        }

        // Niveles -> Subniveles -> Empleados
        $niveles_max_limit = array();
        $niveles = CalendarioNivel::where('status', '=', 'publish')->get();
        $niveles_permitidos = array();
        foreach ($niveles as $key => $nivel) {
            // CalendarioNivelEncarregat
            if ( !in_array($nivel->id, $nivells_encarregats) && !Auth::user()->isAdmin() ) continue;

            $subniveles = CalendarioSubnivel::where('status', '=', 'publish')->where('calendario_nivel_id', '=', $nivel->id)->get();
            foreach ($subniveles as $key2 => $subnivel) {
                $usuarios = User::where('calendario_subnivel_id', '=', $subnivel->id)->where('calendario_nivel_id', '=', 0)->get();
                $subnivel->usuarios = $usuarios;
                $subniveles[$key2] = $subnivel;
                $niveles_max_limit['subnivel-' . $subnivel->id] = $subnivel->get_persones_max();
            }
            $nivel->subniveles = $subniveles;

            $usuarios = User::where('calendario_nivel_id', '=', $nivel->id)->where('calendario_subnivel_id', '=', 0)->get();
            $nivel->usuarios = $usuarios;

            $niveles_max_limit['nivel-' . $nivel->id] = $nivel->get_persones_max();

            $niveles_permitidos[$key] = $nivel;
            //$niveles[$key] = $nivel;
        }
        //echo "<pre>";print_r($niveles_max_limit);echo "</pre>";
        return View('admin.calendari', array(
            'pendientes' => $pendientes, 
            'aprovados' => $aprovados, 
            'denegados' => $denegados,
            'niveles' => $niveles_permitidos,
            'niveles_max_limit' => $niveles_max_limit,
        ));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $any = date('Y');
        $data = $request->all();

        $dies = json_decode($data['dies']);
        $usuarios_a_enviar = array();
        foreach ( $dies as $dia ) {
            if ( $peticio = CalendarioVacacion::findOrFail( $dia->id ) ) {

                $is_peticio_modificada = $peticio->status != $dia->estado || $peticio->start != $dia->start || $peticio->end != $dia->end;

                $peticio->start = $dia->start;
                $peticio->end = $dia->end;
                $peticio->user_aproved_id = \Auth::user()->id;
                $peticio->status = $dia->estado;
                $peticio->update();

                if ( $is_peticio_modificada ) $usuarios_a_enviar[$dia->usuario_id][] = $peticio;
            }            
        }

        foreach ( $usuarios_a_enviar as $usuari_id => $peticiones ) {
            $usuario = User::find($usuari_id);
            if ( !empty($usuario) ) {
                usort($peticiones, function($a, $b) {
                    return strtotime($a->start) - strtotime($b->start);
                });
                $notificacion = new PeticionVacacionNotification($usuario, $peticiones, false);
                $usuario->notify( $notificacion );
            }
        }
        
        return redirect()->route('calendario-vacaciones.index')->with(['message' => __('Vacations assigned successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
