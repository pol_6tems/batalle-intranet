<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Usuario;
use App\User;
use App\Empleado;
use Validator;
use Auth;
use App\CalendarioUserYear;
use App\CalendarioNivel;
use App\CalendarioSubnivel;
use App\Rol;

class AdminUsuariosController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sortby = Input::get('sortby', 'dni');
        $order = Input::get('order', 'ASC');
        $search = Input::get('search', '');

        if ( empty($search) ) {
            $usuarios = User::orderby($sortby, $order)->get();
        } else {
            $usuarios = User::where(function($q) use ($search) {
                $q->orWhere('dni', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('name', 'LIKE', '%'.strtoupper($search).'%')
                ->orWhere('email', 'LIKE', '%'.strtoupper($search).'%');
            })->orderby($sortby, $order)->get();
        }

        return View('admin.configuracion.usuarios.index', compact('usuarios', 'sortby', 'order', 'search'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $empleados = Empleado::whereNull('user_id')->get()->sortBy('nombre');
        $niveles = CalendarioNivel::get()->sortby('name');
        $subniveles = CalendarioSubnivel::get()->sortby('name');
        $rols = Rol::get()->sortby('name');
        return View('admin.configuracion.usuarios.crear', compact('empleados', 'niveles', 'subniveles', 'rols'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        if ( $request->isMethod('post') ) {
			//Roles de validación
			$rules = [
				'name' => 'required|min:3',
				'email' => 'required|email|max:255|unique:users,email',
				'password' => 'required|min:6|confirmed',
			];

			//Posibles mensajes de error de validación
			$messages = [
				'name.required' => 'El campo es requerido',
				'name.min' => 'El mínimo de caracteres permitidos son 3',
				'name.max' => 'El máximo de caracteres permitidos son 16',
				'name.regex' => 'Sólo se aceptan letras',
				'email.required' => 'El campo es requerido',
				'email.email' => 'El formato de email es incorrecto',
				'email.max' => 'El máximo de caracteres permitidos son 255',
				'email.unique' => 'El email ya existe',
				'password.required' => 'El campo es requerido',
				'password.min' => 'El mínimo de caracteres permitidos son 6',
				'password.max' => 'El máximo de caracteres permitidos son 18',
				'password.confirmed' => 'Los passwords no coinciden',
			];

			$validator = Validator::make($request->all(), $rules, $messages);

			//Si la validación no es correcta redireccionar al formulario con los errores
			if ( $validator->fails() ){
				return redirect()->back()->withErrors($validator);
			}
			else { // De los contrario guardar al usuario
				$user = new User;
				$user->name = $request->name;
				$user->lastname = $request->lastname;
				$user->email = $request->email;
				$user->password = bcrypt($request->password);
                $user->remember_token = str_random(100);
            
				//$user->confirm_token = str_random(100);
				//Activar al administrador sin necesidad de enviar correo electrónico
				//$user->active = 1;
				//El valor 1 en la columna determina si el usuario es administrador o no
				$user->role = $request->role;
                $user->dni = $request->dni;
                $user->calendario_nivel_id = $request->calendario_nivel_id;
                $user->calendario_subnivel_id = $request->calendario_subnivel_id;

				if ( $user->save() ) {
                    $empleado = Empleado::where('dni', $user->dni)->first();
                    if ( !empty($empleado) ) {
                        $empleado->user_id = $user->id;
                        $empleado->update();
                    }
					return redirect()->route('usuarios.index')->with('message', 'Enhorabuena nuevo usuario creado correctamente.');
				} else {
					return redirect()->back()->with('error', 'Ha ocurrido un error al guardar los datos.');
				}
			}
		}
		return View('admin.configuracion.usuarios.create');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::find($id);
        $empleados = Empleado::whereNull('user_id')->get()->sortBy('nombre');
        $years = CalendarioUserYear::where('user_id', "=", $id)->orderBy('year', 'DESC')->paginate(20);
        $niveles = CalendarioNivel::get()->sortby('name');
        $subniveles = CalendarioSubnivel::get()->sortby('name');
        $rols = Rol::get()->sortby('name');
        return View('admin.configuracion.usuarios.edit', compact('item', 'empleados', 'years', 'niveles', 'subniveles', 'rols'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->email = $request->email;
        $user->role = $request->role;
        $user->dni = $request->dni;
        $user->calendario_nivel_id = $request->calendario_nivel_id;
        $user->calendario_subnivel_id = $request->calendario_subnivel_id;
        $user->city = $request->city;
        
        
        $user->consentiment_naixement = ($request->consentiment_naixement) ? 1 : 0;
        $user->consentiment_privacitat = ($request->consentiment_privacitat) ? 1 : 0;
        $user->lopd = ($request->lopd) ? 1 : 0;
        $user->address = $request->address;
        $user->fecha_nacimiento = $request->fecha_nacimiento;
        $user->codigo_postal = $request->codigo_postal;
        $user->mobile = $request->mobile;
        $user->phone = $request->phone;

        if ( !empty($request->password) ) $user->password = bcrypt($request->password);

        $user->update();

        return redirect()->route('usuarios.index')->with('message',  __('You have successfully change your information.'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $rows = User::destroy($id);
		if ( $rows > 0 ) {
            // Desasignar user_id a Empleado
            $empleado = Empleado::where('user_id', $id)->first();
            if ( !empty($empleado) ) {
                $empleado->user_id = null;
                $empleado->update();
            }

            return redirect()->back()->with('message', 'Usuario eliminado correctamente.');
        }
        else return redirect()->back()->with('error', 'Ha ocurrido un error al eliminar el usuario.');
    }

    public function perfil() {
        $user = Auth::user();
        return view('admin.perfil', compact('user', $user));
    }

    public function update_avatar(Request $request) {

        /*$request->validate([
            'avatar' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);*/

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        if ( $user->avatar != 'user.jpg' ) {
            Storage::delete('avatars/' . $user->avatar);
        }
        $request->avatar->storeAs('avatars', $avatarName);

        $user->avatar = $avatarName;
        $user->save();

        return back()->with('success', 'You have successfully upload image.');

    }

    public function years_create($id)
    {
        return view('admin.configuracion.usuarios.years.create', compact('id'));
    }

    public function years_store(Request $request)
    {
        $data = $request->all();
        CalendarioUserYear::create($data);
        return redirect(url('/admin/configuracion/usuarios/'.$data['user_id'].'/edit'))->with(['message' => __('Year added successfully')]);
    }

    public function years_edit($id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        return view('admin.configuracion.usuarios.years.edit', array('item' => $item));
    }

    public function years_update(Request $request, $id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect(url('/admin/configuracion/usuarios/'.$data['user_id'].'/edit'))->with(['message' => __('Year updated successfully')]);
    }

    public function years_destroy($id, $user_id)
    {
        $item = CalendarioUserYear::findOrFail($id);
        $item->delete();
        
        return redirect(url('/admin/configuracion/usuarios/'.$user_id.'/edit'))->with(['message' => __('Year deleted successfully')]);
    }

    public function exportar()
    {
        \Excel::create(__('Users'), function($excel) {
            $users = User::where('email', 'NOT LIKE', '%@6tems.com%')->get();

            $excel->sheet('Users', function($sheet) use($users) {
                
                //$sheet->fromArray($users);
                $sheet->row(1, [
                    __('Name'), __('Lastname'), __('Email'), __('Birthdate'), __('ID card'), __('City'),
                    __('Address'), __('ZIP zone'), __('Mobile'), __('Phone'), __('Level'), __('Privacy policy'),
                    __('Consent of birthday')
                ]);
                foreach($users as $index => $user) {
                    $nivel_name = '';
                    if ( $user->nivel()->exists()  ) $nivel_name = $user->nivel->name;
                    else if ( $user->subnivel()->exists() ) $nivel_name = $user->subnivel->name;
                    $sheet->row($index+2, [
                        $user->name, $user->lastname, $user->email, 
                        \Carbon\Carbon::parse($user->fecha_nacimiento)->format('d/m/Y'), 
                        $user->dni, $user->city, $user->address, $user->codigo_postal, $user->mobile, $user->phone,
                        $nivel_name,
                        ($user->consentiment_privacitat ? __('Yes') : __('No')), 
                        ($user->consentiment_naixement ? __('Yes') : __('No')),
                    ]);	
                }
         
            });
         
        })->export('xlsx');
    }

}
