<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\AvatarRequest;
use App\Http\Requests\CanviContrasenyaRequest;
use Auth;
use Storage;
use Hash;

class UsuariosController extends Controller
{
    public function perfil() {
        $user = Auth::user();
        $user->fecha_nacimiento = new \Carbon\Carbon($user->fecha_nacimiento);

        //$user->notify(new \App\Notifications\NuevoUsuarioNotification($user, '123456'));

        return view('usuarios.perfil.edit', compact('user'));
    }

    public function update(Request $request) {
        $user = Auth::user();

        $fecha_nacimiento = new \Carbon\Carbon($request->any . '-' . $request->mes . '-' . $request->dia);

        $user->name = $request->name;
        $user->lastname = $request->lastname;
        $user->dni = $request->dni;
        $user->email = $request->email;
        $user->phone = $request->phone;
        $user->phone = str_replace(__('Phone'), '', $request->phone);
        $user->mobile = $request->mobile;
        $user->address = $request->address;
        $user->codigo_postal = $request->codigo_postal;
        $user->city = $request->city;
        $user->idioma = $request->idioma;
        $user->fecha_nacimiento = $fecha_nacimiento;

        $user->update();

        return back()->with('success', __('You have successfully change your information.') );
    }

    public function update_password(CanviContrasenyaRequest $request)
    {
        $user = Auth::user();

        if ( !Hash::check($request->old_password, $user->password) ) {
            return back()->withErrors(__('validation.confirmed'));
        } else {
            $user->password = bcrypt($request->password);
            $user->update();
            return back()->with('success', __('You have successfully change your password.') );
        }
    }

    public function update_avatar(AvatarRequest $request) {

        $user = Auth::user();

        $avatarName = $user->id.'_avatar'.time().'.'.request()->avatar->getClientOriginalExtension();

        if ( $user->avatar != 'user.jpg' ) {
            Storage::delete('avatars/' . $user->avatar);
        }
        $request->avatar->storeAs('avatars',$avatarName);

        $user->avatar = $avatarName;
        $user->save();

        return back()->with('success', __('You have successfully upload image.'));

    }
}
