<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Opcion;
use DB;

class AdminOpcionesController extends AdminController
{
    public function __construct()
    {
		parent::__construct();
		$this->middleware('admin');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $opciones = Opcion::get()->sortBy('key');

        return view('admin.configuracion.opciones.index', compact('opciones'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
		    'key' => 'required',
		    'value' => 'required',
		]);

        $data = array(
            'key' => $request->key,
            'value' => $request->value,
            'descripcion' => $request->descripcion,
        );

        if ( !Opcion::where('key', '=', $request->key)->exists() ) {
            Opcion::create($data);
        }

        return redirect()->route('opciones.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $key = base64_decode($id);
        Opcion::where('key', '=', $key)->delete();
        
        return response()->json(['success' => $key]);
    }

    public function update(Request $request, $id)
    {
        //
    }

    public function updatevalue(Request $request) {

        if ( Opcion::where('key', '=', $request->pk)->exists() ) {
            $opcion = Opcion::where('key', '=', $request->pk)->first();
            $opcion->value = $request->value;
            $opcion->save();
        }
        return response()->json(['success'=>'Done!']);
    }

    public function updateDesc(Request $request)
    {
        if ( Opcion::where('key', '=', $request->pk)->exists() ) {
            $opcion = Opcion::where('key', '=', $request->pk)->first();
            $opcion->descripcion = $request->value;
            $opcion->save();
        }
        return response()->json(['success'=>'Done!']);
    }


    /**
     * Remove the specified resource from storage.
     * @return Response
    */
    public function updateKey(Request $request) {

        if ( Opcion::where('key', '=', $request->pk)->exists() ) {
            $opcion = Opcion::where('key', '=', $request->pk)->first();
            $opcion->key = $request->value;
            $opcion->save();
        }
        return response()->json(['success'=>'Done!']);
    }
}
