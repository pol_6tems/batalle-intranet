<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rol;

class AdminRolsController extends AdminController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Rol::orderby('name')->paginate(20);
        return View('admin.configuracion.rols.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return View('admin.configuracion.rols.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        Rol::create($data);
        return redirect()->route('configuracion.rols.index')->with(['message' => __('Role added successfully')]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Rol::findOrFail($id);
        return view('admin.configuracion.rols.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Rol::findOrFail($id);
        return view('admin.configuracion.rols.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Rol::findOrFail($id);
        $data = $request->all();
        $item->update($data);
        return redirect()->route('configuracion.rols.index')->with(['message' => __('Role updated successfully')]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Rol::findOrFail($id);
        $item->delete();
        
        return redirect()->route('configuracion.rols.index')->with(['message' => __('Role deleted successfully')]);
    }
}
