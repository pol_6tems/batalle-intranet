<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Session;
use URL;

class LanguageController extends Controller
{
    public function setLocale($locale = 'ca') {

        $locale_actual = app()->getLocale();

        $locales = config('app.locales');
        if ( !in_array($locale, $locales) ) {
            $locale = 'ca';
        }
        session(['locale' => $locale]);
        app()->setLocale($locale);
        \Carbon\Carbon::setLocale( $locale );
        //Session::put('locale', $locale);
        
        // Busca la key de la array de traduccions de la URL actual
        $request_url = str_replace(url('/'), '', URL::previous());
        $request_url_key = $request_url;
        foreach ($this->_languages as $key => $language) {
            $locale_path = base_path() . "/resources/lang/".$language->code.".json";
            if ( file_exists($locale_path) ) {
                $translations = json_decode(file_get_contents($locale_path), true);
                if ( !empty($translations) ) {
                    $key = array_search($request_url, $translations);
                    if ( !empty($key) ) {
                        $request_url_key = $key;
                        break;
                    }
                }
            }
        }
        // Traducció URL actual
        $locale_path = base_path() . "/resources/lang/$locale.json";
        if ( file_exists($locale_path) ) {
            $translations = json_decode(file_get_contents($locale_path), true);
            if ( array_key_exists($request_url_key, $translations) ) {
                return redirect( url( $translations[$request_url_key] ) );
            } else {
                return redirect( url( $request_url_key ) );
            }
        }

        //return redirect(url(URL::previous()));
    }
}