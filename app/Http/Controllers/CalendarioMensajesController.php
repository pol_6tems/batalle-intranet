<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Opcion;
use App\User;
use App\CalendarioMensaje;
use App\Notifications\NuevoMensajeNotification;
use Auth;

class CalendarioMensajesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        
        $mensaje = new CalendarioMensaje();
        $mensaje->user_id = $data['user_id'];
        $mensaje->from = $data['user_id'];
        $mensaje->mensaje = $data['mensaje'];

        $moderadors_calendari_contacte = null;        
        if ( !empty(Opcion::where('key', 'moderadors_calendari_contacte')->first()) ) {
            $moderadors_calendari_contacte = Opcion::where('key', 'moderadors_calendari_contacte')->first()->value;
            if ( !empty($moderadors_calendari_contacte) ) {
                $moderadors = explode('#', $moderadors_calendari_contacte);
                foreach ($moderadors as $mod_email) {
                    $mod_user = User::where('email', $mod_email)->first();
                    if ( !empty($mod_user) ) {
                        $notificacion = new NuevoMensajeNotification($mod_user, $mensaje);
                        $mod_user->notify( $notificacion );
                    }
                }
            }
        }
        
        if ( $mensaje->save() ) echo 'OK';
        else echo 'KO';

        die();
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
