<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Session;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('home');
    }

    public function politica_de_privacitat()
    {
        $user = Auth::user();
        $name = $dni = '';
        if ( !empty($_REQUEST['name']) && !empty($_REQUEST['dni']) ) {
            $name = $_REQUEST['name'];
            $dni = $_REQUEST['dni'];
        } else if ( !empty($user) ) {
            $name = $user->name . ' ' . $user->lastname;
            $dni = $user->dni;
        }
        $locale = !empty( Session::get('locale') ) ? Session::get('locale') : 'es';
        return view('acces.politica_de_privacitat_' . $locale, compact('name', 'dni'));
    }
}
