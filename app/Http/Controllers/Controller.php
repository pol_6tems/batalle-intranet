<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Session;
use App;
use DB;
use View;
use Request;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $locale = app()->getLocale();
        $this->_languages = DB::table('languages')->get()->sortBy('order');
        $url_key = '';
        $urls_trans = array();
        $language_name = 'Català';
        $request_url = Request::getPathInfo();
        
        // Busca l'idioma actual a partir de la URL solicitada
        $locale_trobat = '';
        foreach ($this->_languages as $key => $language) {
            $locale_path = base_path() . "/resources/lang/".$language->code.".json";
            if ( file_exists($locale_path) ) {
                $translations = json_decode(file_get_contents($locale_path), true);
                if ( !empty($translations) ) {
                    $key = array_search($request_url, $translations);
                    if ( !empty($key) ) {
                        $locale_trobat = $language->code;
                        break;
                    }
                }
            }
        }
        if ( !empty($locale_trobat) ) {
            session(['locale' => $locale]);
            app()->setLocale($locale);
            $locale = $locale_trobat;
        }
        
        $locale = Session::get('locale', \Config::get('app.locale'));
        \Carbon\Carbon::setLocale( $locale );

        View::share ( 'urls_trans', $urls_trans );
        View::share ( '_languages', $this->_languages );
        View::share ( 'language_name', $language_name );
        View::share ( 'language_code', $locale );        
    }
}
