<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use DB;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = array();
        
        // Translable fields
        $languages = DB::table('languages')->get();
        foreach ($languages as $key => $lang) {
            $rules[$lang->code]['title'] = 'required';
        }
        
        return $rules;
    }
}
