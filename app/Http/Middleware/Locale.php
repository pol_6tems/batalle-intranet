<?php

namespace App\Http\Middleware;

use Closure;
use Session;
use Config;
use App;
use Request;
use DB;

class Locale
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $locale = Session::get('locale', Config::get('app.locale'));
        session(['locale' => $locale]);
        app()->setLocale($locale);
        \Carbon\Carbon::setLocale( $locale );

        $idioma = DB::table('languages')->where('code', '=', $locale)->first();
        if ( !empty($idioma) ) session(['locale_name' => $idioma->name]);

        return $next($request);
    }
}
