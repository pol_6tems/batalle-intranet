<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $roles = array('admin', 'editor');
        
        if ( Auth::user() && in_array(Auth::user()->role, $roles) ) {
            return $next($request);
        }

        return redirect('/');
    }
}
