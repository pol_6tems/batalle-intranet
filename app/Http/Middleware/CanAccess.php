<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use App\MenuSub;

class CanAccess
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $as = str_replace('App\\Http\\Controllers\\','', request()->route()->getAction()['controller']);
        $pos = strpos($as, '@');
        if ( $pos > 0 ) $as = substr($as, 0, $pos);
        //echo $as;die();
        $exists = MenuSub::whereHas('rol', function ($q) {
			$q->where('level', '<=', Auth::user()->rol->level);
		})->where('controller', '=', $as)->exists();

        if ( $exists ) return $next($request);
        else return abort(403);
    }
}
