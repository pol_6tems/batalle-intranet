<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Post extends Model
{
    use SoftDeletes;
    use \Dimsav\Translatable\Translatable;

    public $translatedAttributes = ['title', 'description', 'post_name'];
    
    protected $fillable = ['type', 'status'];
    
    protected $dates = ['deleted_at'];

    public function get_url()
    {
        return url('/') . $this->post_name;
    }

}
