<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\CalendarioNivelYear;
use App\CalendarioSubnivel;
use App\User;
use App\CalendarioNivelWeek;

class CalendarioNivel extends Model
{
    use SoftDeletes;

    protected $table = 'calendario_niveles';
    
    protected $fillable = ['name', 'status', 'sel_dia_o_semana', 'no_bloquejat', 'max_limit'];
    
    protected $dates = ['deleted_at'];

    public function years()
    {
        return $this->hasMany(CalendarioNivelYear::class);
    }

    public function subniveles()
    {
        return $this->hasMany(CalendarioSubnivel::class);
    }

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function weeks()
    {
        return $this->hasMany(CalendarioNivelWeek::class);
    }

    public function get_persones_max()
    {
        $persones_max = array('general' => $this->max_limit);
        if ( !empty($this->weeks) ) {
            foreach ($this->weeks as $item) {
                $persones_max[$item->year . '-' . $item->week] = $item->max_limit;
            }
        }
        return $persones_max;
    }
}
