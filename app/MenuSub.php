<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Rol;
use App\Menu;

class MenuSub extends Model
{
    protected $table = 'menus_sub';
    
    protected $fillable = ['menu_id', 'rol_id', 'name', 'url_type', 'url', 'requests', 'order', 'controller'];

    public function menu()
    {
        return $this->belongsTo(Menu::class, 'id', 'menu_id');
    }

    public function rol()
    {
        return $this->belongsTo(Rol::class, 'rol_id', 'id');
    }

    public function get_requests() {
        return explode(',', $this->requests);
    }
}
