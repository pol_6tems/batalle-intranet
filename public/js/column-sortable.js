/*
var $sortby = '{{ $sortby }}';
var $order = '{{ $order }}';
*/

$(document).ready(function() {
    var $glyphicon = 'glyphicon-triangle-bottom';
    if ( $order == 'ASC' ) $glyphicon = 'glyphicon-triangle-top';
    var $span = '<span class="sortable pl-1 glyphicon ' + $glyphicon + '"></span>';
    $(".sortable[data-sortby='" + $sortby +"']").attr('data-order', $order);
    $(".sortable[data-sortby='" + $sortby +"']").append( $span );
});
$('.sortable').on('click', function(){
    $sortby = $(this).data('sortby');
    $order = $(this).data('order');
    if ( typeof($order) !== 'undefined' ) $order = $order == 'ASC' ? 'DESC' : 'ASC';
    else $order = 'ASC';
    $(this).attr('data-order', $order);
    parse_sort_link($sortby, $order);
});
function parse_sort_link($sortby, $order) {
    var $url = window.location.href;
    $url = updateURLParameter($url, 'sortby', $sortby);
    $url = updateURLParameter($url, 'order', $order);
    if ( $search != '' ) $url = updateURLParameter($url, 'search', $search);
    window.location.href = $url;
}
function updateURLParameter(url, param, paramVal) {
    var newAdditionalURL = "";
    var tempArray = url.split("?");
    var baseURL = tempArray[0];
    var additionalURL = tempArray[1];
    var temp = "";
    if (additionalURL) {
        tempArray = additionalURL.split("&");
        for (var i=0; i<tempArray.length; i++){
            if(tempArray[i].split('=')[0] != param){
                newAdditionalURL += temp + tempArray[i];
                temp = "&";
            }
        }
    }

    var rows_txt = temp + "" + param + "=" + paramVal;
    return baseURL + "?" + newAdditionalURL + rows_txt;
}