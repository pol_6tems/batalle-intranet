@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	#{{$item->codigo}} - {{$item->nombre}}
</h1>
@endsection

@section('content')
<div class="jumbotron">
        <h2>@Lang('Employee')</h2>
        <p class="pl-4">
            <strong>@lang('ID card'):</strong> {{ $item->dni }}<br>
            <strong>@lang('Gender'):</strong> {{ $item->sexo }}<br>
            <strong>@lang('Birthdate'):</strong> {{ !empty($item->fecha_nacimiento) ? $item->fecha_nacimiento->format('d/m/Y') : '-' }}<br>
            <strong>@lang('Entry date'):</strong> {{ !empty($item->fecha_alta) ? $item->fecha_alta->format('d/m/Y') : '-' }}<br>
            <strong>@lang('Leaving date'):</strong> {{ !empty($item->fecha_baja) ? $item->fecha_baja->format('d/m/Y') : '-' }}<br>
            <strong>@lang('Level'):</strong> {{ !empty($item->nivel) ? $item->nivel->name : '-' }}<br>
            {{--<strong>@lang('Category'):</strong> {{ $item->categoria->nombre }}<br>--}}
        </p>
        
        @if ( $item->user_id && !empty($item->usuario()) )
        @php( $usuario = $item->usuario()->first() )
        <h2>@Lang('User')</h2>
        <p class="pl-4">
            <strong>@lang('Id'):</strong> {{ $usuario->id }}<br>
            <strong>@lang('Email'):</strong> {{ $usuario->email }}<br>
            <strong>@lang('Created at'):</strong> {{ $usuario->created_at->format('g:i d/m/Y') }}<br>
            <strong>@lang('Role'):</strong> {{ __(ucfirst($usuario->role)) }}<br>
            <strong>@lang('Level'):</strong> {{ $usuario->nivel()->exists() ? $usuario->nivel->name : '-' }}<br>
            <strong>@lang('Sublevel'):</strong> {{ $usuario->subnivel()->exists() ? $usuario->subnivel->name : '-' }}<br>
        </p>
        @endif
    </div>
	<button type="button" class="btn btn-raised btn-secondary" onclick="window.location.href='{{route('empleados.index')}}'">@Lang('Return')</button>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection