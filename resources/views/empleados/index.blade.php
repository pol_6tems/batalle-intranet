@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	@lang('Employees')
	<a href="{{ route('empleados.create') }}">
		<i class="material-icons pr-2 admin-menu-icona add_circle"></i>
	</a>
	<a href="javascript:void(0);" onclick="$('#importar_empleados_file').trigger('click');" class="btn btn-secondary">
		@lang('import')
	</a>
</h1>
<form id="importar_empleados_form" action="{{route('empleados.importar')}}" method="post" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="file" id="importar_empleados_file" name="importar_empleados_file" style="display:none" accept=".csv"/>
</form>
<script>
document.getElementById("importar_empleados_file").onchange = function() {
	document.getElementById("importar_empleados_form").submit();
};
</script>
@endsection

@section('content')

<form class="form-inline" method="GET" action="{{ route('empleados.index') }}">
    {!! csrf_field() !!}
    <div class="form-group mr-4">
		<input type="hidden" name="sortby" value="{{$sortby}}">
		<input type="hidden" name="order" value="{{$order}}">
        <label for="search" class="bmd-label-floating">@Lang('Search'):</label>
        <input type="text" name="search" class="form-control" placeholder="" value="{{$search}}">
    </div>
    <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
	<button type="submit" class="btn btn-default btn-raised">@Lang('Search')</button>
	&nbsp;&nbsp;&nbsp;&nbsp;
	<button type="button" onclick="window.location.href='{{route('empleados.index')}}'" class="btn btn-secondary btn-raised">@Lang('Reset')</button>
    </span>
</form>

<table id="taula" class="table table-bordered table-striped table-sortable">
	<thead>
		<tr>
			<th width="5%"><input type="checkbox" class="checkbox_all"></th>
			<th class="sortable" data-sortby="codigo" scope="col">@lang('Code')</th>
			<th class="sortable" data-sortby="nombre" scope="col">@lang('Name')</th>
			<th class="sortable" data-sortby="dni" scope="col">@lang('ID card')</th>
			<th class="sortable" data-sortby="categoria_id" scope="col">@lang('Category')</th>
			<th class="sortable" data-sortby="calendario_nivel_id" scope="col">@lang('Level')</th>
			<th class="sortable" data-sortby="created_at" scope="col">@lang('Created at')</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	@forelse($empleados as $item)
		<tr>
			<td><input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" /></td>
			<td>{{ $item->codigo }}</td>
			<td>
				<a href="{{route('empleados.show', $item->id)}}">
					{{ $item->nombre }}
				</a>
			</td>
			<td>{{ $item->dni }}</td>
			<td>{{ (!empty($item->categoria) ? $item->categoria->nombre : '-') }}</td>
			<td>{{ (!empty($item->nivel) ? $item->nivel->name : '-') }}</td>
			<td>{{ $item->created_at }}</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{route('empleados.edit', $item->id)}}">Editar</a>
						<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{$item->id}}" data-nombre="{{$item->nombre}}">Eliminar</button>
					</div>
				</div>
			</td>
		</tr>
	@empty
		<tr>
			<td colspan="7">@Lang('No entries found.')</td>
		</tr>
	@endforelse
	
	</tbody>
</table>

{{ $links }}

<form action="{{ route('empleados.mass_destroy') }}" method="post" onsubmit="return confirm('Are you sure?');">
	{{ csrf_field() }}
	<input type="hidden" name="_method" value="DELETE">
	<input type="hidden" name="ids" id="ids" value="" />
	<input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
</form>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script src="{{ asset('js/column-sortable.js') }}"></script>
<script>
	var $sortby = '{{ $sortby }}';
	var $order = '{{ $order }}';
	var $search = '{{ $search }}';
	
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('¿Estás seguro de eliminarlo?')) {
            $(e.target).closest('form').submit();
        }
    });
	
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', '/admin/empleados/' + id);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});
</script>
@endsection