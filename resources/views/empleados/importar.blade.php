@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">import_export</i>
	@lang('Import Users')
</h1>
@endsection

@section('content')
<form action="{{url('perfil')}}" method="post" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<div class="form-group">
		<input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
		<small id="fileHelp" class="form-text text-muted">@lang('Please upload a valid image file. Size of image should not be more than 2MB.')</small>
	</div>
	<button type="submit" class="btn btn-primary">@lang('Send')</button>
</form>

<form id="importar_usuarios_form" action="{{url('/admin/configuracion/usuarios/importar')}}" method="post" enctype="multipart/form-data">
	{!! csrf_field() !!}
	<input type="file" id="importar_usuarios_file" name="importar_usuarios_file" style="display:none" accept=".csv"/>
</form>
<script>
document.getElementById("importar_facturas_file").onchange = function() {
	document.getElementById("importar_facturas_form").submit();
};
</script>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', '/admin/configuracion/usuarios/' + id);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});

	$.ajax({
		url: "{{url('/admin/configuracion/usuarios/importar')}}",
		method: 'IMPORTAR',
		success: function(data) {
			cObj.parents("tr").remove();
			alert("Your imaginary file has been deleted.");
		}
	});
</script>
@endsection