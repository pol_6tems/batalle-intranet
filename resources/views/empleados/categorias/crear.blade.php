@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	Nueva categoría
</h1>
@endsection

@section('content')
<form method="POST" action="{{url('/empleados/categorias')}}">
    {!! csrf_field() !!}
	<div class="form-group">
		<label for="nombre" class="bmd-label-floating">Nombre</label>
		<input type="text" class="form-control" id="nombre" name="nombre" value="{{ old('nombre') }}">
		<div class="text-danger">{{$errors->first('nombre')}}</div>
	</div>
	<button type="button" class="btn btn-default" onclick="window.location.href='{{url('empleados/categorias')}}'">Cancelar</button>
	<button type="submit" class="btn btn-primary btn-raised">Crear</button>
</form>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection