@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	@lang('Categories')
	<a href="{{ url('/empleados/categorias/create') }}">
		<i class="material-icons pr-2 admin-menu-icona add_circle"></i>
	</a>
</h1>
@endsection

@section('content')

<table id="taula" class="table table-bordered table-striped table-sortable">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">@lang('Name')</th>
			<th scope="col">@lang('Created at')</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	@foreach($categorias as $item)
		<tr>
			<th scope="row">{{ $item->id }}</th>
			<td>{{ $item->nombre }}</td>
			<td>{{ $item->created_at }}</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{url('/empleados/categorias/create')}}">Crear</a>
						<a class="dropdown-item" href="{{action('CategoriasEmpleadoController@edit', $item->id)}}">Editar</a>
						<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{$item->id}}" data-nombre="{{$item->nombre}}">Eliminar</button>
					</div>
				</div>
			</td>
		</tr>
	@endforeach
	
	</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Eliminar</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('¿Estás seguro de eliminarlo?')) {
            $(e.target).closest('form').submit();
        }
    });
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', '/empleados/categorias/' + id);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});
</script>
@endsection