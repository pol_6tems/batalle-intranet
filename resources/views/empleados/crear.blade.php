@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	Nuevo empleado
</h1>
@endsection

@section('content')
<form method="POST" action="{{route('empleados.store')}}">
    {!! csrf_field() !!}
	<div class="form-row">
		<div class="form-group col-md-4">
			<label for="codigo" class="bmd-label-floating">@Lang('Code')</label>
			<input type="text" class="form-control" name="codigo" value="{{ old('codigo') }}" required>
			<div class="text-danger">{{$errors->first('codigo')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="nombre" class="bmd-label-floating">@Lang('Name')</label>
			<input type="text" class="form-control" name="nombre" value="{{ old('nombre') }}" required>
			<div class="text-danger">{{$errors->first('nombre')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="dni" class="bmd-label-floating">@Lang('ID card')</label>
			<input type="text" class="form-control" name="dni" value="{{ old('dni') }}" required>
			<div class="text-danger">{{$errors->first('dni')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="fecha_nacimiento" class="bmd-label-floating">@Lang('Birthday')</label>
			<input type="date" class="form-control" name="fecha_nacimiento" value="{{ old('fecha_nacimiento') }}">
			<div class="text-danger">{{$errors->first('fecha_nacimiento')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="fecha_alta" class="bmd-label-floating">@Lang('Entry date')</label>
			<input type="date" class="form-control" name="fecha_alta" value="{{ old('fecha_alta') }}">
			<div class="text-danger">{{$errors->first('fecha_alta')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="fecha_baja" class="bmd-label-floating">@Lang('Leaving date')</label>
			<input type="date" class="form-control" name="fecha_baja" value="{{ old('fecha_baja') }}">
			<div class="text-danger">{{$errors->first('fecha_baja')}}</div>
		</div>
		<div class="form-group col-md-4">
			<label for="sexo" class="bmd-label-floating">@Lang('Gender')</label>
			<select name="sexo" class="custom-select col-md-12" data-live-search="true">
				<option value="Hombre">@Lang('Male')</option>
				<option value="Mujer">@Lang('Female')</option>
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="categoria_id" class="bmd-label-floating">@Lang('Category')</label>
			<select name="categoria_id" class="custom-select col-md-12" data-live-search="true">
			@foreach ($categorias as $categoria)
				<option value="{{$categoria->id}}" {{ ($categoria->id == 1 ? 'selected' : '') }}>{{$categoria->nombre}}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="calendario_nivel_id" class="bmd-label-floating">@Lang('Level')</label>
			<select name="calendario_nivel_id" class="custom-select col-md-12" data-live-search="true">
			@foreach ($niveles as $nivel)
				<option value="{{$nivel->id}}" {{ ($nivel->id == 1 ? 'selected' : '') }}>{{$nivel->name}}</option>
			@endforeach
			</select>
		</div>
		<div class="form-group col-md-4">
			<label for="user_id" class="bmd-label-floating">@Lang('User')</label>
			<select name="user_id" class="custom-select col-md-12" data-live-search="true" title="@Lang('User')">
			@foreach ($users as $user)
				<option value="{{$user->id}}">{{$user->name}} ({{$user->email}})</option>
			@endforeach
			</select>
		</div>
	</div>
	<br/><br/>
	<button type="button" class="btn btn-default" onclick="window.location.href='{{route('empleados.index')}}'">@Lang('Cancel')</button>
	<button type="submit" class="btn btn-primary btn-raised">@Lang('Create')</button>
</form>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
@endsection