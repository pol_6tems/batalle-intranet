@extends('layouts.frontend')

@section('page')
<section class="single-page register-done">
    <div class="area2">
        <img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" class="logo">
        <p class="quote">
            @Lang('benvingut-titol')
        </p>
        <p class="quote">
            @Lang('benvingut-text')
        </p>			
        <button type="submit" onclick="window.location.href='{{url('/')}}'" style="text-transform: uppercase;">@Lang('Private area')</button>
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
<script src="{{ asset('js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.register-done').css('min-height', setHeight);	
    }
            
        
    $(document).ready(function() {
        $(".select_box").selectbox();
        setHeight()
    });

    $(window).resize(function(){
        setHeight()
    });
</script>
@endsection