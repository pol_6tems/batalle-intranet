@extends('layouts.frontend')

@section('page')
<section class="single-page">
    <div class="form-register">
    <form id="registre_form" action="{{ route('acces.crear_user') }}" method="post">
        {{ csrf_field() }}
        <img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" class="logo">
        <p class="quote">
            {!! __('acces-registre-titol') !!}
        </p>
        @if ($errors->count() > 0)
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        @endif
        <ul class="form-container">
            <li>
                <label>@Lang('Name')*:</label>
                <input required id="name" name="name" style="color:#000;" class="frmCrtl" type="text" value="{{$empleado->nombre}}" onBlur="if (this.value == ''){this.value = '@Lang('Name')'; }" onFocus="if (this.value == '@Lang('Name')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('Lastname')*:</label>
                <input required id="lastname" name="lastname" style="color:#000;" class="frmCrtl" type="text" value="{{$empleado->apellido}}" onBlur="if (this.value == ''){this.value = '@Lang('Lastname')'; }" onFocus="if (this.value == '@Lang('Lastname')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('ID card')*:</label>
                <input required id="dni" name="dni" style="color:#000;" class="frmCrtl" type="text" value="{{$empleado->dni}}" onBlur="if (this.value == ''){this.value = '@Lang('ID card')'; }" onFocus="if (this.value == '@Lang('ID card')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('Email')*:</label>
                <input required id="email" name="email" class="frmCrtl" type="text" value="@Lang('Email')" onBlur="if (this.value == ''){this.value = '@Lang('Email')'; }" onFocus="if (this.value == '@Lang('Email')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('Phone'):</label>
                <input name="phone" class="frmCrtl" type="text" value="@Lang('Phone')" onBlur="if (this.value == ''){this.value = '@Lang('Phone')'; }" onFocus="if (this.value == '@Lang('Phone')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('Mobile')*:</label>
                <input id="mobile" name="mobile" class="frmCrtl" type="text" value="@Lang('Mobile')" onBlur="if (this.value == ''){this.value = '@Lang('Mobile')'; }" onFocus="if (this.value == '@Lang('Mobile')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('Address')*:</label>
                <input id="address" name="address" class="frmCrtl" type="text" value="@Lang('Address')" onBlur="if (this.value == ''){this.value = '@Lang('Address')'; }" onFocus="if (this.value == '@Lang('Address')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('ZIP zone')*:</label>
                <input id="codigo_postal" name="codigo_postal" class="frmCrtl" type="text" value="@Lang('ZIP zone')" onBlur="if (this.value == ''){this.value = '@Lang('ZIP zone')'; }" onFocus="if (this.value == '@Lang('ZIP zone')') {this.value = ''; }">
            </li>
            <li>
                <label>@Lang('City')*:</label>
                <input id="city" name="city" class="frmCrtl" type="text" value="@Lang('City')" onBlur="if (this.value == ''){this.value = '@Lang('City')'; }" onFocus="if (this.value == '@Lang('City')') {this.value = ''; }">
            </li>				
            <li>
                <label>@Lang('Language')*:</label>
                <div class="select-data full-width select-language">
                    <select name="idioma" class="select_box">
                    @foreach ($_languages as $language)
                        <option {{ ($language->code == $language_code ? 'selected' : '') }} value="{{$language->code}}">@lang($language->name)</option>
                    @endforeach
                    </select>
                </div>					
            </li>
            <li>
                <label>@Lang('Birthdate')*:</label>
                <div class="full-width">
                    <div class="select-data scrollable">
                        <select name="dia" class="select_box" name="dia">
                        @for ($i = 1; $i <= 31; $i++)
                            <option {{ ($empleado->fecha_nacimiento->day == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                    <div class="select-data scrollable">
                        <select name="mes" class="select_box" name="mes">
                        @for ($i = 1; $i <= 12; $i++)
                            <option {{ ($empleado->fecha_nacimiento->month == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                    <div class="select-data scrollable">
                        <select name="any" class="select_box" name="any">
                        @for ($i = date('Y'); $i >= date('Y') - 80; $i--)
                            <option {{ ($empleado->fecha_nacimiento->year == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                </div>
            </li>
            </ul>
            
            <div style="float: left;width: 100%;position: relative;">
                <label style="font-weight: 400;font-size: 13.5px;color: #000;float: left;position: relative;width: 100%;text-align: left;display: block;margin: 30px 0 30px;cursor: pointer;padding-right: 35px;">
                    @Lang('Would you like Batallé to congratulate you for your birthday?')
                </label>
            </div>
            <div class="checkbox-big">
                <input id="consentiment_naixement_si" name="consentiment_naixement_si" type="checkbox">
                <label for="consentiment_naixement_si" style="color: #000;margin: 0;margin-bottom: 25px;width: 50%;margin-left: 50%;">
                    @Lang('Yes')
                </label>
            </div>
            <div class="checkbox-big">
                <input id="consentiment_naixement_no" name="consentiment_naixement_no" type="checkbox" checked>
                <label for="consentiment_naixement_no" style="color: #000;margin: 0;width: 50%;margin-left: 50%;">
                    @Lang('No')
                </label>
            </div>

            <ul>
            <li class="password">
                <label style="height: 30px;padding-top: 10px;">@Lang('Password')*:</label>
                <input id="password" name="password" class="frmCrtl" type="password" placeholder="@Lang('Password')">
                <span class=""></span>
            </li>
            <li class="password"> 
            <label style="height: 30px;padding-top: 10px;">@Lang('Repeat password')*:</label>
                <input id="password2" class="frmCrtl" type="password" placeholder="@Lang('Repeat password')">
                <span class=""></span>
            </li>
        </ul>

        <div class="checkbox-big">
            <input id="lopd" name="lopd" type="checkbox">
            <label for="lopd">
                @lang('registre-footer-politica', ['url' => url(__('/politica-de-privacitat'))])
            </label>
        </div>

        <div class="dono_consentiment_si check-box3">
            <input name="dono_consentiment_si" type="checkbox" id="c1">
            <label for="c1"> <span></span> @Lang('I consent to the processing of my image in accordance with the advertising purposes described above.')</label>
        </div>
        <div class="dono_consentiment_no check-box3">
            <input name="dono_consentiment_no" type="checkbox" checked id="c2">
            <label for="c2"><span></span> @Lang('I do not give my consent for the treatment of my image in accordance with the advertising purposes described above.')</label>
        </div>
        <button type="submit">@Lang('Next')</button>
    </div>
</section>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">@Lang('Notice')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <ul class="lista"></ul>
            </div>
            <div class="modal-footer"></div>
			<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Accept')</button>
		</div>
	</div>
</div>
@endsection

@section('styles_grandfather')
<link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('styles')
<style>
    .select-data .sbHolder .sbSelector { color: #000; }
    .dono_consentiment_si, .dono_consentiment_no { display: none; }
</style>
@endsection

@section("scripts")
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
<script src="{{ asset('js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
function anar_a_politica() {
    var url = $('#link-politica').data('url');
    var dni = $('#dni').val();
    var name = $('#name').val()  + ' ' + $('#lastname').val();
    window.location.href = url + '?dni=' + dni + '&name=' + name;
}
$(document).ready(function() {
	$(".select_box").selectbox();
	//Password / text toggle
	$('.password span').click(function(){
		var getClass = $(this).attr("class");
		$(this).toggleClass('showPassword');
		if(getClass == "showPassword")
		{
			$(this).siblings('.bmd-form-group').children('.frmCrtl').attr("type", "password")
		}
		else
		{
			$(this).siblings('.bmd-form-group').children('.frmCrtl').attr("type", "text")
		}
	});//End of Password / text toggle
	
	//block on focuss
	$('.frmCrtl').keydown(function(e){
		var a = e.which;
		//console.log(a);
		if(a !== 9)
		{
			$(this).css('color', '#000');
		}
    });
    
	
	$('.sbHolder .sbOptions li a').click(function(){
        $(this).parents('.sbHolder').find('.sbSelector').css('color', '#000');
    });
    
	$('#lopd').change(function() {
        if ( this.checked ) {
            $('.dono_consentiment_si').show();
            $('.dono_consentiment_no').show();
        } else {
            $('.dono_consentiment_si').hide();
            $('.dono_consentiment_no').hide();
        }
    });
    $('#c1').click(function() {
        if ( this.checked ) {
            $('#c2')[0].checked = false;
        } else {
            $('#c2')[0].checked = true;
        }
    });
    $('#c2').click(function() {
        if ( this.checked ) {
            $('#c1')[0].checked = false;
        } else {
            $('#c1')[0].checked = true;
        }
    });
    $('#consentiment_naixement_si').click(function() {
        if ( this.checked ) {
            $('#consentiment_naixement_no')[0].checked = false;
        } else {
            $('#consentiment_naixement_no')[0].checked = true;
        }
    });
    $('#consentiment_naixement_no').click(function() {
        if ( this.checked ) {
            $('#consentiment_naixement_si')[0].checked = false;
        } else {
            $('#consentiment_naixement_si')[0].checked = true;
        }
    });
    
    $('#registre_form').submit(function(e){
        $errors = [];
        $name = $('#name').val();
        $lastname = $('#lastname').val();
        $email = $('#email').val();
        $dni = $('#dni').val();
        $mobile = $('#mobile').val().replace('@Lang('Mobile')', '');
        $address = $('#address').val().replace('@Lang('Address')', '');
        $codigo_postal = $('#codigo_postal').val().replace('@Lang('ZIP zone')', '');
        $city = $('#city').val().replace('@Lang('City')', '');
        $lopd = $('#lopd');
        $password = $('#password').val();
        $password2 = $('#password2').val();
        if ( $name == '' ) $errors.push("@Lang('Name is empty.')");
        if ( $lastname == '' ) $errors.push("@Lang('Lastname is empty.')");
        if ( !isEmail($email) ) $errors.push("@Lang('Email is not correct.')");
        if ( $dni == '' ) $errors.push("@Lang('ID card is empty.')");
        if ( $mobile == '' ) $errors.push("@Lang('Mobile') @Lang('is empty.')");
        if ( $address == '' ) $errors.push("@Lang('Address') @Lang('is empty.')");
        if ( $codigo_postal == '' ) $errors.push("@Lang('ZIP zone') @Lang('is empty.')");
        if ( $city == '' ) $errors.push("@Lang('City') @Lang('is empty.')");
        if ( !$lopd.is(':checked') ) $errors.push("@Lang('You have not accepted the LOPD')");
        if ( $password == '' ) {
            $errors.push("@Lang('Password is empty.')");
        } else if ( $password != $password2 ) {
            $errors.push("@Lang('Passwords do not match.')");
        }

        if ( $errors.length > 0 ) {
            var modal = $('#myModal');
            var html = '';
            for ($i = 0; $i < $errors.length; $i++) {
                html += "<li>" + $errors[$i] + "</li>";
            }
            modal.find('.modal-body .lista').html(html);
            $('#myModal').modal('show');
            
            e.preventDefault();
            return false;
        } else return true;
    });
});
function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}
</script>
@endsection