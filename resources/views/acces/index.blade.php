@extends('layouts.registre_login')

@section('content')
<section class="area-container">
	<div class="area-content">
		<h2>@Lang('Private area')</h2>
		<form method="get" action="{{ route('acces.dni') }}">
			<img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" alt="BATALLE-INTRANET-logo-VERMELL">
			<h3>@Lang('Sign up for the workers area')</h3>
			@if (session('message'))
				<div class="alert alert-info">{{ session('message') }}</div>
				<br/>
			@endif
			<input type="text" name="dni" class="frm-ctrl" Value="@Lang('DNI')"  onBlur="if (this.value == ''){this.value = '@Lang('DNI')'; }" onFocus="if (this.value == '@Lang('DNI')') {this.value = ''; }">
			<div class="area-style">
				<select onchange='window.location.href = "{{ url('/locale') }}/" + this.value;'>
					@foreach ($_languages as $language)
                        <option value="{{$language->code}}" {{$language->code == app()->getLocale() ? 'selected' : ''}}>@lang($language->name)</option>
                    @endforeach
				</select>
			</div>
			<button type="submit">@Lang('Enter')</button>
			<div class="clear"></div>
			<p>@lang('acces-footer', ['url' => url(__('/politica-de-privacitat'))])</p>
		</form>
	</div>
</section>
@endsection

@section("scripts")

@endsection