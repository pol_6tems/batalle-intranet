@extends('layouts.frontend')

@section('page')
<section class="single-page">
    <div class="terms">
        <img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" class="logo">
        <p class="quote">
            Protecció de dades de caracter personal compromís de <span>confidencialitat treballadors.<span>
        </p>
        <p>
        @if( !empty($name) && !empty($dni) )
            El sotasignat, <strong>{{$name}},</strong>  amb DNI/NIE <strong>{{$dni}}</strong>,<br>
        @endif
        @if( !empty($name) && !empty($dni) )
            en  el  marc  de  la  relació  laboral  que  l'uneix  amb  CARNICA  BATALLÉ  SA,  es compromet a:</p>
        @else
            En  el  marc  de  la  relació  laboral  que  l'uneix  amb  CARNICA  BATALLÉ  SA,  es compromet a:</p>
        @endif
        <p><span>-</span>	No revelar a cap persona aliena a CARNICA BATALLÉ SA, sense el consentiment degut, informació a la qual hagi tingut accés per raó de la seva feina, excepte en aquells casos en els quals sigui necessari per desenvolupar la  seva  tasca  o  per  haver-se-li  requerit  per  mandat  legal  o  de  l'autoritat competent.<br>
        <span>-</span>	Utilitzar  la  informació  que  s'esmenta  en  l'apartat  anterior  únicament  de  la manera que exigeix el desenvolupament de les seves funcions a CARNICA BATALLÉ SA. No utilitzar de cap altra manera qualsevol informació que hagi pogut  obtenir  utilitzant la seva  condició  d'empleat  i  que  no sigui necessaria pel desenvolupament de les seves funcions.<br>
        <span>-</span>	Complir, en el desenvolupament de les seves funcions, la normativa vigent, autonómica, estatal o comunitaria, relativa a la Protecció de Dades de Caracter Personal, i en particular, la Llei Organica 15/1999, de 13 de desembre i el Reglament  (UE)  2016/679,  de  27  d'abril  de 2016, relatiu a la protecció de les persones físiques pel que fa al tractament de les Dades Personals i a la lliure circulació d'aquestes.<br>
        <span>-</span>	Complir els compromisos anteriors fins i tot un cop extingida, per qualsevol causa, la relació laboral que l'uneix a CARNICA BATALLÉ SA.</p>
        
        <p>L'empleat  declara  així  mateix  saber  que  l'incompliment  d'aquest  compromís  pot generar  l'exercici  d'accions  disciplinaries  per  part  de  CARNICA  BATALLÉ  SA,  tal com s'estableix a l'art. 58 de l'Estatut General dels Treballadors.</p>
        
        <p>De conformitat amb el Reglament (UE) 2016/679, de 27 d'abril de 2016, CARNICA BATALLÉ SA informa al treballador que les seves dades personals incloses en el contracte laboral, en el currículum vitae o en qualsevol altre document derivat de la relació laboral que mantenim, seran incloses en els fitxers LABORAL i RECURSOS HUMANS, creats i custodiats sota la responsabilitat de CARNICA BATALLÉ  SA.  L'empleat  manifesta  que  facilita  voluntariament  aquestes  dades, inclosa la seva imatge, amb la finalitat d'establir una relació laboral amb l'empresa. L'informem que la seva imatge sera utilitzada amb la finalitat de poder identificar- lo com a treballador de l'empresa i, en segon terme, també l'informem que aquesta podra ser captada durant el procés de control de la producció i per mantenir la seguretat a l'interior de les nostres instal•lacions.</p>
        
        <p>L'informem que les seves dades poden ser cedides a les empreses que formen part del grup empresarial o comunicades a empreses col·laboradores per poder fer efectiva i millorar la relació de caracter laboral que ens uneix, com, per exemple,</p>
        
        <p>dur a terme accions formatives, així com les gestions própies del departament de Recursos Humans i Comptabilitat.</p>
        
        <p>Pel  que  fa  a  la  imatge,  d'acord  amb  el  que  disposa  la  Instrucció  1/2006,  de  12  de desembre, i el mateix Reglament (UE) 2016/679, sobre videovigilancia, sol·licitem el seu consentiment per poder utilitzar la seva imatge, si fos el cas, en campanyes publicitaries dels nostres productes, a través de la pagina web corporativa, catalegs o qualsevol altre mitja audiovisual que pugui gravar-la o reproduir-la d'acord amb les finalitats que s'acaben d'esmentar.</p>
        
        <p>D'altra banda, en el cas de dur a terme la tasca de xofer o repartidor dels nostres productes,  l'informem  que  els  vehicles  del  GRUP  BATALLÉ  que  es  posen  a  la  seva disposició tenen instal•lat un sistema de geolocalització que informa sobre la seva ubicació en temps real amb la finalitat de controlar la situació i seguretat dels nostres treballadors i vehicles.</p>
        
        <p>En el cas de que modifiqui les seves dades, sol•licitem que ens ho comuniqui per escrit amb la finalitat de mantenir-les actualitzades.</p>
        
        <p>Podra  exercir  els  seus  drets  d'accés,  informació,  rectificació,  supressió,  oposició, limitació  i  portabilitat,  d'acord  amb  el  que  estableix  la  normativa  legal,  a  l'adre<a: CARNICA BATALLÉ SA; AVDA. SEGADORS S/N RIUDARENES 17421 GIRONA, o per correu electrónic a l'adre<a: rgpd@batalle.com.</p>
        
        <button type="submit" onClick="window.history.back();">
        @if ( Auth::check() )
            Tornar
        @else
            Accepto
        @endif
        </button>
    </div>
</section>
@endsection

@section('scripts_parent')
<script src="{{ asset('js/maqueta/jquery.min.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.relative-pad').css('min-height', setHeight);	
    }

    $(document).ready(function() {	
		setHeight();
		$(".select_box").selectbox();
    });

    $(window).resize(function(){
        setHeight();
    });
</script>
@endsection