@extends('layouts.app')

@section('header')
<div class="title-head calen-ico">
	<h1>@Lang('Contact')</h1>
</div>
@endsection

@section('content')
<div class="cal-cnt pad-left cate">  
  <div class="row1">  
    <div class="cal-lft contact-l">
      <h2>Contacte amb nosaltres</h2>
      <p>Envia’ns la teva consulta, petició i/o dubte i et respondrem al més aviat possible. </p>
    </div>
  
    <div class="cal-rht contact-r">                
      <div class="message mCustomScrollbar">
				<ul>
						<!-- Missatge inicial benvinguda -->
						<li>
								<div class="msg-l">
										<img src="{{asset('images/BATALLE-INTANET-icona-usuari.png')}}">
								</div>                        
								<div class="msg-r">
										<h4>Batallé<span></span></h4>
										<p>{!! __('calendari-mensajes-benvinguda') !!}</p>
								</div>                        
						</li>
						<!-- end Missatge inicial benvinguda -->
						
						@php( $last_date = null )
						@foreach( $mensajes as $key => $mensaje )
								@if ( $key == 0 )
										@php( $last_date = $mensaje->created_at->format('M d') )
										<li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
								@elseif ( $last_date != $mensaje->created_at->format('M d') )
										<li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
								@endif
						<li class="message-line">
								<div class="msg-l">
										@if( Auth::user()->id == $mensaje->from )
												<img src="/storage/avatars/{{ Auth::user()->avatar }}">
										@else
												<img src="{{asset('images/BATALLE-INTANET-icona-usuari.png')}}">
										@endif
								</div>                        
								<div class="msg-r">
										<h4>
												@if( Auth::user()->id == $mensaje->from )
														{{ Auth::user()->name }} {{ Auth::user()->lastname }}
												@else
														Batallé
												@endif
												<span>{{ $mensaje->created_at->format('G:i') }}</span>
										</h4>
										<p>{{ $mensaje->mensaje }}</p>
										@if ($mensaje->fitxer != '')
											<span class="fitxer"><a href="{{ asset('storage/'.$mensaje->fitxer) }}"><i class="material-icons">description</i></a></span>
										@endif
								</div>                        
						</li>
								@php( $last_date = $mensaje->created_at->format('M d') )
						@endforeach
				</ul>
			</div>
			<div class="form-pad">
				<ul>
						<li class="textarea">
								<textarea id="mensaje-box" class="txt-box" name="Escriu el teu comentari..." onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">Escriu el teu comentari...</textarea>
						</li>
						<li class="selecc">
							<input id="filetext" type="text" placeholder="No s’ha seleccionat cap arxiu" readonly>
							<label id="#bb" style="top:10px;"> Seleccionar arxiu
								<input type="file" name="fitxer" id="file" >
							</label> 
						</li>
						<li class="last-btn">
								<input id="enviar_mensaje_btn" type="button" value="Enviar">
						</li>
				</ul>
			</div>
		</div>
  </div>
</div>
@endsection

@section('styles')
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
<link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.css') }}" />
@endsection

@section('scripts')
<script src="{{ asset('js/maqueta/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script>
/* CalendarioMensajes */
$.ajaxSetup({
		headers: {
				'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
		}
});

$('.msg_info').click(function(e) {
		window.setTimeout(function(){
				$(".mCustomScrollbar").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
		}, 200);
});

$('#file').change(function(e) {
	var fitxer = $('#file')[0].files[0];
	$('#filetext').val(fitxer.name);
});

$('#enviar_mensaje_btn').click(function(e) {
		var user_id = {{ Auth::user()->id }};
		var mensaje = $('#mensaje-box').val();
		
		var formData = new FormData();
		var fitxer = $('#file')[0].files[0];

		formData.append("_token", '{{csrf_token()}}');
		formData.append("mensaje", mensaje);
		formData.append("user_id", user_id);
		formData.append('fitxer', fitxer);

		$.ajax({
				url: '{{ route('contactar.store') }}',
				method: 'POST',
				processData: false, // important
        contentType: false, // important
				data: formData,
				success: function(data) {
					if ( data == 'KO' ) alert('Error al enviar el missatge.')
					var ahora = new Date();
					var hora = ahora.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });
					var missatge = `
							<li>
									<div class="msg-l">
											<img style="min-width: 48px;min-height: 48px;border-radius: 50%;object-fit: scale-down;max-width: 48px;max-height: 48px;" src="/storage/avatars/{{ Auth::user()->avatar }}">
									</div>                        
									<div class="msg-r">
											<h4>{{ Auth::user()->name }} {{ Auth::user()->lastname }} <span>` + hora + `</span></h4>
											<p style="display: inline-block;min-width: 500px;width: 100%;">
													` + mensaje + `
											</p>`;
											if (fitxer) {
					missatge += `<span class="fitxer"><a><i class="material-icons">description</i></a></span>`;
											}
					missatge +=`</div>
							</li>
					`;

					$('.message.mCustomScrollbar ul').append(missatge);
					$('.mCustomScrollbar').mCustomScrollbar("update");
					$(".mCustomScrollbar").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
					$('#mensaje-box').val('');
				}
		});
});
/* end CalendarioMensajes */
</script>
@endsection