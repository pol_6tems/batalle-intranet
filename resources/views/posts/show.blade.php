@extends('layouts.frontend')

@section('page')
<section class="single-page register-done">
    <div class="row" style="text-align: left;">
        <h1 style="padding-bottom: 20px;">{{ $post->title }}</h1>
        {!! $post->description !!}
    </div>
</section>
@endsection

@section('scripts')
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('js/popper.js') }}"></script>
<script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
<script src="{{ asset('js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.register-done').css('min-height', setHeight);
    }
            
        
    $(document).ready(function() {
        $(".select_box").selectbox();
        setHeight()
    });

    $(window).resize(function(){
        setHeight()
    });
</script>
@endsection