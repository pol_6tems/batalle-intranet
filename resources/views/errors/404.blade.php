@extends('layouts.errors')

@section('title', '404 Página no encontrada')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	404
</div>
@endsection

@section('content')
<div style="text-align: center;">Página no encontrada</div>
@endsection