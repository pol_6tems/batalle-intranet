@extends('layouts.errors')

@section('title', '403 Acceso no autorizado')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	403
</div>
@endsection

@section('content')
<div style="text-align: center;">Acceso no autorizado</div>
@endsection