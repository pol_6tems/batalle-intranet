@extends('layouts.app')
@section('header')
<div class="title-head calen-ico">
	<h1>@Lang('Vacancy calendar')</h1>
</div>
@endsection
@section('content')
<div class="cal-cnt pad-left">
    <div class="row1">
        <div class="cal-lft tab_out_calender">
            <div class="dies">
                <h2><span>{{ $dies_pendents[ $aquest_any + 1 ] }} dies</span> pendents</h2>
            </div>
            <p class="pend-txt">Selecciona sobre el calendari els teus dies de vacances. Quan tinguis la teva proposta, envia’ns la teva petició.</p>
            <a href="#" class="show-pop">Més informació</a>

            <div id="pop-info-session-message" class="simplePopup especific" style=" margin: 25% auto;width: 70%;">
                <div class="desktop_pop_info">
                    <a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
                    <ul>
                        <span style="color:#000;" class="missatge"></span>
                    </div>
                    <div class="mobile_pop_info" style="display: none;">
                        <a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
                    <div class="mbl_pop_inner">
                    </div>
                </div>
            </div>
            <div id="pop-info" class="simplePopup">
                <div class="desktop_pop_info">
                    <a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
                    <h1>Funcionament del calendari de vacances:</h1>
                    <ul>
                        <li><strong>1.</strong>&nbsp; Selecciona sobre el calendari els teus dies de vacances. Quan tinguis la teva proposta, envia’ns la teva petició. <span>Revisa totes les dates abans d’enviar la petició, un cop enviada no es podrà modificar.</span> </li>
                        <li><strong>2.</strong>&nbsp;  Batallé revisarà la teva petició.</li>
                        <li class="no-mar"><strong>3.</strong>&nbsp;  Rebràs una notificació amb una confirmació ( <span class="r-grn"></span> ) i/o nova proposta dels dies escollits ( <span class="r-yel"></span> ). </li>
                        <ul class="second-level">
                            <li><strong>3.1</strong> Dies confirmats ( <span class="r-grn"></span> ): Són els teus dies de vacances. </li>
                            <li><strong>3.2</strong> Dies denegats ( <span class="r-yel"></span> ): Obre el xat i veuràs la nostra nova proposta.  </li>
                        </ul>
                    
                        <ul class="third-level">
                            <li>- Si estàs d’acord amb les noves dates, edita les dates denegades i selecciona al calendari els nous dies. </li>
                            <li>- Si no estàs d’acord, pots proposar al xat una contraproposta. Batallé et confirmarà o denegarà la data, fins a arribar a un acord.  Un cop acordada la data, edita les dates denegades i selecciona al calendari els nous dies.</li>
                        </ul>
                        <li><strong>4.</strong>&nbsp; En cas de tenir dies denegats, Batalle us confirmara les noves dates.</li>
                    </ul>
                    <div class="alert-pop">Tots els canvis o alteracions de dies festius de Batallé et seràn notificats a través d’aquesa plataforma.</div>
                </div>
                <div class="mobile_pop_info" style="display: none;">
                    <a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
                    <div class="mbl_pop_inner"></div>
                </div>
            </div>
            <!-- mobile buttons -->
            <div class="mob-cal-btns">
                <ul>
                    <li><button onClick="getCalender();">Calendari</button></li>
                    <li><button onClick="getmessage();">Missatge</button></li>
                </ul>                        
            </div>
            <!-- mobile buttons ends -->

            <div class="alert-div" style="@if(!$bloquejar_enviar_peticio)display:none;@endif">
                <img src="{{asset('images/BATALLE-INTANET-icona-avis.png')}}">
                @Lang('Attention, there is a pending request.')
            </div> 

            <div class="ultimate-div">
                <div class="tab-pad tab-pad1">
                    <ul class="nav nav-tabs1">
                        <li class="active tab-pink" id="pink">
                            <a href="#tab4"> <div class="tooltip"><p>{{$aquest_any}}</p> <span>{{ $dies_pendents[$aquest_any] }}</span> <span class="tooltiptext">Dies sol·licitats</span></div>
                            <!-- <div class="tooltip"><span>22</span> <span class="tooltiptext">Dies pendents d'escollir</span></div>-->
                            </a>
                        </li>
                        <li class="tab-blue" id="blue">
                            <a href="#tab5"><div class="tooltip"><p>{{ $aquest_any + 1 }}</p> <span>{{ $dies_pendents[ $aquest_any + 1 ] }}</span> <span class="tooltiptext">Dies pendents d'escollir</span></div></a>
                        </li>
                        <li class="tab-red" id="red">
                            <a href="#tab6"><div class="tooltip"><div>Festius</div> <p>{{$aquest_any + 1}}</p> <span>{{ $reds_num }}</span> <span class="tooltiptext">Dies Festius</span></div></a>
                        </li>
                    </ul>
                    <section id="tab4" class="tab-content active tab-content-pink">
                        <div>
                            <h3>Queden <span class="dies_pendents_pink">{{ $dies_pendents[$aquest_any] }}</span> dies a escollir del {{ $aquest_any }}: <span>*Data límit de consum 31 de gener {{ $aquest_any + 1 }}</span></h3>
                            <ul>
                                <!--<li class="yr_2018">24 Gener 2018 <span></span> </li>
                                <li class="yr_2018">25 Gener 2018 <span></span> </li>
                                <li class="yr_2019">26 Gener 2019 <span></span> </li>-->
                            </ul>
                        </div>
                    </section>
                    <section id="tab5" class="tab-content hide tab-content-blue">
                        <div>
                            <h3>Queden <span class="dies_pendents_blue">{{ $dies_pendents_totals[ $aquest_any + 1 ] }}</span> dies a escollir del {{ $aquest_any + 1 }}: <span>*Data límit de consum 31 de gener {{ $aquest_any + 2 }}</span></h3>
                            <ul>
                                <!--<li class="yr_2018">24 Gener 2018 <span></span> </li>-->
                            </ul>
                        </div>
                    </section>
                    <section id="tab6" class="tab-content hide tab-content-red">
                        <div>
                            <!--<h3>Queden <span class="dies_pendents_red">2</span> dies a escollir del {{date('Y') + 1}}:</h3>-->
                            <ul>
                                <!--<li>24 Gener 2018 <span></span> </li>-->
                            </ul>
                        </div>
                    </section>
                </div>
                <div class="dies-list">
                    <ul>
                        <li class="dies-green"><span></span> Dies aprovats</li>
                        <li class="dies-yellow"><span></span> Dies denegats</li>
                    </ul>
                </div>

                <form id="form_enviar_dies" action="{{route('calendari.store')}}" method="post">
                    {{ csrf_field() }}
                    <input id="eBlue" name="eBlue" type="hidden" />
                    <input id="eRed" name="eRed" type="hidden" />
                    <input id="ePink" name="ePink" type="hidden" />
                </form>

                <a href="javascript:void(0);" @if(!$bloquejar_enviar_peticio) onClick="$('#form_enviar_dies').submit();" @endif class="envir-btn @if($bloquejar_enviar_peticio) disabled @endif">
                    @if(!$bloquejar_enviar_peticio)
                    @Lang('Send request')
                    @else
                    @Lang('Waiting for the response')
                    @endif
                </a>
                
                @if ( !empty($last_update) )
                    <h3 style="display: inline-block;">@Lang('Last update'): {{$last_update->format('d/m/Y')}} · {{$last_update->format('G:i')}}</h3>
                @endif

            </div>
        </div>
        <div class="cal-rht tab_calender">
            <ul class="buttons">
                <li><a href="javascript:genScreenshot();"><img src="{{asset('images/BATALLE-INTANET-icona-pdf.png')}}"> Descarregar</a></li>
                <!--<li><a href="javascript:void(0);"><img src="{{asset('images/BATALLE-INTANET-icona-excel.png')}}"> Descarregar</a></li>-->
            </ul>
            <div class="tab-pad">
                <ul class="nav nav-tabs tab_cal">
                    <li class="active">
                        <a href="#tab1" class="cal_info">Calendari</a>
                    </li>
                    <li>
                        <a href="#tab2" class="msg_info">Missatge</a>
                    </li>
                </ul>
                <section id="tab1" class="tab-content active">
                    <div>
                        <div class="mbl_backward">
                            <button onClick="removeCal(0);"><span></span></button>
                        </div>
                        
                        <div class="cal-dpdwn">
                            <ul>
                                <li>											   
                                    <div class="cal-dropdown seleccio-vista">
                                        <button id="trigger-dropdown">
                                            <div id="dropdown-title" onFocus="this.blur()"> Any <span></span></div>
                                        </button>
                                        <ul class="dropdown" style="display:none;">
                                            <li id="agendaWeek" class="option">Dia <span>D</span></li>
                                            <li id="agendaDay" class="option">Setmana <span>W</span></li>
                                            <li id="month" class="option">Mes <span>M</span></li>
                                            <li id="year" class="option">Any <span>Y</span></li>
                                        </ul>
                                    </div>
                                </li>
                                <li>
                                    <div class="dropdown-hotel">
                                        <a href="javascript:void(0);" class="arw-toggle"></a>
                                        <div class="title-box">
                                            <h2 id="hotel-name" onFocus="this.blur()">Llegenda</h2>
                                        </div>
                                        <ul id="hotel-list" style="display:none;">
                                            <li class="pink"><a class="active" href="javascript:void(0);">Dies solicitats {{$aquest_any}} <span></span></a></li>
                                            <li class="blue"><a href="javascript:void(0);">Dies solicitats {{$aquest_any + 1}} <span></span></a></li>
                                            <li class="red"><a href="javascript:void(0);">Dies festius <span></span></a></li>
                                            <li class="green"><a href="javascript:void(0);">Dies aprovats <span></span></a></li>
                                            <li class="yellow"><a href="javascript:void(0);">Dies denegats <span></span></a></li>
                                        </ul>
                                    </div>
                                </li>
                            </ul>
                        </div>
                        <div id="calendar"></div>
                    </div>
                </section>
                    <section id="tab2" class="tab-content hide">
                    
                    <div class="mbl_backward">
                        <button onClick="removeCal(1);"><span></span></button>
                    </div>

                    <div class="message mCustomScrollbar">
                        <ul>

                            <!-- Missatge inicial benvinguda -->
                            <li>
                                <div class="msg-l">
                                    <img src="{{asset('images/BATALLE-INTANET-icona-usuari.png')}}">
                                </div>                        
                                <div class="msg-r">
                                    <h4>Batallé<span></span></h4>
                                    <p>{!! __('calendari-mensajes-benvinguda') !!}</p>
                                </div>                        
                            </li>
                            <!-- end Missatge inicial benvinguda -->
                            
                            @php( $last_date = null )
                            @foreach( $mensajes as $key => $mensaje )
                                @if ( $key == 0 )
                                    @php( $last_date = $mensaje->created_at->format('M d') )
                                    <li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
                                @elseif ( $last_date != $mensaje->created_at->format('M d') )
                                    <li class="line"><span>{{ $mensaje->created_at->format('M d') }}</span></li>
                                @endif
                            <li>
                                <div class="msg-l">
                                    @if( Auth::user()->id == $mensaje->from )
                                        <img src="/storage/avatars/{{ Auth::user()->avatar }}">
                                    @else
                                        <img src="{{asset('images/BATALLE-INTANET-icona-usuari.png')}}">
                                    @endif
                                </div>                        
                                <div class="msg-r">
                                    <h4>
                                        @if( Auth::user()->id == $mensaje->from )
                                            {{ Auth::user()->name }} {{ Auth::user()->lastname }}
                                        @else
                                            Batallé
                                        @endif
                                        <span>{{ $mensaje->created_at->format('G:i') }}</span>
                                    </h4>
                                    <p>{{ $mensaje->mensaje }}</p>
                                </div>                        
                            </li>
                                @php( $last_date = $mensaje->created_at->format('M d') )
                            @endforeach
                        </ul>
                    </div>
                    <!-- AIXO ES UN COMENTARI  2-->
                    <div class="form-pad">
                        <ul>
                            <li>
                                <textarea id="mensaje-box" class="txt-box" name="Escriu el teu comentari..." onFocus="if(this.value==this.defaultValue)this.value='';" onBlur="if(this.value=='')this.value=this.defaultValue;">@Lang('escriu-comentari')</textarea>
                            </li>
                            <li>
                                <input id="enviar_mensaje_btn" type="button" value="Enviar">
                            </li>
                        </ul>
                    </div>
                </section>
            </div>
        </div>
    </div>
</div>
<!-- PRINT PDF CANVAS *-->
<div id="box1" style="display: none;"></div>
@endsection

@section('styles')
<link rel="stylesheet" href="{{ asset('css/fullcalendar.css') }}" />
<link rel="stylesheet" href="{{ asset('css/jquery.mCustomScrollbar.css') }}" />
@endsection

@section('scripts')
<script>
    @if($bloquejar_enviar_peticio)
        var $bloquejar_enviar_peticio = true;
    @else
        var $bloquejar_enviar_peticio = false;
    @endif
</script>
<script src="{{ asset('js/html2canvas.min.js') }}"></script>
<script src="{{ asset('js/jspdf.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/fullcalendar-3.9.0/lib/moment.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/maqueta/fullcalendar.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/maqueta/jquery.mCustomScrollbar.concat.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/calendari.js') }}"></script>

<script>
/* PRINT PDF CANVAS */
function genScreenshot() {
    html2canvas(document.getElementById('tab1'), {
        onrendered: function(canvas) {
            $('#box1').html("");
            $('#box1').append(canvas);

            if (navigator.userAgent.indexOf("MSIE ") > 0 || navigator.userAgent.match(/Trident.*rv\:11\./)) {
                var blob = canvas.msToBlob();
                window.navigator.msSaveBlob(blob,'Test file.png');
            } else {
                //$('#test').attr('href', canvas.toDataURL("image/png"));
                doc = new jsPDF({
                    unit: 'px',
                    format: 'a4'
                });
                doc.addImage(canvas.toDataURL("image/png"), 'JPEG', 25, 50);
                doc.save('@Lang('Vacancy calendar') ' + $('#calendar').fullCalendar('getView').title + '.pdf');
                //window.open(doc.output('datauristring'));
                //doc.output('dataurlnewwindow');
            }
        }
    });
}
/* end PRINT PDF CANVAS */
</script>
<script>
    var $sel_dia_o_semana = '{{$sel_dia_o_semana}}';
    var tab_actual = 'pink';
    var ePink = [], eBlue = [], eYellow = [], eGreen = [], eRed = [], eGrey = [];
    var pendents_pink = parseInt('{{ $dies_pendents[ $aquest_any ] }}');
    var pendents_blue = parseInt('{{ $dies_pendents[ $aquest_any + 1 ] }}');
    var pendents_pink_totals = parseInt('{{ $dies_pendents_totals[ $aquest_any ] }}');
    var pendents_blue_totals = parseInt('{{ $dies_pendents_totals[ $aquest_any + 1 ] }}');
    var pendents_red = 0;
    var $colors = {
        'pink': {!! json_encode($pinks) !!},
        'blue': {!! json_encode($blues) !!},
        'green': {!! json_encode($greens) !!},
        'yellow': {!! json_encode($yellows) !!},
        'red': {!! json_encode($reds) !!},
        'grey': {!! json_encode($greys) !!}
    };

    $('#form_enviar_dies').submit(function(e) {
        var $blues = [], $pinks = [];
        
        var $eventos = $('#calendar').fullCalendar( 'clientEvents');
        $.each($eventos, function(idx, evt) {
            mes_aux = evt.start.getMonth() + 1;
            start = evt.start.getFullYear() + '-' + (mes_aux > 9 ? mes_aux : '0' + mes_aux) + '-' + evt.start.getDate();
            
            if ( evt.end != null ) {
                mes_aux = evt.end.getMonth() + 1;
                end = evt.end.getFullYear() + '-' + (mes_aux > 9 ? mes_aux : '0' + mes_aux) + '-' + evt.end.getDate();
            }
            else end = start;
            
            if ( evt.estado == 'blue' ) {
                $blues.push({
                    'start': start,
                    'end': end
                });
            } else if ( evt.estado == 'pink' ) {
                $pinks.push({
                    'start': start,
                    'end': end
                });
            }
        });
        
        // Si els dies pendents no és 0 no s'envia
        @if($no_bloquejat)
            var $no_bloquejat = true;
        @else
            var $no_bloquejat = false;
        @endif

        if ( pendents_blue > 0 && !$no_bloquejat) $blues = [];
        if ( pendents_pink > 0 && !$no_bloquejat) $pinks = [];
        
        $('#eBlue').val(JSON.stringify($blues));
        $('#ePink').val(JSON.stringify($pinks));
        
        if ( $blues.length > 0 || $pinks.length > 0 ) {
            return true;
        } else {
            obrir_popup( '@Lang('You still have pending days.')' );
            e.preventDefault();
            return false;
        }
        
        
    });
    
    /* CalendarioMensajes */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('.msg_info').click(function(e) {
        window.setTimeout(function(){
            $(".mCustomScrollbar").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
        }, 200);
    });

    $('#enviar_mensaje_btn').click(function(e) {
        var user_id = {{ Auth::user()->id }};
        var mensaje = $('#mensaje-box').val();
        
        $.ajax({
            url: '{{ route('calendario-mensajes.store') }}',
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'mensaje': mensaje, 'user_id': user_id},
            success: function(data) {
                if ( data == 'KO' ) alert('Error al enviar el missatge.')
                var ahora = new Date();
                var hora = ahora.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });
                $('.message.mCustomScrollbar ul').append(`
                    <li>
                        <div class="msg-l">
                            <img style="min-width: 48px;min-height: 48px;border-radius: 50%;object-fit: scale-down;max-width: 48px;max-height: 48px;" src="/storage/avatars/{{ Auth::user()->avatar }}">
                        </div>                        
                        <div class="msg-r">
                            <h4>{{ Auth::user()->name }} {{ Auth::user()->lastname }} <span>` + hora + `</span></h4>
                            <p style="display: inline-block;min-width: 500px;width: 100%;">
                                ` + mensaje + `
                            </p>
                        </div>                        
                    </li>
                `);
                $('.mCustomScrollbar').mCustomScrollbar("update");
                $(".mCustomScrollbar").mCustomScrollbar("scrollTo","bottom",{scrollInertia:0});
                $('#mensaje-box').val('');
            }
        });
    });
    /* end CalendarioMensajes */

    function obrir_popup(missatge) {
        $('#pop-info-session-message.especific span.missatge').html( missatge );
        $('#pop-info-session-message.especific').addClass('p-open');
        $('body').addClass('overlay');
		var width_pg = $(window).width();
    }

</script>
@endsection