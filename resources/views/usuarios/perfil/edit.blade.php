@extends('layouts.app')

@section('header')
<div class="title-head no-lftpad">
	<h1>@Lang('Editar perfil')</h1>              
</div>
@endsection

@section('content')
<div class="user-porfile">
    <div class="row1">         
        <p class="title-p">@Lang('Edit your profile')</p>
    </div>
</div>

@if (count($errors) > 0)
    <div class="alert alert-danger" style="float: left;width: 100%;color: #da4a61;padding: 10px 0px;">
        <div class="row1" style="border: solid 1px #da4a61;padding: 15px 5px;">
            <strong>@Lang('There have been some errors'):</strong>
            <br><br>
            <ul style="margin-left: 15px;padding-left: 15px;">
                @foreach ($errors->all() as $error)
                    <li style="list-style: disc;">{{ __($error) }}</li>
                @endforeach
            </ul>
        </div>
    </div>
@endif

@if (session('success'))
    <div class="alert alert-danger" style="float: left;width: 100%;color: #0b1;padding: 10px 0px;">
        <div class="row1" style="border: solid 1px #0b1;padding: 15px 5px;">
            {{ session('success') }}
        </div>
    </div>
@endif

<div class="single-page">
<div class="contact-frm form-register">
<div class="row1"> 
    <div class="left">
        <div class="img">
            <img src="/storage/avatars/{{ $user->avatar }}" alt="user-image-big.png">
            <a href="javascript:void(0);" onclick="$('#avatarFile').click();">@Lang('Change photo')</a>
            <i style="font-size: 0.8rem;line-height: 10px;float: left;width: 100%;text-align: left;padding: 10px 0 0;">(@Lang('only photo of card'))</i>
            <form id="update_avatar_form" action="{{route('usuario.update_avatar')}}" method="post" enctype="multipart/form-data" style="display:none;">
                {!! csrf_field() !!}
                <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
            </form>
        </div>

        <form id="update_perfil_form" action="{{route('usuario.update')}}" method="post">
            {!! csrf_field() !!}
            <div class="frm">
                <ul class="form-container">
                <li>
                    <label>@Lang('Name')*:</label>
                    <input required id="name" name="name" class="frmCrtl" type="text" value="{{$user->name}}" onBlur="if (this.value == ''){this.value = '@Lang('Name')'; }" onFocus="if (this.value == '@Lang('Name')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('Lastname')*:</label>
                    <input required id="lastname" name="lastname" class="frmCrtl" type="text" value="{{$user->lastname}}" onBlur="if (this.value == ''){this.value = '@Lang('Lastname')'; }" onFocus="if (this.value == '@Lang('Lastname')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('ID card')*:</label>
                    <input required id="dni" name="dni" class="frmCrtl" type="text" value="{{$user->dni}}" onBlur="if (this.value == ''){this.value = '@Lang('ID card')'; }" onFocus="if (this.value == '@Lang('ID card')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('Email')*:</label>
                    <input required id="email" name="email" class="frmCrtl" type="text" value="{{$user->email}}" onBlur="if (this.value == ''){this.value = '@Lang('Email')'; }" onFocus="if (this.value == '@Lang('Email')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('Phone'):</label>
                    <input name="phone" class="frmCrtl" type="text" value="{{$user->phone}}" onBlur="if (this.value == ''){this.value = '@Lang('Phone')'; }" onFocus="if (this.value == '@Lang('Phone')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('Mobile')*:</label>
                    <input required name="mobile" class="frmCrtl" type="text" value="{{$user->mobile}}" onBlur="if (this.value == ''){this.value = '@Lang('Mobile')'; }" onFocus="if (this.value == '@Lang('Mobile')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('Address')*:</label>
                    <input required name="address" class="frmCrtl" type="text" value="{{$user->address}}" onBlur="if (this.value == ''){this.value = '@Lang('Address')'; }" onFocus="if (this.value == '@Lang('Address')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('ZIP zone')*:</label>
                    <input required name="codigo_postal" class="frmCrtl" type="text" value="{{$user->codigo_postal}}" onBlur="if (this.value == ''){this.value = '@Lang('ZIP zone')'; }" onFocus="if (this.value == '@Lang('ZIP zone')') {this.value = ''; }">
                </li>
                <li>
                    <label>@Lang('City')*:</label>
                    <input required name="city" class="frmCrtl" type="text" value="{{$user->city}}" onBlur="if (this.value == ''){this.value = '@Lang('City')'; }" onFocus="if (this.value == '@Lang('City')') {this.value = ''; }">
                </li>				
                <li>
                    <label>@Lang('Language')*:</label>
                    <div class="select-data full-width select-language">
                        <select name="idioma" class="select_box">
                        @foreach ($_languages as $language)
                            <option {{ ($language->code == $user->idioma ? 'selected' : '') }} value="{{$language->code}}">@lang($language->name)</option>
                        @endforeach
                        </select>
                    </div>					
                </li>
                <li>
                    <label>@Lang('Birthdate')*:</label>
                    <div class="full-width">
                    <div class="select-data scrollable">
                        <select name="dia" class="select_box" name="dia">
                        @for ($i = 1; $i <= 31; $i++)
                            <option {{ ($user->fecha_nacimiento->day == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                    <div class="select-data scrollable">
                        <select name="mes" class="select_box" name="mes">
                        @for ($i = 1; $i <= 12; $i++)
                            <option {{ ($user->fecha_nacimiento->month == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                    <div class="select-data scrollable">
                        <select name="any" class="select_box" name="any">
                        @for ($i = date('Y'); $i >= date('Y') - 60; $i--)
                            <option {{ ($user->fecha_nacimiento->year == $i ? 'selected' : '') }} value="{{$i}}">{{$i}}</option>
                        @endfor
                        </select>
                    </div>
                    </div>
                </li>
                <button type="submit">@Lang('Save')</button>
            </div>
        </form>

    </div>

    <form id="update_password_form" action="{{route('usuario.update_password')}}" method="post">
        {!! csrf_field() !!}
        <div class="right">
            <ul>
                <li class="password password-perfil">
                    <label>@Lang('Password')*:</label>
                    <input name="old_password" class="frmCrtl" type="password" placeholder="@Lang('Password')">
                    <span class=""></span>
                </li>
                <li class="password password-perfil"> 
                    <label><strong>@Lang('New password')*:</strong></label>
                    <input name="password" class="frmCrtl" type="password" placeholder="@Lang('New password')">
                    <span class=""></span>
                </li>
                <li class="password password-perfil"> 
                    <label><strong>@Lang('Repeat password')*:</strong></label>
                    <input name="password_confirmation" class="frmCrtl" type="password" placeholder="@Lang('Repeat password')">
                    <span class=""></span>
                </li>
                
            </ul>
            <!--<a href="javascript:void(0);">Has oblidat la contrasenya?</a>-->
            <button>@Lang('Save')</button>
        </div>
    </form>

</div>
</div>
</div>
@endsection

@section('scripts')
<script>
$(document).ready(function() {
	//Password / text toggle
	$('.password span').click(function(){
		var getClass = $(this).attr("class");
		$(this).toggleClass('showPassword');
		if(getClass == "showPassword")
		{
			$(this).siblings('.frmCrtl').attr("type", "password")
		}
		else
		{
			$(this).siblings('.frmCrtl').attr("type", "text")
		}
	});//End of Password / text toggle
	
	//block on focuss
	$('.frmCrtl').keydown(function(e){
		var a = e.which;
		if(a !== 9)
		{
			$(this).css('color', '#000');
		}
	});
	
	$('.sbHolder .sbOptions li a').click(function(){
		$(this).parents('.sbHolder').find('.sbSelector').css('color', '#000');
	});
});
</script>
<script>
document.getElementById("avatarFile").onchange = function() {
    document.getElementById("update_avatar_form").submit();
};
</script>
@endsection