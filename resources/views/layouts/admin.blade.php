<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
	<!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

	<title>@yield("title", config('app.name', 'Endo'))</title>
	
	<link href="{{asset('css/bootstrap/css/bootstrap.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('css/bootstrap/css/bootstrap-editable.css')}}" rel="stylesheet" type="text/css">
	@yield("styles_before")
	
    <!-- Styles -->
    <!--<link href="{{ asset('css/app.css') }}" rel="stylesheet">-->
    <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
	
	<!-- Material Design for Bootstrap fonts and icons -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons">
    <!-- Material Design for Bootstrap CSS -->
	<link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">
	<link href="{{asset('js/bootstrap-select/bootstrap-select.min.css')}}" rel="stylesheet" type="text/css">
	
	@yield("styles")
	
	<link href="{{asset('css/endo-admin.css')}}" rel="stylesheet" type="text/css">
	
	<style>
		.navbar-brand i, .navbar-brand span { vertical-align: middle; }
	</style>
	
	
</head>
<body>

	<div class="bmd-layout-container bmd-drawer-f-l bg-white border-bottom box-shadow">
		<header class="bmd-layout-header">
			<div class="navbar navbar-light bg-faded">
				<button class="navbar-toggler" type="button" data-toggle="drawer" data-target="#dw-s1">
					<span class="sr-only">Toggle drawer</span>
					<i class="material-icons">menu</i>
				</button>
				<span class="header-seccio my-0 font-weight-bold ml-md-3" onclick="window.location.href = '{{url('/')}}';">
					{{ config('app.name', 'Endo') }}
				</span>
				<div class="btn-group selector-idiomas">
					<button class="btn dropdown-toggle" type="button" id="languages" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						@Lang( (!empty(session()->get('locale_name')) ? session()->get('locale_name') : 'Idioma') )
					</button>
					<div class="dropdown-menu" aria-labelledby="languages">
						@foreach ($_languages as $language)
							<a class="dropdown-item" href="{{ url('/locale/' . $language->code) }}">@lang($language->name)</a>
						@endforeach
					</div>
				</div>
				<i onclick="window.location.href = '{{url('/')}}';" class="logo-admin ml-2 ml-md-auto"></i>
				<div class="title-admin"><h5>Endo</h5></div>
				<!--<h5 class="my-0 font-weight-normal ml-md-auto">{{ config('app.name', 'BATALLE') }}</h5>-->
				<nav class="my-2 my-md-0 mr-md-3">
					<!--<a class="p-2 text-dark" href="#">Pricing</a>-->
				</nav>
			</div>
		</header>
		<div id="dw-s1" class="bmd-layout-drawer">

			@foreach ( \App\Http\Controllers\AdminController::getMenu() as $menu )
				<header class="pb-0">
					<a class="navbar-brand pl-0" data-toggle="collapse" href="#{{$menu->name}}" role="button" aria-expanded="false" aria-controls="{{$menu->name}}">
						<i class="material-icons pr-2">{{$menu->icon}}</i><span>{{__($menu->name)}}</span>
					</a>
				</header>
				<div class="collapse show in" id="{{$menu->name}}">
					<ul class="list-group pt-0">
					@foreach ( $menu->submenus as $sub )
						@if ( $sub->rol->level <= \Auth::user()->rol->level )
						<li>
							<a class="list-group-item pt-2 pb-2 
							@foreach ( explode(',', $sub->requests) as $sub_req )
								@if (request()->is($sub_req . '*')) active @endif
							@endforeach
							"
							href="@if ( $sub->url_type == 'route' ) {{ route($sub->url) }} @else {{ url($sub->url) }} @endif">
								{{__($sub->name)}}
							</a>
						</li>
						@endif
					@endforeach
					</ul>
				</div>
			@endforeach
			
			<footer>
				<a class="btn btn-raised btn-primary" href="{{ route('logout') }}"
					onclick="event.preventDefault(); document.getElementById('logout-form').submit();">
					@Lang('Logout')
				</a>
				<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
					{{ csrf_field() }}
				</form>
			</footer>
		</div>
		
		<main class="bmd-layout-content">

			<div class="header">
				<div class="container">
					@yield("header")
				</div>
			</div>

			<div class="container">
				@if (Session::has('message'))
					<div class="alert alert-success alert-dismissible" role="alert">
						@lang(Session::get('message'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				@if (Session::has('error'))
					<div class="alert alert-danger alert-dismissible" role="alert">
						@lang(Session::get('error'))
						<button type="button" class="close" data-dismiss="alert" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
				@endif
				<div class="content">
					@yield("content")
				</div>
			</div>
		</main>

	</div>
	
	<footer>
		@yield("footer")
	</footer>	

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
	<!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>
    <script src="{{ asset('js/popper.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select/bootstrap-select.min.js') }}"></script>
    <script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
	<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>
	
	<script>$(document).ready(function() { $('select').selectpicker(); });</script>
	
	@yield("scripts")
	
</body>
</html>
