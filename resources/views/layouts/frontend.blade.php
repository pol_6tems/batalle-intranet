<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield("title", config('app.name', 'Endo'))</title>

<link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">

<!-- Material Design for Bootstrap CSS -->
<!--<link href="{{asset('css/bootstrap-material-design.min.css')}}" rel="stylesheet" type="text/css">-->
@yield('styles_grandfather')

<!-- Styles -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/jquery.selectbox.css')}}" rel="stylesheet" type="text/css">

@yield('styles_parent')
@yield('styles')

<!--Responsive CSS-->
<link href="{{asset('css/media.css')}}" rel="stylesheet" type="text/css">

<body class="dp-menu">

@if (Session::has('message'))
	<div id="pop-info-session-message" class="simplePopup general" style=" margin: 25% auto;width: 70%;">
		<div class="desktop_pop_info">
			<a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
			<ul>
				<span style="color:#000;">@Lang(Session::get('message'))</span>
			</div>
			<div class="mobile_pop_info" style="display: none;">
				<a href="javascript:void(0);" class="close-btn"><img src="{{asset('images/BATALLE-INTANET-icona-popup-tancar.png')}}"></a>
			<div class="mbl_pop_inner">
			</div>
		</div>
	</div>
@endif

<header>
	<section class="hdr-top hdr-top-red">
		<div class="row">
			<div class="top-lft">
				<ul>
					@if ( Auth::user() && (Auth::user()->role == 'admin' || Auth::user()->role == 'editor' ))
					<li><a href="{{ url('/admin') }}" class="active">@Lang('Private area')</a></li>
					@endif
					<li id="selector-idioma">
						<select class="select_box" onchange="window.location.href='{{ url('/locale') }}/' +  this.value; ">
							@foreach ($_languages as $language)
								<option value="{{$language->code}}" {{ ($language->code == $language_code ? 'selected' : '') }}>
									@lang($language->name)
								</option>
							@endforeach
						</select>
					</li>
				</ul>
			</div>
			<div class="top-rht ml-md-auto">
				<a href="{{url('/')}}"><img src="{{asset('images/BATALLE-INTRANET-logo-BLANC.png')}}"></a>
			</div>
		</div>
	</section>
</header>

@yield('page')

<!-- Footers -->
<footer>
	<div class="main-footer footer-red">
		<div class="row">
			<ul class="main">
				<li class="contact"><a href="http://www.batalle.com/es/companyia.html"> <img src="{{asset('images/foot-logo-red.png')}}" alt=""></a> </li>

				<li class="dept">
					<h4>@Lang('Contact')</h4>
					<p>Av. dels Segadors, s/n <br> 17421 Riudarenes, Girona (Spain)</p>
				</li>

				<li class="dept mar-20">
					<ul>
						<li><a href="tel:972 85 60 50">972 85 60 50</a></li>
						<li><a href="mailto:info@batalle.com">info@batalle.com</a></li>
					</ul>
				</li>

				<li class="dept rht">
					<p class="cookie">
							<a href="{{ \App\Post::find(2)->get_url() }}">@Lang('Legal note')</a>
							<span class="divider">/</span>
							<a href="{{ \App\Post::find(8)->get_url() }}">@Lang('Cookies policy')</a>
							<span class="divider">/</span>
							<a href="{{url(__('/politica-de-privacitat'))}}">@Lang('Privacy policy')</a>
							<span> © 2018 Batallé</span>
					</p>
				</li>
			</ul>
		</div>
	</div>
</footer>

@yield('scripts_grandfather')

<!-- Scripts -->
<script src="{{ asset('js/maqueta/respond.js') }}"></script>
<script src="{{ asset('js/maqueta/html5.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery-1.5.min.js') }}"></script>
<!--<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>-->
<!--<script src="{{ asset('js/popper.js') }}"></script>-->

@yield('scripts_parent')

<script src="{{ asset('js/maqueta/jquery.sticky.js') }}"></script>
<script src="{{ asset('js/maqueta/common.js') }}"></script>
<!--<script src="{{ asset('js/bootstrap-material-design.js') }}"></script>
<script>$(document).ready(function() { $('body').bootstrapMaterialDesign(); });</script>-->

@yield('scripts')

@if (Session::has('message'))
	<script>
		$(document).ready(function() {
			$('#pop-info-session-message.general').addClass('p-open');
			$('body').addClass('overlay');
			var width_pg = $(window).width();
		});
	</script>
@endif

</body>
</html>