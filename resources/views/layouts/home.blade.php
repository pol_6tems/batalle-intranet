<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <title>@yield('title')</title>
        <meta name="description" content="@yield('description')" />
        <meta name="keywords" content="@yield('keywords')" />
		
        <script type='text/javascript' src="{{asset('bootstrap/js/bootstrap.min.js')}}"></script>
        
        <link href="{{asset('bootstrap/css/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet" type="text/css">
		<link href="{{asset('css/endo.css')}}" rel="stylesheet" type="text/css">
    </head>
    <body>
	
		<?php /*
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand">Endo v3</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNav">
				<ul class="navbar-nav">
					<li class="nav-item {{Request::is('/') ? 'active' : ''}}">
						<a class="nav-link" href="{{url('/')}}">Inici</a>
					</li>
					<!--<li class="nav-item {{Request::segment(1) == 'llocs' ? 'active' : ''}}">
						<a class="nav-link" href="{{url('llocs')}}">Llocs </a>
					</li>-->
				</ul>
			</div>
		</nav>
		*/ ?>
		
        <div class="position-ref">
			
            @if (Route::has('login'))
                <div class="top-right links">
                    @auth
                        <a href="{{ url('/home') }}">Home</a>
                    @else
                        <a href="{{ route('login') }}">Login</a>

                        @if (Route::has('register'))
                            <a href="{{ route('register') }}">Register</a>
                        @endif
                    @endauth
                </div>
            @endif
			
			<div class="header">
                @yield("header")
            </div>

            <div class="content">
                @yield("content")
            </div>
			
        </div>
		
		<footer>
			@yield("footer")
		</footer>
		
		<?php /*
		<script src="{{asset('js/jquery-3.3.1.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.min.js')}}"></script>
		<script src="{{asset('js/bootstrap.bundle.js')}}"></script>
		<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
		<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
		*/ ?>
		
		@yield("scripts")
		
    </body>
</html>