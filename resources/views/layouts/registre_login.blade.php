<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">

<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<title>@yield("title", config('app.name', 'Endo'))</title>

<link rel="shortcut icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">
<link rel="icon" href="{{asset('images/favicon.ico')}}" type="image/x-icon">

<!-- Styles -->
<link href="{{asset('css/style.css')}}" rel="stylesheet" type="text/css">
<link href="{{asset('css/jquery.selectbox.css')}}" rel="stylesheet" type="text/css">
<!--Responsive CSS-->
<link href="{{asset('css/media.css')}}" rel="stylesheet" type="text/css">

<body class="area-body">

@yield("content")

<!-- Scripts -->
<script src="{{ asset('js/maqueta/respond.js') }}"></script>
<script src="{{ asset('js/maqueta/html5.js') }}"></script>
<script src="{{ asset('js/jquery-3.2.1.min.js') }}"></script>

<!--<script src="js/moment.min.js"></script>-->
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script type='text/javascript'>
	$(document).ready(function() {
		$(".area-style select").selectbox();
	});
</script>
@yield("scripts")

</body>
</html>