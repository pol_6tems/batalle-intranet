@extends('layouts.frontend')

@section('page')
<div class="clear"></div>
<section class="relative-pad"> 
<!-- logo section starts -->
<div class="user-section pad-left">
	<div class="menu">
		<a href="javascript:void(0);" id="m-menu"><img src="{{asset('images/BATALLE-INTANET-icona-menu.png')}}" class="m_menu"></a> 
		
		<!-- Maga Menu -->
			<div class="button-dropdown nav-toggle-red">
			<div class="button-inner">
			<div class="menu-header">
				<div class="w-menu-p">
				<div id="m-menu-close" class="nav-toggle">
					<a href="javascript:void(0);"><span></span> <p>menú</p></a>
				</div> 
				</div>          
			</div>
			<div id="divfocus" class="button-dropdown-int">
				<div class="menu-list">
				<ul>
					<li><a href="{{route('calendari.index')}}"><img src="{{asset('images/menu-cal-ico.png')}}"> Calendari de vacances</a></li>
					<li class="menu-grey"><a href="javascript:void(0);"><img src="{{asset('images/menu-forma-ico.png')}}"> Formació</a></li>
					<li class="menu-grey"><a href="javascript:void(0);"><img src="{{asset('images/menu-not-ico.png')}}"> Notícies</a></li>
					<li class="menu-grey"><a href="javascript:void(0);"><img src="{{asset('images/menu-des-ico.png')}}"> Descomptes</a></li>
					<li class="menu-grey"><a href="javascript:void(0);"><img src="{{asset('images/menu-enques-ico.png')}}"> Enquestes</a> </li>
					<li class="menu-grey"><a href="javascript:void(0);"><img src="{{asset('images/menu-sugger-ico.png')}}"> Bústia suggeriments</a> </li>
					<li><a href="{{route('contactar.index')}}"><img src="{{asset('images/menu-cont-ico.png')}}"> Contactar</a> </li>                  
				</ul> 
				</div>
			</div>
			</div>
			</div>
		<!-- /Maga Menu -->
	</div>
	
	<div class="row1">
	
		@if (Auth::check())
			<div class="user-rht">
				<a href="javascript:void(0);">
					<img src="/storage/avatars/{{ Auth::user()->avatar }}" style="margin-left: 10px;">
					<br>
					<h5 style="width: 100%;text-align: center;">{{Auth::user()->name}}</h5>
				</a>
				<div class="user-popup">
					<h3>{{Auth::user()->name}} {{Auth::user()->lastname}}</h3>
					<ul>
						<li><a href="{{route('usuario.perfil')}}">@Lang('Editar perfil')</a></li>
						<!--<li><a href="javascript:void(0);">Modificar contrasenya</a></li>-->
						<li><a href="{{ route('logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();">@Lang('Logout')</a></li>
						<form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
							{{ csrf_field() }}
						</form>
					</ul>
				</div>         
			</div>
		@endif

		@yield('header')

	</div>
</div>  
<!-- logo section ends -->

	@yield("content")

</section>
<div class="clear"></div>
@endsection

@section('styles_parent')
<link rel="stylesheet" href="{{ asset('css/megamenu.css') }}" />
@endsection

@section('scripts_parent')
<script src="{{ asset('js/maqueta/jquery.min.js') }}"></script>
<script src="{{ asset('js/maqueta/jquery.selectbox-0.2.js') }}"></script>
<script src="{{ asset('js/maqueta/menu.js') }}"></script>
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.relative-pad').css('min-height', setHeight);	
    }

    $(document).ready(function() {	
		setHeight();
		$(".select_box").selectbox();
    });

    $(window).resize(function(){
        setHeight();
    });
</script>
@endsection