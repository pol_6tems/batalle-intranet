@extends('layouts.app')

@section('header')
<div class="title-head cal-ico">
	<h1>@Lang('Dashboard')</h1>              
</div>
@endsection

@section('content')
<div class="benvigut">
	<div class="row1">
		<img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}">
		<p>Benvigut/da a l’àrea privada de Batallé, <span>aquest canal està pensat per tenir una relació més estreta i àgil entre treballador/a i empresa.</span></p>
		<div class="beguit-row">
			<ul>
			<li class="ben-cal"><a href="{{route('calendari.index')}}"><span></span> <h3>Calendari de vacances</h3></a></li>
			<li class="ben-prop"><a href="javascript:void(0);" class="opact"><span></span> <h3>PROPERAMENT</h3></a></li>
			<li class="ben-book"><a href="javascript:void(0);" class="opact"><span></span> <h3>PROPERAMENT</h3></a></li>
			<li class="ben-off"><a href="javascript:void(0);" class="opact"><span></span> <h3>PROPERAMENT</h3></a></li>
			<li class="ben-list"><a href="javascript:void(0);" class="opact"><span></span> <h3>PROPERAMENT</h3></a></li>
			<li class="ben-drop"><a href="javascript:void(0);" class="opact"><span></span> <h3>PROPERAMENT</h3></a></li>
			<li class="ben-cont"><a href="{{route('contactar.index')}}"><span></span> <h3>Contactar</h3></a></li>
			</ul>
		</div>
	</div>
</div>
@endsection