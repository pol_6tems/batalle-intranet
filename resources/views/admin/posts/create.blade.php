@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h1>@Lang('Add new')</h1>
                    </div>

                    <div class="panel-body">

                    @if ($errors->count() > 0)
                        <ul>
                            @foreach($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    <ul class="nav nav-tabs">
                        @foreach($_languages as $language_item)
                            <li class="nav-item">
                                <a class="nav-link {{ ($language_item->code == $language_code) ? 'active' : '' }}" data-toggle="tab" href="#{{$language_item->code}}">{{$language_item->code}}</a>
                            </li>
                        @endforeach
                    </ul>

                    <form action="{{ route('posts.store') }}" method="post">
                        {{ csrf_field() }}
                        <input type="hidden" name="status" value="publish" />
                        <input type="hidden" name="type" value="post" />
                        
                        <div class="tab-content">
                            @foreach($_languages as $language_item)
                            <div id="{{$language_item->code}}" class="tab-pane fade {{ ($language_item->code == $language_code) ? 'active in show' : '' }}">
                                
                                Title:
                                <br />
                                <input type="text" name="{{$language_item->code}}[title]" value="{{ old('title') }}" style="font-size: 30px;width: 100%;"/>
                                <br /><br />
                                Description:
                                <br />
                                <textarea name="{{$language_item->code}}[description]" class="summernote"></textarea>
                                <br /><br />
                                <input onclick="window.location.href='{{ route('posts.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
                                <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
                                    
                            </div>
                                
                            @endforeach
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('styles')
<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.css" rel="stylesheet">
@endsection

@section('scripts')
<script src="https://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.9/summernote.js"></script>  
<script>
    $(document).ready(function() {
        $('.summernote').summernote({
            height: 300
        });
        $('.popover').hide();
    });
</script>
@endsection