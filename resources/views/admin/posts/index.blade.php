@extends('layouts.admin')

@section('header')
<h1>
    Posts
    @can('create', \App\Post::class)
        <a href="{{ route('posts.create') }}" class="btn btn-default">@Lang('Add new')</a>
    @endcan
</h1>
@endsection

@section('content')
<ul class="nav nav-tabs">
    <li class="nav-item active">
        <a class="nav-link active" data-toggle="tab" href="#posts">@Lang('Posts')</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" data-toggle="tab" href="#trash">@Lang('Trash')</a>
    </li>
</ul>

<div class="tab-content">
    <div id="posts" class="tab-pane active in show">
        <table class="table table-bordered">
            <thead>
                <tr>
                    @can('delete', \App\Post::class)
                        <th width="5%"><input type="checkbox" class="checkbox_all"></th>
                    @endcan
                    <th>@Lang('Title')</th>
                    <th>@Lang('URL')</th>
                    @can('update', \App\Post::class)
                        <th width="5%">@Lang('Actions')</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @forelse($posts as $item)
                <tr>
                    @can('delete', \App\Post::class)
                        <td><input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" /></td>
                    @endcan
                    
                    <td>
                        <a href="{{ $item->get_url() }}" target="_blank">
                            {{ $item->title }}
                        </a>
                    </td>
                    <td>{{ $item->post_name }}</td>

                    @can('update', \App\Post::class)
                        <td class="cela-opcions">
                            <div class="btn-group">
                                <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </button>  
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                                    <a class="dropdown-item" href="{{ route('posts.edit', $item->id) }}">@Lang('Edit')</a>
                                    <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to delete it?')" data-url="{{ route('posts.destroy', $item->id) }}" data-nombre="{{$item->title}}" data-method="DELETE" data-titulo="@Lang('Delete')">@Lang('Delete')</button>
                                </div>
                            </div>
                        </td>
                    @endcan
                </tr>
                @empty
                    <tr>
                        <td colspan="3">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $posts->links() }}

        @can('delete', \App\Post::class)
            <form action="{{ route('posts.mass_destroy') }}" method="post" onsubmit="return confirm('Are you sure?');">
                {{ csrf_field() }}
                <input type="hidden" name="_method" value="DELETE">
                <input type="hidden" name="ids" id="ids" value="" />
                <input type="submit" value="@Lang('Delete selected')" class="btn btn-danger" />
            </form>
        @endcan

    </div>

    <div id="trash" class="tab-pane fade">
        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>@Lang('Title')</th>
                    @can('update', \App\Post::class)
                        <th width="5%">@Lang('Actions')</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @forelse($posts_trash as $item)
                <tr>
                    <td>{{ $item->title }}</td>

                    @can('update', \App\Post::class)
                        <td class="cela-opcions">
                            <div class="btn-group">
                                <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </button>  
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                                    <a class="dropdown-item" href="{{ route('posts.edit', $item->id) }}">@Lang('Edit')</a>
                                    <button type="button" class="dropdown-item" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to restore it?')" data-url="{{ route('posts.restore', $item->id) }}" data-method="PUT" data-nombre="{{$item->title}}" data-titulo="@Lang('Restore')">@Lang('Restore')</button>
                                    <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-text="@Lang('Are you sure to delete it?')" data-url="{{ route('posts.destroy_permanent', $item->id) }}" data-nombre="{{$item->title}}" data-method="DELETE" data-titulo="@Lang('Delete permanently')">@Lang('Delete permanently')</button>
                                </div>
                            </div>
                        </td>
                    @endcan
                </tr>
                @empty
                    <tr>
                        <td colspan="3">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $posts_trash->links() }}
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" id="_method" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Attention')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Accept')</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection	

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var titulo = button.data('titulo');
		var nombre = button.data('nombre');
		var texto = button.data('text');
		var method = button.data('method');
		var modal = $(this);
		modal.find('form').attr('action', url);
		$('#_method').val(method);
		modal.find('.modal-body').html( texto );
		modal.find('.modal-title').html( titulo );
	});
</script>
<script>
    function getIDs() {
        var ids = [];
        $('.checkbox_delete').each(function () {
            if($(this).is(":checked")) {
                ids.push($(this).val());
            }
        });
        $('#ids').val(ids.join());
    }

    $(".checkbox_all").click(function(){
        $('input.checkbox_delete').prop('checked', this.checked);
        getIDs();
    });

    $('.checkbox_delete').change(function() {
        getIDs();
    });
</script>
@endsection