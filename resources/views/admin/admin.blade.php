@extends('layouts.admin')

@section('header')
<div class="title m-b-md flex-center" style="padding-top: 20vh;">
	<!--Admin-->
</div>
@endsection

@section('content')
<div style="text-align: center;">
	Bienvenid@ {{Auth::user()->name}} a su Panel de Administrador
</div>
@endsection