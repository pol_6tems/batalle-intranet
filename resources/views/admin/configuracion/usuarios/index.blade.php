@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	@lang('Users')
	<a href="{{ url('/admin/configuracion/usuarios/create') }}">
		<i class="material-icons pr-2 admin-menu-icona add_circle"></i>
	</a>
	<a target="_blank" href="{{ url('/admin/configuracion/usuarios/exportar') }}" class="btn btn-default btn-raised">@Lang('Export')</a>
</h1>
@endsection

@section('content')
<form class="form-inline" method="GET" action="{{ url('admin/configuracion/usuarios/') }}">
	{!! csrf_field() !!}
	<div class="form-group mr-4">
		<input type="hidden" name="sortby" value="{{$sortby}}">
		<input type="hidden" name="order" value="{{$order}}">
		<label for="search" class="bmd-label-floating">@Lang('Search'):</label>
		<input type="text" name="search" class="form-control" placeholder="" value="{{$search}}">
	</div>
	<span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
		<button type="submit" class="btn btn-default btn-raised">@Lang('Search')</button>
		&nbsp;&nbsp;&nbsp;&nbsp;
		<button type="button" onclick="window.location.href='{{url('admin/configuracion/usuarios/')}}'" class="btn btn-secondary btn-raised">@Lang('Reset')</button>
	</span>
</form>

<table id="taula" class="table table-bordered table-striped table-sortable">
	<thead>
		<tr>
			<th scope="col">#</th>
			<th scope="col">@Lang('Name')</th>
			<th scope="col">@Lang('Email')</th>
			<th scope="col">@Lang('ID card')</th>
			<th scope="col">@Lang('Created at')</th>
			<th scope="col">@Lang('Role')</th>
			<th scope="col">@Lang('Level')</th>
			<th scope="col">@Lang('Sublevel')</th>
			<th scope="col">&nbsp;</th>
		</tr>
	</thead>
	<tbody>
	
	@foreach($usuarios as $item)
		@if ( Auth::user()->role != 'admin' && Auth::user()->id != $item->id && ($item->role == 'admin' || $item->role == 'editor') )
			@continue
		@endif
		<tr>
			<th scope="row">{{ $item->id }}</th>
			<td>{{ $item->fullname() }}</td>
			<td>
			@if ( !empty($item->empleado()->first()) )
				<a href="{{route('empleados.show', $item->empleado()->first()->id)}}" target="_blank">
					{{ $item->email }}
				</a>
			@else
				{{ $item->email }}
			@endif
			</td>
			<td>{{ $item->dni }}</td>
			<td>{{ $item->created_at }}</td>
			<td>{{ $item->role }}</td>
			<td>@if ( $item->nivel()->exists() ) {{ $item->nivel->name }} @else - @endif</td>
			<td>@if ( $item->subnivel()->exists() ) {{ $item->subnivel->name }} @else - @endif</td>
			<td class="cela-opcions">
				<div class="btn-group">
					<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						<i class="material-icons">more_vert</i>
					</button>  
					<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
						<a class="dropdown-item" href="{{action('AdminUsuariosController@edit', $item->id)}}">Editar</a>
						@if ( $item->id > 1 && Auth::user()->id != $item->id )
							<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-id="{{$item->id}}" data-nombre="{{$item->name}}">Eliminar</button>
						@endif
					</div>
				</div>
			</td>
		</tr>
	@endforeach
	
	</tbody>
</table>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				{{ method_field('DELETE') }}

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Eliminar</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
@endsection

@section('scripts')
<script>
    $('.eliminar-item').click(function(e){
        e.preventDefault() // Don't post the form, unless confirmed
        if (confirm('¿Estás seguro de eliminarlo?')) {
            $(e.target).closest('form').submit();
        }
    });
	$('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var id = button.data('id');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', '/admin/configuracion/usuarios/' + id);
		modal.find('.modal-body').html("¿Seguro que quieres eliminar a <strong>" + nombre + "?</strong>");
	});
</script>
@endsection