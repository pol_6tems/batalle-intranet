@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	Nuevo usuario
</h1>
@endsection

@section('content')
<form method="POST" action="{{url('admin/configuracion/usuarios')}}">
    {!! csrf_field() !!}
	<div class="form-group">
		<label for="name" class="bmd-label-floating">@Lang('Name')</label>
		<input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}">
		<div class="text-danger">{{$errors->first('name')}}</div>
	</div>
	<div class="form-group">
		<label for="lastname" class="bmd-label-floating">@Lang('Lastname')</label>
		<input type="text" class="form-control" id="lastname" name="lastname" value="{{ old('lastname') }}">
		<div class="text-danger">{{$errors->first('lastname')}}</div>
	</div>
	<div class="form-group">
		<label for="email" class="bmd-label-floating">@Lang('Email')</label>
		<input type="email" class="form-control" id="email" name="email" value="{{ old('email') }}">
		<!--<span class="bmd-help">No compartiremos su mail con nadie.</span>-->
		<div class="text-danger">{{$errors->first('email')}}</div>
	</div>
	<div class="form-group">
		<label for="dni" class="bmd-label-floating">@Lang('ID card')</label>
		<input type="text" class="form-control" id="dni" name="dni" value="{{ old('dni') }}">
		<div class="text-danger">{{$errors->first('dni')}}</div>
	</div>
	<div class="form-group">
		<label for="password" class="bmd-label-floating">@Lang('Password')</label>
		<input type="password" class="form-control" id="password" name="password">
		<div class="text-danger">{{$errors->first('password')}}</div>
	</div>
	<div class="form-group">
		<label for="password_confirmation" class="bmd-label-floating">@Lang('Confirm password')</label>
		<input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
	</div>
	<div class="form-group">
		<label for="role" class="bmd-label-floating">@Lang('Role')</label>
		<select class="form-control custom-select" name="role" data-live-search="true" title="@Lang('Role')">
		@foreach ($rols as $rol)
			<option value="{{$rol->name}}">{{__($rol->name)}}</option>
		@endforeach
		</select>
	</div>
	<div class="form-group">
		<label for="calendario_nivel_id" class="bmd-label-floating">@Lang('Level')</label>
		<select name="calendario_nivel_id" class="custom-select col-md-12" data-live-search="true">
			<option value="0">@Lang('No item selected')</option>
		@foreach ($niveles as $nivel)
			<option value="{{$nivel->id}}">{{$nivel->name}}</option>
		@endforeach
		</select>
	</div>
	<div class="form-group">
		<label for="calendario_subnivel_id" class="bmd-label-floating">@Lang('Sublevel')</label>
		<select name="calendario_subnivel_id" class="custom-select col-md-12" data-live-search="true">
			<option value="0">@Lang('No item selected')</option>
		@foreach ($subniveles as $subnivel)
			<option value="{{$subnivel->id}}">{{$subnivel->name}}</option>
		@endforeach
		</select>
	</div>
	{{--<div class="form-group">
		<label for="empleado_dni" class="bmd-label-floating">@Lang('Employee')</label>
		<select class="form-control custom-select" name="dni" data-live-search="true" title="@Lang('Employee')">
			@foreach ($empleados as $empleado)
				<option value="{{$empleado->dni}}">{{$empleado->nombre}} ({{$empleado->dni}})</option>
			@endforeach
		</select>
	</div>--}}
	<button type="button" class="btn btn-default" onclick="window.location.href='{{url('admin/configuracion/usuarios')}}'">Cancelar</button>
	<button type="submit" class="btn btn-primary btn-raised">Crear</button>
</form>
@endsection

@section('footer')
@endsection

@section('styles')
<style>
	.dropdown-menu.show {
		overflow: inherit !important;
    	top: 35px !important;
	}
</style>
@endsection

@section('scripts')
@endsection