@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	{{ $item->name }} {{ $item->lastname }}
</h1>
@endsection

@section('content')
<form method="POST" action="{{route('usuarios.update', $item->id)}}">
	{!! csrf_field() !!}
	<input type="hidden" name="_method" value="PUT">
	<div class="">
		<div class="col-sm-6">
			<div class="card">
				<h5 class="card-header white-text text-center py-4" style="background-color:#009688; color: white;">
					<strong>Dades Usuari</strong>
				</h5>
				<div class="form-group">
					<label for="name" class="bmd-label-floating">@Lang('Name')</label>
					<input type="text" class="form-control" id="name" name="name" value="{{ $item->name }}">
					<div class="text-danger">{{$errors->first('name')}}</div>
				</div>
				<div class="form-group">
					<label for="lastname" class="bmd-label-floating">@Lang('Lastname')</label>
					<input type="text" class="form-control" id="lastname" name="lastname" value="{{ $item->lastname }}">
					<div class="text-danger">{{$errors->first('lastname')}}</div>
				</div>
				<div class="form-group">
					<label for="email" class="bmd-label-floating">@Lang('Email')</label>
					<input type="email" class="form-control" id="email" name="email" value="{{ $item->email }}">
					<!--<span class="bmd-help">No compartiremos su mail con nadie.</span>-->
					<div class="text-danger">{{$errors->first('email')}}</div>
				</div>
				<div class="form-group">
					<label for="fecha_nacimiento" class="bmd-label-floating">@Lang('Birthdate')</label>
					<input type="text" class="form-control" id="fecha_nacimiento" name="fecha_nacimiento" value="{{ $item->fecha_nacimiento }}">
					<div class="text-danger">{{$errors->first('fecha_nacimiento')}}</div>
				</div>
				<div class="form-group">
					<label for="dni" class="bmd-label-floating">@Lang('ID card')</label>
					<input type="text" class="form-control" id="dni" name="dni" value="{{ $item->dni }}">
					<div class="text-danger">{{$errors->first('dni')}}</div>
				</div>
			</div>
		</div>
		<div class="col-sm-6">
			<div class="card">
				<h5 class="card-header info-color white-text text-center py-4" style="background-color:#009688; color: white;">
					<strong>Direcció</strong>
				</h5>
				<div class="form-group">
					<label for="city" class="bmd-label-floating">@Lang('City')</label>
					<input type="text" class="form-control" id="city" name="city" value="{{ $item->city }}">
					<div class="text-danger">{{$errors->first('city')}}</div>
				</div>
				<div class="form-group">
					<label for="address" class="bmd-label-floating">@Lang('Address')</label>
					<input type="text" class="form-control" id="address" name="address" value="{{ $item->address }}">
					<div class="text-danger">{{$errors->first('phoaddressne')}}</div>
				</div>
				<div class="form-group">
					<label for="codigo_postal" class="bmd-label-floating">@Lang('ZIP zone')</label>
					<input type="text" class="form-control" id="codigo_postal" name="codigo_postal" value="{{ $item->codigo_postal }}">
					<div class="text-danger">{{$errors->first('codigo_postal')}}</div>
				</div>
				<div class="form-group">
					<label for="mobile" class="bmd-label-floating">@Lang('Mobile')</label>
					<input type="text" class="form-control" id="mobile" name="mobile" value="{{ $item->mobile }}">
					<div class="text-danger">{{$errors->first('mobile')}}</div>
				</div>
				<div class="form-group">
					<label for="phone" class="bmd-label-floating">@Lang('Phone')</label>
					<input type="text" class="form-control" id="phone" name="phone" value="{{ $item->phone }}">
					<div class="text-danger">{{$errors->first('phone')}}</div>
				</div>
			</div>
		</div>
	</div>
	<div>
		<div class="col-sm-6" style="margin-top: 50px;">
			<div class="form-group">
				<label for="role" class="bmd-label-floating">@Lang('Role')</label>
				<select class="custom-select col-md-12" name="role" data-live-search="true" title="@Lang('Role')">
					@foreach ($rols as $rol)
						<option value="{{$rol->name}}" {{ ($rol->name == $item->role ? 'selected' : '') }}>{{__($rol->name)}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="calendario_nivel_id" class="bmd-label-floating">@Lang('Level')</label>
				<select name="calendario_nivel_id" class="custom-select col-md-12" data-live-search="true">
					<option value="0">@Lang('No item selected')</option>
					@foreach ($niveles as $nivel)
						<option value="{{$nivel->id}}" {{ ($item->nivel()->exists() && $nivel->id == $item->nivel->id ? 'selected' : '') }}>{{$nivel->name}}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label for="calendario_subnivel_id" class="bmd-label-floating">@Lang('Sublevel')</label>
				<select name="calendario_subnivel_id" class="custom-select col-md-12" data-live-search="true">
					<option value="0">@Lang('No item selected')</option>
				@foreach ($subniveles as $subnivel)
					<option value="{{$subnivel->id}}" {{ ($item->subnivel()->exists() && $subnivel->id == $item->subnivel->id ? 'selected' : '') }}>{{$subnivel->name}}</option>
				@endforeach
				</select>
			</div>
		</div>
		<div class="col-sm-6" style="margin-top: 50px;">
			<div class="card">
				<h5 class="card-header info-color white-text text-center py-4" style="background-color:#009688; color: white;">
					<strong>Contrasenya</strong>
				</h5>
				<div class="form-group">
					<label for="password" class="bmd-label-floating">@Lang('Password')</label>
					<input type="password" class="form-control" id="password" name="password">
					<div class="text-danger">{{$errors->first('password')}}</div>
				</div>
				<div class="form-group">
					<label for="password_confirmation" class="bmd-label-floating">@Lang('Confirm password')</label>
					<input type="password" class="form-control" id="password_confirmation" name="password_confirmation">
				</div>
			</div>
		</div>
	</div>
	<div class="">
		<div class="col-sm-12">
			{{--<div class="form-group">
				<label for="empleado_dni" class="bmd-label-floating">@Lang('Employee')</label>
				<select class="form-control custom-select" name="dni" data-live-search="true" title="@Lang('Employee')">
					@foreach ($empleados as $empleado)
						<option value="{{$empleado->dni}}" {{ ($empleado->user_id == $item->id ? 'selected' : '') }}>{{$empleado->nombre}} ({{$empleado->dni}})</option>
					@endforeach
				</select>
			</div>--}}
			<hr><br><br>
			<div class="form-group custom-control custom-checkbox">
				@if ( $item->consentiment_naixement ) @php( $birth = 'checked' )
				@else @php( $birth = '' )
				@endif 
				<input type="checkbox" class="custom-control-input" name="consentiment_naixement" id="birth" {{ $birth }}>
				<label class="custom-control-label" for="birth">@Lang('Would you like Batallé to congratulate you for your birthday?')</label>
			</div>
			<br>
			<div class="form-group custom-control custom-checkbox">
				@if ( $item->lopd ) @php( $lopd = 'checked' )
				@else @php( $lopd = '' )
				@endif 
				<input type="checkbox" class="custom-control-input" name="lopd" id="lopd" {{ $lopd }}>
				<label class="custom-control-label" for="lopd">@Lang('I have read and I accept the privacy policy of the BATTLE GROUP')</label>
			</div>
			<div class="ml-4 radio">
				@if ( $item->consentiment_privacitat )
					@php( $consentiment_privacitat_si = 'checked' )
					@php( $consentiment_privacitat_no = '' )
				@else
					@php( $consentiment_privacitat_si = '' )
					@php( $consentiment_privacitat_no = 'checked' )
				@endif
				<label>
					<input type="radio" value="1" name="consentiment_privacitat" id="consentiment_privacitat_si" {{$consentiment_privacitat_si}}>
					@Lang('I consent to the processing of my image in accordance with the advertising purposes described above.')
				</label>
				<label>
					<input type="radio" value="0" name="consentiment_privacitat" id="consentiment_privacitat_no" {{$consentiment_privacitat_no}}>
					@Lang('I do not give my consent for the treatment of my image in accordance with the advertising purposes described above.')
				</label>
			</div>
			<br>
			<br>
			<button type="button" class="btn btn-default" onclick="window.location.href='{{url('admin/configuracion/usuarios')}}'">@Lang('Cancel')</button>
			<button type="submit" class="btn btn-primary btn-raised">@Lang('Update')</button>
		</div>
	</div>
</form>

<div class="col-sm-12 mt-5 mb-5">
	<h2>
		@Lang('Personalized holiday pending days')
		<a href="{{ route('configuracion.usuarios.years.create', $item->id) }}" class="btn btn-default">@Lang('Add year')</a>
	</h2>
	<table class="table table-bordered">
		<thead>
			<tr>
				<th>@Lang('Year')</th>
				<th>@Lang('Days')</th>
				<th width="5%">@Lang('Actions')</th>
			</tr>
		</thead>
		<tbody>
			@forelse($years as $year)
			<tr>
				<td>{{ $year->year }}</td>
				<td>{{ $year->days }}</td>
				<td class="cela-opcions">
					<div class="btn-group">
						<button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $year->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							<i class="material-icons">more_vert</i>
						</button>
						<div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $year->id }}">
							<a class="dropdown-item" href="{{ route('configuracion.usuarios.years.edit', $year->id) }}">@Lang('Edit')</a>
							<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('configuracion.usuarios.years.destroy', [$year->id, $item->id]) }}" data-nombre="@Lang('Year'): {{$year->year}} - @Lang('Days'): {{$year->days}}">@Lang('Delete')</button>
						</div>
					</div>
				</td>
			</tr>
			@empty
			<tr>
				<td colspan="4">@Lang('No entries found.')</td>
			</tr>
			@endforelse
		</tbody>
	</table>
	{{ $years->links() }}
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>
@endsection

@section('footer')
@endsection

@section('styles')
<style>
	.dropdown-menu.show {
		overflow: inherit !important;
    	top: 35px !important;
	}
</style>
@endsection

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
	$('#lopd').change(function() {
		if ( this.checked ) {
			$('#consentiment_privacitat_si').prop( "disabled", false );
			$('#consentiment_privacitat_no').prop( "disabled", false );
		} else {
			$('#consentiment_privacitat_si').prop( "disabled", true );
			$('#consentiment_privacitat_no').prop( "disabled", true );
			$('#consentiment_privacitat_si').prop( "checked", false );
			$('#consentiment_privacitat_no').prop( "checked", true );
		}
	});
	$(document).ready(function() {
		var ischecked = $('#lopd').prop( "checked");
		if ( ischecked ) {
			$('#consentiment_privacitat_si').prop( "disabled", false );
			$('#consentiment_privacitat_no').prop( "disabled", false );
		} else {
			$('#consentiment_privacitat_si').prop( "disabled", true );
			$('#consentiment_privacitat_no').prop( "disabled", true );
			$('#consentiment_privacitat_si').prop( "checked", false );
			$('#consentiment_privacitat_no').prop( "checked", true );
		}
	});
</script>
@endsection