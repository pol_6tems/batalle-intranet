@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Menu'): @Lang('Add new')
    </h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('configuracion.menus.store') }}" method="post">
        {{ csrf_field() }}

        @Lang('Order')
        <br />
        <input type="number" min="0" name="order" value="{{ old('order') }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Name')
        <br />
        <input type="text" name="name" value="{{ old('name') }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Icon')
        <br />
        <input type="text" name="icon" value="{{ old('icon') }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        <div class="form-group">
            <label for="role" class="bmd-label-floating">@Lang('Role')</label>
            <select class="form-control custom-select" name="rol_id" data-live-search="true" title="@Lang('Role')">
            @foreach ($rols as $rol)
                <option value="{{$rol->id}}" {{($rol->name == 'user' ? 'selected' : '')}} >{{__($rol->name)}}</option>
            @endforeach
            </select>
        </div>
        <input onclick="window.location.href='{{ route('configuracion.menus.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
    </form>

</div>
@endsection