@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons pr-2 admin-menu-icona">menu</i>
        @Lang('Submenu'): @Lang('Add new')
    </h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('configuracion.menus.subs.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="menu_id" value="{{$id}}"/>

        <div class="form-group">
            <label for="order" class="bmd-label-floating">@Lang('Order')</label>
            <input type="number" min="0" name="order" value="{{ old('order') }}" style="font-size: 30px;width: 100%;"/>
        </div>
        <div class="form-group">
            <label for="name" class="bmd-label-floating">@Lang('Name')</label>
            <input type="text" name="name" value="{{ old('name') }}" style="font-size: 30px;width: 100%;" required/>
        </div>
        <div class="card form-group">
            <h5 class="card-header bg-light">
                <strong>@Lang('Url')</strong>
            </h5>
            <div class="form-group">
                <label for="url_type" class="bmd-label-floating" style="top: 0.4rem;">@Lang('Type')</label>
                <select class="form-control custom-select" name="url_type" data-live-search="true" title="@Lang('Type')" required>
                    <option value="route" selected>ROUTE</option>
                    <option value="url">URL</option>
                </select>
            </div>
            <div class="form-group">
                <label for="url" class="bmd-label-floating">@Lang('Url')</label>
                <input type="text" name="url" value="{{ old('url') }}" style="font-size: 30px;width: 100%;" required/>
            </div>
        </div>
        <div class="form-group">
            <label for="rol_id" class="bmd-label-floating">@Lang('Role')</label>
            <select class="form-control custom-select" name="rol_id" data-live-search="true" title="@Lang('Role')" required>
            @foreach ($rols as $rol)
                <option value="{{$rol->id}}" {{($rol->name == 'user' ? 'selected' : '')}} >{{__($rol->name)}}</option>
            @endforeach
            </select>
        </div>
        <div class="form-group">
            <label for="requests" class="bmd-label-floating">@Lang('Requests')</label>
            <input type="text" name="requests" value="{{ old('requests') }}" style="font-size: 30px;width: 100%;" required/>
        </div>
        <div class="form-group">
            <label for="controller" class="bmd-label-floating">@Lang('Controller')</label>
            <input type="text" name="controller" value="{{ ('controller') }}" style="font-size: 30px;width: 100%;"/>
        </div>

        <input onclick="window.location.href='{{ route('configuracion.menus.edit', $id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
    </form>

</div>
@endsection