@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons pr-2 admin-menu-icona">tune</i>
        @Lang('Role'): {{__($item->name)}} ({{$item->name}})
    </h1>
</div>

<div class="panel-heading">
    <h3>
        @Lang('Users')
    </h3>
</div>

<div class="panel-body">
    <table id="tabla" class="table table-hover table-bordered">
        <thead>
            <tr>
                <th>@Lang('Name')</th>
                <th>@Lang('Email')</th>
                <th>@Lang('ID card')</th>
                <th>@Lang('Level')</th>
                <th>@Lang('Sublevel')</th>
            </tr>
        </thead>
        <tbody>
            @forelse($item->users as $user)
            <tr>
                <td>{{ $user->name }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->dni }}</td>
                <td>@if ( $user->nivel()->exists() ) {{ $user->nivel->name }} @else - @endif</td>
			    <td>@if ( $user->subnivel()->exists() ) {{ $user->subnivel->name }} @else - @endif</td>
            </tr>
            @empty
                <tr>
                    <td colspan="3">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>

</div>
@endsection

@section('styles')
<link href="{{asset('css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<style>
    .navbar {
        margin-bottom: 0;
        border-radius: 0px;
        border: none;
        min-height: inherit;
    }
    .bmd-layout-content a {
        color: #666;
    }
    .row { width: 100% !important; }
</style>
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<!--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
-->
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $('#tabla').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
        });
    });
</script>
@endsection