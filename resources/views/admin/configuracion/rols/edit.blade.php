@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Edit')</h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('configuracion.rols.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        
        @Lang('Name'):
        <br />
        <input type="text" name="name" value="{{ $item->name }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Level')
        <br/>
        <input type="number" min="0" name="level" value="{{ $item->level }}" style="font-size: 30px;width: 100%;" required/>
        <br /><br />
        <input onclick="window.location.href='{{ route('configuracion.rols.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
             
        </div>
    </form>
</div>
@endsection