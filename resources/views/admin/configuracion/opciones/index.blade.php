@extends('layouts.admin')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">tune</i>
	@Lang('Options')
</h1>
@endsection

@section('content')
<form class="form-inline" method="POST" action="{{ route('opciones.store') }}">
    {!! csrf_field() !!}
    <div class="form-group mr-4">
        <label for="key" class="bmd-label-floating">@Lang('Key')</label>
        <input type="text" name="key" class="form-control" placeholder="">
    </div>
    <div class="form-group mr-4">
        <label for="value" class="bmd-label-floating">@Lang('Value')</label>
        <input type="text" name="value" class="form-control" placeholder="">
    </div>
    <div class="form-group mr-4">
        <label for="descripcion" class="bmd-label-floating">@Lang('Description')</label>
        <input type="text" name="descripcion" class="form-control" placeholder="">
    </div>
    <span class="form-group bmd-form-group"> <!-- needed to match padding for floating labels -->
        <button type="submit" class="btn btn-default btn-raised">Add</button>
    </span>
</form>

<table id="tabla-traducciones" class="table table-hover table-bordered">
    <thead>
    <tr>
        <th>@Lang('Key')</th>
        <th>@Lang('Value')</th>
        <th>@Lang('Description')</th>
        <th width="80px;">@Lang('Action')</th>
    </tr>
    </thead>
    <tbody>
        @foreach($opciones as $item)
            <tr>
                <td><a href="#" class="translate-key" data-title="" data-type="text" data-pk="{{ $item->key }}" data-url="{{ route('opciones.update_key') }}">{{ $item->key }}</a></td>
                <td><a href="#" data-title="" class="translate" data-code="{{ $item->value }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('opciones.update_value') }}">{{ $item->value }}</a></td>
                <td><a href="#" data-title="" class="translate" data-code="{{ $item->descripcion }}" data-type="textarea" data-pk="{{ $item->key }}" data-url="{{ route('opciones.update_desc') }}">{{ $item->descripcion }}</a></td>
                <td><button data-action="{{ route('opciones.destroy', base64_encode($item->key)) }}" class="btn btn-danger btn-xs remove-key">@Lang('Destroy')</button></td>
            </tr>
        @endforeach
    </tbody>
</table>
@endsection

@section('styles')
<link href="{{asset('css/dataTables.bootstrap.min.css')}}" rel="stylesheet" type="text/css">
<style>
    .navbar {
        margin-bottom: 0;
        border-radius: 0px;
        border: none;
        min-height: inherit;
    }
    .bmd-layout-content a {
        color: #666;
    }
    .row { width: 100% !important; }
</style>
@endsection

@section('scripts')
<script src="{{asset('js/bootstrap.min.js')}}"></script>
<script src="{{asset('js/bootstrap-editable.min.js')}}"></script>
<!--
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/x-editable/1.5.0/bootstrap3-editable/js/bootstrap-editable.min.js"></script>
-->
<script src="{{asset('js/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/dataTables.bootstrap.min.js')}}"></script>
<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });


    $('.translate').editable({
        params: function(params) {
            params.code = $(this).editable().data('code');
            return params;
        }
    });


    $('.translate-key').editable({
        validate: function(value) {
            if($.trim(value) == '') {
                return '@Lang('Key is required')';
            }
        }
    });


    $('body').on('click', '.remove-key', function(){
        var cObj = $(this);


        if (confirm("@Lang('Are you sure to delete it?')")) {
            $.ajax({
                url: cObj.data('action'),
                method: 'DELETE',
                success: function(data) {
                    cObj.parents("tr").remove();
                    alert("@Lang('Item deleted succesfully.')");
                }
            });
        }
    });

    $(document).ready(function() {
        $('#tabla-traducciones').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
        });
        $('#tabla-rutas').DataTable({
			"pageLength": 25,
			"language": { "url": "{{asset('js/datatables/Catalan.json')}}" }
		});
    });
</script>
@endsection