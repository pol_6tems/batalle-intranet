@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
        <i class="material-icons admin-menu-icona">message</i>
        @Lang('Contact') @Lang('Messages')
    </h1>
</div>

<div class="panel-body">
<div class="container">
<div class="messaging">
    <div class="inbox_msg">

        <!-- CONTACTES -->
        <div class="inbox_people">
            <div class="headind_srch">
                <div class="recent_heading">
                    <h4>@Lang('Users')</h4>
                </div>
                {{--
                <div class="srch_bar">
                    <div class="stylish-input-group">
                        <input type="text" class="search-bar"  placeholder="Search" >
                        <span class="input-group-addon">
                            <button type="button"> <i class="fa fa-search" aria-hidden="true"></i> </button>
                        </span>
                    </div>
                </div>
                --}}
            </div>
          

            <div class="inbox_chat">

            @php($active_chat = null)
            @foreach ( $users as $key => $user )
                @if($key == 0) @php($active_chat = $user->id) @endif
                @php($last_mensaje = $user->contactar_mensajes()->latest()->first())
                <div class="chat_list user_id-{{$user->id}} @if($key == 0) active_chat @endif" onclick="seleccionar_chat( {{$user->id}} );">
                    <div class="chat_people">
                        <div class="chat_img"> <img class="avatar-icon" src="/storage/avatars/{{ $user->avatar }}"> </div>
                        <div class="chat_ib {{ !$last_mensaje->visto ? 'nuevo_mensaje' : '' }}">
                            <h5>{{$user->fullname()}} <span class="chat_date">{{$last_mensaje->created_at->format('G:i · d/m/Y')}}</span></h5>
                            <p>{{ $last_mensaje->mensaje }}</p>
                        </div>
                    </div>
                </div>
            @endforeach
                
            </div>
        </div>
        <!-- end CONTACTES -->

        <!-- MISSATGES -->
        <div class="mesgs">
        
        @foreach ( $users as $key => $user )
            @php( $mensajes = \App\User::find($user->id)->contactar_mensajes()->get()->sortBy('created_at') )
            <div class="msg_history" id="user_id-{{$user->id}}" style="display:none;">
            
                @foreach ( $mensajes as $mensaje )
                    @if ( $mensaje->from == $user->id )
                        <div class="incoming_msg">
                            <div class="incoming_msg_img"> <img class="avatar-icon" src="/storage/avatars/{{ $mensaje->from()->first()->avatar }}"> </div>
                            <div class="received_msg">
                                <div class="received_withd_msg">
                                    <p>{{$mensaje->mensaje}}</p>
                                    @if ($mensaje->fitxer != '')
                                    <span class="fitxer"><a href="{{ asset('storage/'.$mensaje->fitxer) }}"><i class="material-icons">description</i></a></span>
                                    @endif
                                    <span class="time_date">{{$mensaje->created_at->format('G:i · d/m/Y')}}</span>
                                </div>
                            </div>
                        </div>
                    @else
                        <div class="outgoing_msg">
                            <div class="sent_msg">
                            <p>{{$mensaje->mensaje}}</p>
                                <span class="time_date">{{$mensaje->created_at->format('G:i · d/m/Y')}}</span>
                            </div>
                        </div>
                    @endif

                
                @endforeach

            </div>

        @endforeach
            
            <!-- TYPE A MESSAGE -->
            <div class="type_msg">
                <div class="input_msg_write">
                    {{--<input id="mensaje-box" type="text" class="write_msg" placeholder="@Lang('Type a message')" />--}}
                    <textarea id="mensaje-box" type="text" class="write_msg" placeholder="@Lang('Type a message')"></textarea>
                    <button id="enviar_missatge_btn" title="@Lang('Send')" class="msg_send_btn btn btn-info bmd-btn-fab bmd-btn-fab-sm" type="button"><i style="line-height: 1.25;" class="material-icons" aria-hidden="true">send</i></button>
                </div>
            </div>
            
        </div>
        <!-- end MISSATGES -->

    </div>
</div>
</div>
</div>
@endsection	

@section('styles')
<link href="{{asset('css/endo-admin-messaging.css')}}" rel="stylesheet" type="text/css">
@endsection

@section('scripts')
<script>
    var $active_chat = {{$active_chat}};
    var $last_update_chat = new Date();

    $(document).ready(function(){
        seleccionar_chat( '{{ $active_chat }}' );
    });

    function seleccionar_chat( user_id ) {
        $('.msg_history').hide();
        $('.chat_list').removeClass('active_chat');
        $('.chat_list.user_id-' + user_id).addClass('active_chat');
        $('#user_id-' + user_id).show();
        $('#user_id-' + user_id).animate({ scrollTop: $('#user_id-' + user_id).prop("scrollHeight")}, 0);
        $active_chat = user_id;

        // Mensaje visto
        $.ajax({
            url: '{{ route('mensajes.visto') }}',
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'user_id': user_id},
            success: function(data) {
                $('.chat_list.user_id-' + user_id + ' .chat_ib.nuevo_mensaje').removeClass('nuevo_mensaje');
            }
        });
    }

    function date_to_str(d) {
        var d_aux = d.getDate();
        var m_aux = d.getMonth() + 1;
        //return d.getFullYear() + '-' + (m_aux > 9 ? m_aux : '0' + m_aux) + '-' + (d_aux > 9 ? d_aux : '0' + d_aux);
        return (d_aux > 9 ? d_aux : '0' + d_aux) + '/' + (m_aux > 9 ? m_aux : '0' + m_aux) + '/' + d.getFullYear();
    }

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    $('#enviar_missatge_btn').click(function(){
        var user_id = $active_chat;
        var mensaje = $('#mensaje-box').val();
        
        $.ajax({
            url: '{{ route('mensajes.store') }}',
            method: 'POST',
            data: {'_token': '{{csrf_token()}}', 'mensaje': mensaje, 'user_id': user_id},
            success: function(data) {
                if ( data == 'KO' ) alert('Error al enviar el missatge.')
                var ahora = new Date();
                var hora = ahora.toLocaleString('es-ES', { hour: '2-digit', minute: '2-digit', hour12: false });
                var fecha = date_to_str(ahora);
                $('#user_id-' + user_id).append(`
                    <div class="outgoing_msg">
                        <div class="sent_msg">
                        <p>` + mensaje + `</p>
                            <span class="time_date">` + hora + ` · ` + fecha + `</span>
                        </div>
                    </div>
                `);
                $('#user_id-' + user_id).animate({ scrollTop: $('#user_id-' + user_id).prop("scrollHeight")}, 0);
                $('#mensaje-box').val('');
            }
        });
    });
</script>
@endsection