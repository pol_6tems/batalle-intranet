@extends('layouts.app')

@section('header')
<h1>
	<i class="material-icons pr-2 admin-menu-icona">supervisor_account</i>
	Perfil
</h1>
@endsection

@section('content')

<section class="single-page register-done">
<div class="row">
        @if ($message = Session::get('success'))

            <div class="alert alert-success alert-block">

                <button type="button" class="close" data-dismiss="alert">×</button>

                <strong>{{ $message }}</strong>

            </div>

        @endif

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
    </div>
    <div class="row justify-content-center">

        <div class="profile-header-container">
            <div class="profile-header-img">
                <img class="avatar rounded-circle" src="/storage/avatars/{{ $user->avatar }}" />
                <!-- badge -->
                <div class="rank-label-container">
                    <span class="label label-default rank-label">{{$user->name}}</span>
                </div>
            </div>
        </div>

    </div>
    <div class="row justify-content-center">
        <form action="{{url('perfil')}}" method="post" enctype="multipart/form-data">
            {!! csrf_field() !!}
            <div class="form-group">
                <input type="file" class="form-control-file" name="avatar" id="avatarFile" aria-describedby="fileHelp">
                <small id="fileHelp" class="form-text text-muted">Please upload a valid image file. Size of image should not be more than 2MB.</small>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</section>
@endsection

@section('scripts')
<script>
    function setHeight() {
        var getHeaderHeight = $('header').outerHeight();
        var getFooterHeight = $('footer').outerHeight();
        var getWindowHeight =$(window).height();
        var setHeight = getWindowHeight - (getHeaderHeight + getFooterHeight);
        $('.register-done').css('min-height', setHeight);	
    }
            
        
    $(document).ready(function() {	
        setHeight()
    });

    $(window).resize(function(){
        setHeight()
    });
</script>
@endsection