@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Add new')</h1>
</div>

<div class="panel-body">
    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.subniveles.store') }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="publish" />

        @Lang('Name'):
        <br />
        <input type="text" name="name" value="{{ old('name') }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Selection'):
        <br />
        <select name="sel_dia_o_semana" class="custom-select" style="font-size: 30px;width: 100%;height:51px;">
            <option value="dia" selected>@Lang('dia')</option>
            <option value="semana">@Lang('semana')</option>
        </select>
        <br /><br />
        @Lang('Blocked per request'):
        <br />
        <select name="no_bloquejat" class="custom-select" style="font-size: 30px;width: 100%;height:51px;">
            <option value="0" selected>@Lang('Yes')</option>
            <option value="1">@Lang('No')</option>
        </select>
        <br /><br />
        @Lang('Level'):
        <br />
        <select name="calendario_nivel_id" class="custom-select" style="font-size: 30px;width: 100%;height:51px;">
        @foreach ($niveles as $item)
            <option value="{{$item->id}}">{{$item->name}}</option>
        @endforeach
        </select>
        <br /><br />
        <input onclick="window.location.href='{{ route('calendario.subniveles.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Publish')" class="btn btn-raised btn-primary" />
    </form>

</div>
@endsection