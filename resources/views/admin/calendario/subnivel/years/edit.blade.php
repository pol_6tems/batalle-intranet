@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Edit')</h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.subniveles.years.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_subnivel_id" value="{{ $item->calendario_subnivel_id }}" />
        
        @Lang('Year'):
        <br />
        <input required type="number" min="0" name="year" value="{{ $item->year }}" style="font-size: 30px;width: 100%;"/>
        <br />
        <br />
        @Lang('Days'):
        <br />
        <input required type="number" min="0" name="days" value="{{ $item->days }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        <input onclick="window.location.href='{{ route('calendario.subniveles.edit', $item->calendario_subnivel_id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
             
        </div>
    </form>

</div>
@endsection