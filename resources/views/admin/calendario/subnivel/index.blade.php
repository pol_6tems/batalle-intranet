@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>
    @Lang('Vacations') - @Lang('Levels')
        @can('create', \App\CalendarioNivel::class)
            <a href="{{ route('calendario.subniveles.create') }}" class="btn btn-default">@Lang('Add new')</a>
        @endcan
    </h1>
</div>

<div class="panel-body">
    <table class="table table-bordered">
        <thead>
            <tr>
                @can('delete', \App\CalendarioNivel::class)
                    <th width="5%"><input type="checkbox" class="checkbox_all"></th>
                @endcan
                <th>@Lang('Name')</th>
                <th>@Lang('Level')</th>
                <th width="10%">@Lang('Selection')</th>
                <th width="15%">@Lang('Blocked per request')</th>
                <th width="5%">@Lang('Limit')</th>
                @can('update', \App\CalendarioNivel::class)
                    <th width="5%">@Lang('Actions')</th>
                @endcan
            </tr>
        </thead>
        <tbody>
            @forelse($items as $item)
            <tr>
                @can('delete', \App\CalendarioNivel::class)
                    <td><input type="checkbox" class="checkbox_delete" name="entries_to_delete[]" value="{{ $item->id }}" /></td>
                @endcan
                <td>
                    <a href="{{ route('calendario.subniveles.edit', $item->id) }}">
                        {{ $item->name }}
                    </a>
                </td>
                <td>
                    <a href="{{ route('calendario.niveles.edit', $item->nivel->id) }}">
                        {{ $item->nivel->name }}
                    </a>
                </td>
                <td>{{ __($item->sel_dia_o_semana) }}</td>
                <td>@if ($item->no_bloquejat == 1) @Lang('No') @else @Lang('Yes') @endif</td>
                <td>{{ $item->max_limit }}</td>

                @can('update', \App\CalendarioNivel::class)
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $item->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $item->id }}">
                                <a class="dropdown-item" href="{{ route('calendario.subniveles.edit', $item->id) }}">@Lang('Edit')</a>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('calendario.subniveles.destroy', $item->id) }}" data-nombre="{{$item->title}}">@Lang('Delete')</button>
                            </div>
                        </div>
                    </td>
                @endcan
            </tr>
            @empty
                <tr>
                    <td colspan="3">@Lang('No entries found.')</td>
                </tr>
            @endforelse
        </tbody>
    </table>
    {{ $items->links() }}

    @can('delete', \App\CalendarioNivel::class)
        <form action="{{ route('calendario.subniveles.mass_destroy') }}" method="post" onsubmit="return confirm('Are you sure?');">
            {{ csrf_field() }}
            <input type="hidden" name="_method" value="DELETE">
            <input type="hidden" name="ids" id="ids" value="" />
            <input type="submit" value="Delete selected" class="btn btn-danger" />
        </form>
    @endcan

</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">Eliminar</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
					<button type="submit" class="btn btn-danger">Eliminar</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection	

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
</script>
<script>
    function getIDs() {
        var ids = [];
        $('.checkbox_delete').each(function () {
            if($(this).is(":checked")) {
                ids.push($(this).val());
            }
        });
        $('#ids').val(ids.join());
    }

    $(".checkbox_all").click(function(){
        $('input.checkbox_delete').prop('checked', this.checked);
        getIDs();
    });

    $('.checkbox_delete').change(function() {
        getIDs();
    });
</script>
@endsection