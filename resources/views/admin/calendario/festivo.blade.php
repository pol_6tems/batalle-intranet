@extends('layouts.admin')

@section('header')
<h3 class="font-weight-bold">
	<i class="material-icons pr-2 admin-menu-icona">date_range</i>
	@Lang('Holidays')
</h3>
<form class="ml-3" id="form_enviar_dies" action="{{route('calendario.festivos.store')}}" method="post">
    {{ csrf_field() }}
    <input id="dies" name="dies" type="hidden" />
    <input class="btn btn-raised btn-primary mr-2" type="submit" value="@Lang('Save')">
    <button type="button" onclick="window.location.href='{{route('calendario.festivos.index')}}'" class="btn btn-default btn-raised">@Lang('Cancel')</button>
</form>
@endsection

@section('content')
<div id='calendar'></div>
@endsection

@section('styles')
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/fullcalendar.css')}}" />
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/fullcalendar.print.css')}}" media='print' />
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/scheduler.min.css')}}" />
<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  .fc-license-message { display: none !important; }
  .fc-scroller { height: auto !important; }
</style>
@endsection

@section('scripts')

<script src="{{asset('js/fullcalendar-3.9.0/lib/moment.min.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/fullcalendar.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/scheduler.min.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/locale-all.js')}}"></script>
<script>
var $festivos = {!! json_encode($festivos) !!};
var $niveles = {!! json_encode($niveles) !!};
var $fullcalendar_events = [];
var $fullcalendar_resources = [];

$(document).ready(function() {
  
  var today = new Date().toISOString().slice(0,10);

  // Niveles -> Subniveles -> Usuarios
  $.each($niveles, function($index, $nivel) {
    if ( typeof($nivel) !== 'undefined' ) {
      var $subniveles = [];
      if ( $nivel.subniveles.length > 0 ) {
        $.each($nivel.subniveles, function($index2, $subnivel) {
          $subniveles.push({
            'id': 'subnivel-' + $subnivel.id,
            'title': $subnivel.name
          });
        });
      }
      $fullcalendar_resources.push({
        id: 'nivel-' + $nivel.id,
        title: $nivel.name,
        children: $subniveles
      });
    }
  });

  // Festivos
 $.each($festivos, function($any, $niveles) {
    $.each($niveles, function($nivel_id, $dias) {
        $.each($dias, function($index, $dia) {
            if ( typeof($dia) !== 'undefined' ) {
                $fullcalendar_events.push({
                    id: $any + '_' + $nivel_id + '_' + $dia,
                    title: '@Lang('Holiday')',
                    start: $dia,
                    className: 'holiday',
                    estado: 'holiday',
                    any: $any,
                    fecha: $dia,
                    resourceId: $nivel_id,
                });
            }
        });
    });
 });

  // Init Calendar
  $('#calendar').fullCalendar({
    now: today,
    defaultDate: today,
    defaultView: 'timelineYear',
    header: {
      left: 'prev,next',
      center: 'title',
      //right: 'month,basicWeek,basicDay,timelineDay,timelineTenDay,timelineMonth,timelineYear'
      right: 'timelineYear,timelineMonth,timelineWeek,timelineDay',
    },
    buttonText: { timelineYear: '@Lang('Year')' },
    views: {
        timelineDay: {
            type: 'timeline',
            duration: { days: 1 },
            groupByResource: true,
            slotDuration: '24:00:00'
        },
        timelineWeek: {
            type: 'timelineWeek',
            groupByResource: true,
            slotDuration: '24:00:00'
        }
    },
    visibleRange: {
        start: '{{date('Y')}}-01-01',
        end: '{{date('Y') + 1}}-12-31'
    },
    validRange: {
        start: '{{date('Y')}}-01-01',
        end: '{{date('Y') + 1}}-12-31'
    },
    locale: '{{$language_code}}',
    editable: false, // Draggable or not
    eventLimit: true, // allow "more" link when too many events
    navLinks: true, // can click day/week names to navigate views
    resourceAreaWidth: '20%',
    resourceLabelText: '@Lang('Levels')',
    resources: $fullcalendar_resources,
    events: $fullcalendar_events,
    eventClick: function(calEvent, jsEvent, view) {
        $('#calendar').fullCalendar('removeEvents', calEvent.id);
    },
    loading: function (view) {
      var elemento = $('.fc-widget-header[data-date=' + today + ']').position();
      if ( typeof(elemento) !== 'undefined' ) {
        $('.fc-scroller').animate({scrollLeft: elemento.left}, 500);
      }
    },
    dayClick: function(date, allDay, jsEvent, view) {
        // view = nivel
        date = date._d;
        var day = date.getDate() >= 10 ? date.getDate() : '0'+ date.getDate(),
            mon = (date.getMonth()+1) >= 10 ? (date.getMonth()+1) : '0'+ (date.getMonth()+1);
            tDate = date.getFullYear() +'-'+ mon +'-'+ day;
        
        $('#calendar').fullCalendar('removeEvents', date.getFullYear() + '_' + view.id + '_' + tDate);

        $('#calendar').fullCalendar('renderEvent', {
            id: date.getFullYear() + '_' + view.id + '_' + tDate,
            title: '@Lang('Holiday')',
            start: tDate,
            className: 'holiday',
            estado: 'holiday',
            any: date.getFullYear(),
            fecha: tDate,
            resourceId: view.id,
        }, true);
    }
  });
  // end Init Calendar

  // Enviar Dies
  $('#form_enviar_dies').submit(function(e) {
    var dies_a_enviar = [];
    var all_events = $('#calendar').fullCalendar('clientEvents');
    
    $.each(all_events, function($index, $evento) {
      dies_a_enviar.push({
        'id': $evento.id,
        'className': $evento.className,
        'estado': $evento.estado,
        'usuario_id': $evento.usuario_id,
        'any': $evento.any,
        'fecha': $evento.fecha,
        'nivel': $evento.resourceId
      });
    });
    
    $('#dies').val(JSON.stringify(dies_a_enviar));
    
    return true;
  });
});

// Array.prototype.remove
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/* AGAFAR TOTS ELS DISSABTE I DIUMENGES */
function getAllDissabtesoDiumenges(year, dissabte_o_diumenge, month = null) {
    var sat = new Array();
    var sun = new Array();
    if ( month == null ) {
        $mes_ini = 0;
        $mes_fi = 12;
    } else {
        $mes_ini = $month - 1;
        $mes_fi = $month;
    }
    for ( var mes = $mes_ini; mes < $mes_fi; mes++ ) {
        var getTot = daysInMonth(mes, year);
        for ( var i = 1; i <= getTot; i++ ) {
            var newDate = new Date(year, mes, i)
            if ( newDate.getDay() == 6 ) {
                sat.push(formatDate(newDate));
            }
            if ( newDate.getDay() == 0 ) {
                sun.push(formatDate(newDate));
            }

        }
    }
    if ( dissabte_o_diumenge == 'dissabte' ) return sat;
    else return sun;
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
/* end AGAFAR TOTS ELS DISSABTE I DIUMENGES */

</script>
@endsection