@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Answer') - {{$user->fullname()}}</h1>
</div>



<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    

</div>

@endsection

@section('scripts')
<script>
</script>
@endsection