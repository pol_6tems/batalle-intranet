@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Edit')</h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.niveles.update', $item->id) }}" method="post">
        <input type="hidden" name="_method" value="PUT">
        {{ csrf_field() }}
        <input type="hidden" name="status" value="{{ $item->status }}" />
        
        @Lang('Name'):
        <br />
        <input type="text" name="name" value="{{ $item->name }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Selection'):
        <br />
        <select name="sel_dia_o_semana" class="custom-select" style="font-size: 30px;width: 100%;height:51px;">
            <option value="dia" {{ ($item->sel_dia_o_semana == 'dia' ? 'selected' : '') }}>@Lang('dia')</option>
            <option value="semana" {{ ($item->sel_dia_o_semana == 'semana' ? 'selected' : '') }}>@Lang('semana')</option>
        </select>
        <br /><br />
        @Lang('Blocked per request'):
        <br />
        <select name="no_bloquejat" class="custom-select" style="font-size: 30px;width: 100%;height:51px;">
            <option value="0" {{ ($item->no_bloquejat == 0 ? 'selected' : '') }}>@Lang('Yes')</option>
            <option value="1" {{ ($item->no_bloquejat == 1 ? 'selected' : '') }}>@Lang('No')</option>
        </select>
        <br /><br />
        @Lang('Limit'):
        <br />
        <input type="number" min="0" name="max_limit" value="{{ $item->max_limit }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        <input onclick="window.location.href='{{ route('calendario.niveles.index') }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
             
        </div>
    </form>

    <!-- CalendarioNivelYear -->
    <div class="col-sm-12 mt-5 mb-5">
        <h2>
            @Lang('Personalized holiday pending days')
            @can('create', \App\CalendarioNivel::class)
                <a href="{{ route('calendario.niveles.years.create', $item->id) }}" class="btn btn-default">@Lang('Add year')</a>
            @endcan
        </h2>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>@Lang('Year')</th>
                    <th>@Lang('Days')</th>
                    @can('update', \App\CalendarioNivel::class)
                        <th width="5%">@Lang('Actions')</th>
                    @endcan
                </tr>
            </thead>
            <tbody>
                @forelse($years as $year)
                <tr>
                    <td>{{ $year->year }}</td>
                    <td>{{ $year->days }}</td>

                    @can('update', \App\CalendarioNivel::class)
                        <td class="cela-opcions">
                            <div class="btn-group">
                                <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $year->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="material-icons">more_vert</i>
                                </button>  
                                <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $year->id }}">
                                    <a class="dropdown-item" href="{{ route('calendario.niveles.years.edit', $year->id) }}">@Lang('Edit')</a>
                                    <!--<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('calendario.niveles.destroy', $year->id) }}" data-nombre="@Lang('Year'): {{$year->year}} - @Lang('Days'): {{$year->days}}">@Lang('Delete')</button>-->
                                </div>
                            </div>
                        </td>
                    @endcan
                </tr>
                @empty
                    <tr>
                        <td colspan="4">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $years->links() }}
    </div>
    <!-- end CalendarioNivelYear -->

    <!-- CalendarioNivelWeek -->
    <div class="col-sm-12 mt-5 mb-5">
        <h2>
            @Lang('Employee limit per week')
            <a href="{{ route('calendario.niveles.weeks.create', $item->id) }}" class="btn btn-default">@Lang('Add week')</a>
        </h2>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th>@Lang('Year')</th>
                    <th>@Lang('Week')</th>
                    <th>@Lang('Limit')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($weeks as $week)
                <tr>
                    <td>{{ $week->year }}</td>
                    <td>{{ $week->week }}</td>
                    <td>{{ $week->max_limit }}</td>                    
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $week->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $week->id }}">
                                <a class="dropdown-item" href="{{ route('calendario.niveles.weeks.edit', $week->id) }}">@Lang('Edit')</a>
                                <!--<button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('calendario.niveles.destroy', $week->id) }}" data-nombre="@Lang('Year'): {{$week->year}} - @Lang('Week'): {{$week->week}}">@Lang('Delete')</button>-->
                            </div>
                        </div>
                    </td>
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="4">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $weeks->links() }}
    </div>
    <!-- end CalendarioNivelWeek -->
    
    <!-- CalendarioNivelEncarregat -->
    <div class="col-sm-12 mt-5 mb-5">
        <h2>
            @Lang('Managers')
            <a href="{{ route('calendario.niveles.encarregat.create', $item->id) }}" class="btn btn-default">@Lang('Add manager')</a>
        </h2>

        <table class="table table-bordered">
            <thead>
                <tr>
                    <th width="5%">@Lang('Id')</th>
                    <th>@Lang('Manager')</th>
                    <th>@Lang('Email')</th>
                    <th width="15%">@Lang('Role')</th>
                    <th width="5%">@Lang('Actions')</th>
                </tr>
            </thead>
            <tbody>
                @forelse($encarregats as $encarregat)
                <tr>
                    <td>{{ $encarregat->user_id }}</td>
                    <td>{{ $encarregat->user->fullname() }}</td>
                    <td>{{ $encarregat->user->email }}</td>
                    <td>{{ __($encarregat->user->role) }}</td>
                    <td class="cela-opcions">
                        <div class="btn-group">
                            <button class="btn bmd-btn-icon dropdown-toggle" type="button" id="opciones-{{ $encarregat->id }}" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <i class="material-icons">more_vert</i>
                            </button>  
                            <div class="dropdown-menu dropdown-menu-left" aria-labelledby="opciones-{{ $encarregat->id }}">
                                <a class="dropdown-item" href="{{ route('calendario.niveles.encarregat.edit', $encarregat->id) }}">@Lang('Edit')</a>
                                <button type="button" class="dropdown-item btn-danger" data-toggle="modal" data-target="#myModal" data-url="{{ route('calendario.niveles.encarregat.destroy', $encarregat->id) }}" data-nombre="@Lang('Manager'): {{ $encarregat->user->fullname() }} - @Lang('Email'): {{ $encarregat->user->email }}">@Lang('Delete')</button>
                            </div>
                        </div>
                    </td>
                    
                </tr>
                @empty
                    <tr>
                        <td colspan="4">@Lang('No entries found.')</td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $encarregats->links() }}
    </div>
    <!-- end CalendarioNivelEncarregat -->
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<form method="POST" action="" style="padding:0;margin:0;">
				{{ csrf_field() }}
				<input type="hidden" name="_method" value="DELETE">

				<div class="modal-header">
					<h5 class="modal-title" id="myModalLabel">@Lang('Delete')</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body"></div>
				<div class="modal-footer">
					<button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
					<button type="submit" class="btn btn-danger">@Lang('Delete')</button>
				</div>
			</form>
		</div>
	</div>
</div>

@endsection

@section('scripts')
<script>
   $('#myModal').on('show.bs.modal', function (event) {
		var button = $(event.relatedTarget);
		var url = button.data('url');
		var nombre = button.data('nombre');
		var modal = $(this);
		modal.find('form').attr('action', url);
		modal.find('.modal-body').html("@Lang('Are you sure to delete it?')");
	});
</script>
@endsection