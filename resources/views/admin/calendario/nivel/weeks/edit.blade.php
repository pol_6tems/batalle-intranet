@extends('layouts.admin')

@section('content')
<div class="panel-heading">
    <h1>@Lang('Edit')</h1>
</div>

<div class="panel-body">

    @if ($errors->count() > 0)
        <ul>
            @foreach($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    @endif

    <form action="{{ route('calendario.niveles.weeks.update', $item->id) }}" method="post">
        {{ csrf_field() }}
        <input type="hidden" name="calendario_nivel_id" value="{{ $item->calendario_nivel_id }}" />
        
        @Lang('Year'):
        <br />
        <input required type="number" min="0" name="year" value="{{ $item->year }}" style="font-size: 30px;width: 100%;"/>
        <br />
        <br />
        @Lang('Week'):
        <br />
        <input required type="number" min="0" name="week" value="{{ $item->week }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        @Lang('Limit'):
        <br />
        <input required type="number" min="0" name="max_limit" value="{{ $item->max_limit }}" style="font-size: 30px;width: 100%;"/>
        <br /><br />
        <input onclick="window.location.href='{{ route('calendario.niveles.edit', $item->calendario_nivel_id) }}'" type="button" value="@Lang('Cancel')" class="btn btn-raised btn-secondary" />
        <input type="submit" value="@Lang('Update')" class="btn btn-raised btn-primary" />
             
        </div>
    </form>

</div>
@endsection