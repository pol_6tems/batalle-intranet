@extends('layouts.admin')

@section('header')
<h3 class="font-weight-bold">
	<i class="material-icons pr-2 admin-menu-icona">date_range</i>
	@Lang('Days banned')
</h3>
<form class="ml-3" id="form_enviar_dies" action="{{route('calendario.vetados.store')}}" method="post">
    {{ csrf_field() }}
    <input id="dies" name="dies" type="hidden" />
    <input class="btn btn-raised btn-primary mr-2" type="submit" value="@Lang('Save')">
    <button type="button" onclick="window.location.href='{{route('calendario.vetados.index')}}'" class="btn btn-default btn-raised">@Lang('Cancel')</button>
    <button style="display:none!important;" id="seleccionar-add-dies-btn" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal" data-action="add">@Lang('Add')</button>
    <button style="display:none!important;" id="seleccionar-del-dies-btn" type="button" class="btn btn-default" data-toggle="modal" data-target="#myModal" data-action="del">@Lang('Delete')</button>
</form>
@endsection

@section('content')
<div id='calendar'></div>
<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
			<div class="modal-header">
                <h5 class="modal-title" id="myModalLabel">@Lang('Select the days banned')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="dies" class="bmd-label-floating">@Lang('Days')</label>
                    <select class="form-control custom-select" multiple data-live-search="true" id="selecciona-dies" name="dies">
                        <option value="1">@Lang('Monday')</option>
                        <option value="2">@Lang('Tuesday')</option>
                        <option value="3">@Lang('Wednesday')</option>
                        <option value="4">@Lang('Thursday')</option>
                        <option value="5">@Lang('Friday')</option>
                        <option value="6">@Lang('Saturday')</option>
                        <option value="0">@Lang('Sunday')</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="nivells" class="bmd-label-floating">@Lang('Levels')</label>
                    <select class="form-control custom-select" multiple data-live-search="true" id="selecciona-nivells" name="nivells">
                        <option value="" selected>@Lang('All')</option>
                        @foreach($niveles as $item)
                            <option value="nivel-{{$item->id}}">{{$item->name}}</option>
                            @foreach($item->subniveles as $subitem)
                                <option value="subnivel-{{$subitem->id}}">{{$subitem->name}}</option>
                            @endforeach
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">@Lang('Cancel')</button>
                <button type="submit" class="btn btn-primary">@Lang('Accept')</button>
            </div>
		</div>
	</div>
</div>
@endsection

@section('styles')
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/fullcalendar.css')}}" />
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/fullcalendar.print.css')}}" media='print' />
<link rel='stylesheet' href="{{asset('js/fullcalendar-3.9.0/scheduler.min.css')}}" />
<style>

  body {
    margin: 40px 10px;
    padding: 0;
    font-family: "Lucida Grande",Helvetica,Arial,Verdana,sans-serif;
    font-size: 14px;
  }

  #calendar {
    max-width: 900px;
    margin: 0 auto;
  }
  .fc-license-message { display: none !important; }
  .fc-scroller { height: auto !important; }
  .dropdown-menu.show {
      overflow: inherit !important;
      top: 35px !important;
  }
</style>
@endsection

@section('scripts')

<script src="{{asset('js/fullcalendar-3.9.0/lib/moment.min.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/fullcalendar.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/scheduler.min.js')}}"></script>
<script src="{{asset('js/fullcalendar-3.9.0/locale-all.js')}}"></script>
<script>
var $vetados = {!! json_encode($vetados) !!};
var $niveles = {!! json_encode($niveles) !!};
var $fullcalendar_events = [];
var $fullcalendar_resources = [];

$(document).ready(function() {
  
  var today = new Date().toISOString().slice(0,10);

  // Niveles -> Subniveles -> Usuarios
  $.each($niveles, function($index, $nivel) {
    if ( typeof($nivel) !== 'undefined' ) {
      var $subniveles = [];
      if ( $nivel.subniveles.length > 0 ) {
        $.each($nivel.subniveles, function($index2, $subnivel) {
          $subniveles.push({
            'id': 'subnivel-' + $subnivel.id,
            'title': $subnivel.name
          });
        });
      }
      $fullcalendar_resources.push({
        id: 'nivel-' + $nivel.id,
        title: $nivel.name,
        children: $subniveles
      });
    }
  });

  // Vetados
 $.each($vetados, function($any, $niveles) {
    $.each($niveles, function($nivel_id, $dias) {
        $.each($dias, function($index, $dia) {
            if ( typeof($dia) !== 'undefined' ) {
                $fullcalendar_events.push({
                    id: $any + '_' + $nivel_id + '_' + $dia,
                    title: '@Lang('Banned')',
                    start: $dia,
                    className: 'banned',
                    estado: 'banned',
                    any: $any,
                    fecha: $dia,
                    resourceId: $nivel_id,
                });
            }
        });
    });
 });

  // Init Calendar
  $('#calendar').fullCalendar({
    now: today,
    defaultDate: today,
    defaultView: 'timelineYear',
    header: {
      left: 'prev,next add_dies del_dies',
      center: 'title',
      //right: 'month,basicWeek,basicDay,timelineDay,timelineTenDay,timelineMonth,timelineYear'
      right: 'timelineYear,timelineMonth,timelineWeek,timelineDay',
    },
    buttonText: { timelineYear: '@Lang('Year')' },
    customButtons: {
        add_dies: {
            text: '@Lang('Add') @Lang('Days banned')',
            click: function() {
                $('#seleccionar-add-dies-btn').click();
            }
        },
        del_dies: {
            text: '@Lang('Delete') @Lang('Days banned')',
            click: function() {
                $('#seleccionar-del-dies-btn').click();
            }
        }
    },
    views: {
        timelineDay: {
            type: 'timeline',
            duration: { days: 1 },
            groupByResource: true,
            slotDuration: '24:00:00'
        },
        timelineWeek: {
            type: 'timelineWeek',
            groupByResource: true,
            slotDuration: '24:00:00'
        }
    },
    visibleRange: {
        start: '{{date('Y')}}-01-01',
        end: '{{date('Y') + 1}}-12-31'
    },
    validRange: {
        start: '{{date('Y')}}-01-01',
        end: '{{date('Y') + 1}}-12-31'
    },
    locale: '{{$language_code}}',
    editable: false, // Draggable or not
    eventLimit: true, // allow "more" link when too many events
    navLinks: true, // can click day/week names to navigate views
    resourceAreaWidth: '20%',
    resourceLabelText: '@Lang('Levels')',
    resources: $fullcalendar_resources,
    events: $fullcalendar_events,
    eventClick: function(calEvent, jsEvent, view) {
        $('#calendar').fullCalendar('removeEvents', calEvent.id);
    },
    loading: function (view) {
      var elemento = $('.fc-widget-header[data-date=' + today + ']').position();
      if ( typeof(elemento) !== 'undefined' ) {
        $('.fc-scroller').animate({scrollLeft: elemento.left}, 500);
      }
    },
    dayClick: function(date, allDay, jsEvent, view) {
        // view = nivel
        date = date._d;
        var day = date.getDate() >= 10 ? date.getDate() : '0'+ date.getDate(),
            mon = (date.getMonth()+1) >= 10 ? (date.getMonth()+1) : '0'+ (date.getMonth()+1);
            tDate = date.getFullYear() +'-'+ mon +'-'+ day;
        
        $('#calendar').fullCalendar('removeEvents', date.getFullYear() + '_' + view.id + '_' + tDate);

        $('#calendar').fullCalendar('renderEvent', {
            id: date.getFullYear() + '_' + view.id + '_' + tDate,
            title: '@Lang('Banned')',
            start: tDate,
            className: 'banned',
            estado: 'banned',
            any: date.getFullYear(),
            fecha: tDate,
            resourceId: view.id,
        }, true);
    }
  });
  // end Init Calendar

  // Enviar Dies
  $('#form_enviar_dies').submit(function(e) {
    var dies_a_enviar = [];
    var all_events = $('#calendar').fullCalendar('clientEvents');
    
    $.each(all_events, function($index, $evento) {
      dies_a_enviar.push({
        'id': $evento.id,
        'className': $evento.className,
        'estado': $evento.estado,
        'usuario_id': $evento.usuario_id,
        'any': $evento.any,
        'fecha': $evento.fecha,
        'nivel': $evento.resourceId
      });
    });
    
    $('#dies').val(JSON.stringify(dies_a_enviar));
    
    return true;
  });
});

// Array.prototype.remove
Array.prototype.remove = function() {
    var what, a = arguments, L = a.length, ax;
    while (L && this.length) {
        what = a[--L];
        while ((ax = this.indexOf(what)) !== -1) {
            this.splice(ax, 1);
        }
    }
    return this;
};

/* AGAFAR TOTS ELS DISSABTE I DIUMENGES */
function getAllDaysFromWeekDayNumber(year, weekday, month = null) {
    var days = new Array();
    if ( month == null ) {
        $mes_ini = 0;
        $mes_fi = 12;
    } else {
        $mes_ini = month - 1;
        $mes_fi = month;
    }
    for ( var mes = $mes_ini; mes < $mes_fi; mes++ ) {
        var getTot = daysInMonth(mes + 1, year);
        for ( var i = 1; i <= getTot; i++ ) {
            var newDate = new Date(year, mes, i)
            if ( newDate.getDay() == weekday ) {
                days.push(formatDate(newDate));
            }
        }
    }
    return days;
}

function daysInMonth(month,year) {
    return new Date(year, month, 0).getDate();
}

function formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
/* end AGAFAR TOTS ELS DISSABTE I DIUMENGES */
</script>
<script>
var action = '';
$('#myModal').on('show.bs.modal', function (event) {
    var button = $(event.relatedTarget);
    action = button.data('action');
});
$('#myModal').find('button[type=submit]').on('click', function() {
    if ( action != '' ) afegir_dies_automatic(action);
    action = '';
});
function afegir_dies_automatic(add_or_remove = 'add') {
    var dies_sel = $('#selecciona-dies').val();

    if ( dies_sel === '' ) return;

    var nivells_sel = $('#selecciona-nivells').val();    
    var any_calendari = $('#calendar').fullCalendar('getView').start.format('YYYY');
    var mes_calendari = null;
    if ( $('#calendar').fullCalendar('getView').name !== 'timelineYear' ) {
        mes_calendari = $('#calendar').fullCalendar('getView').start.format('MM');
    }
    var eventos_nuevos = [];
    var eventos_nuevos_del = [];
    $.each(dies_sel, function(index, dia_sel) {
        dies = getAllDaysFromWeekDayNumber(any_calendari, dia_sel, mes_calendari);
        $.each(dies, function(index2, dia) {
            weekday = new Date(dia).getDay();
            $.each(nivells_sel, function(index3, nivell_sel) {
                if ( nivell_sel == '' ) {
                    $("#selecciona-nivells option").each(function() {
                        nivell_id = $(this).val();
                        if ( nivell_id != '' ) {
                            eventos_nuevos_del.push(any_calendari + '_' + nivell_id + '_' + dia);
                            if ( add_or_remove == 'add' ) {
                                eventos_nuevos.push({
                                    id: any_calendari + '_' + nivell_id + '_' + dia,
                                    title: '@Lang('Banned')',
                                    start: dia,
                                    className: 'banned',
                                    estado: 'banned',
                                    any: any_calendari,
                                    fecha: dia,
                                    resourceId: nivell_id,
                                });
                            }
                        }
                    });
                } else {
                    nivell_id = nivell_sel;
                    eventos_nuevos_del.push(any_calendari + '_' + nivell_id + '_' + dia);
                    if ( add_or_remove == 'add' ) {
                        eventos_nuevos.push({
                            id: any_calendari + '_' + nivell_id + '_' + dia,
                            title: '@Lang('Banned')',
                            start: dia,
                            className: 'banned',
                            estado: 'banned',
                            any: any_calendari,
                            fecha: dia,
                            resourceId: nivell_id,
                        });
                    }
                }
            });
        });
    });
    
    $('#calendar').fullCalendar('removeEvents', function(event) {
        var trobat = false;
        $.each(eventos_nuevos_del, function(index, id) {
            if ( event.id === id ) trobat = true;
        });
        return trobat;
    });
    
    if ( add_or_remove == 'add' ) {
        $('#calendar').fullCalendar('renderEvents', eventos_nuevos, true);
    }
    $('#myModal').modal('hide');
}
</script>
@endsection