@extends('layouts.registre_login')

@section('content')
<section class="area-container">
	<div class="area-content">
		<h2>@Lang('Private area')</h2>

		<form class="form-horizontal" method="POST" action="{{ route('password.email') }}">
			{{ csrf_field() }}

			<img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" alt="BATALLE-INTRANET-logo-VERMELL">
			<h3>@Lang('Forgot Your Password?')</h3>

			@if (session('status'))
				<div class="alert alert-success" role="alert" style="color: green;padding: 0px 0px 15px 0px;">
					{{ __(session('status')) }}
				</div>
			@endif

			<div class="col-md-6">
				<input type="email" name="email" id="email" class="frm-ctrl" placeholder="" value="@Lang('Email')" onBlur="if (this.value == ''){this.value = '@Lang('Email')'; }" onFocus="if (this.value == '@Lang('Email')') {this.value = ''; }" required>

				@if ($errors->has('email'))
				<div class="alert alert-warning" role="alert" style="color: red;padding: 15px 0px 0px 0px;">
					<span class="help-block">
						<strong>{{ __($errors->first('email')) }}</strong>
					</span>
				</div>
				@endif
			</div>

			<div class="form-group">
				<div class="col-md-6 col-md-offset-4">
					<button type="submit" class="btn btn-primary btn-raised">
						@Lang('Reset the password')
					</button>
				</div>
			</div>
		</form>

	</div>
</section>
@endsection
