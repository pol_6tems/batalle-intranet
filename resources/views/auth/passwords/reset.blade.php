@extends('layouts.registre_login')

@section('content')
<section class="area-container">
	<div class="area-content">
		<h2>@Lang('Private area')</h2>
		
		<form class="form-horizontal" method="POST" action="{{ route('password.request') }}">
			
			{{ csrf_field() }}
			<input type="hidden" name="token" value="{{ $token }}">

			<img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" alt="BATALLE-INTRANET-logo-VERMELL">
			
			<h3>@Lang('Reset the password')</h3>
			
			@if (session('message'))
				<div class="alert alert-info">{{ session('message') }}</div>
				<br/>
			@endif

			<div class="col-md-6">
				{--<input type="dni" name="dni" id="dni" class="frm-ctrl form-control" placeholder="" value="@Lang('ID card')" onBlur="if (this.value == ''){this.value = '@Lang('ID card')'; }" onFocus="if (this.value == '@Lang('ID card')') {this.value = ''; }" required autofocus>--}
				<input type="email" name="email" id="email" class="frm-ctrl form-control" placeholder="" value="{{ $email or old('email') }}" onBlur="if (this.value == ''){this.value = '@Lang('Email')'; }" onFocus="if (this.value == '@Lang('Email')') {this.value = ''; }" required autofocus>

				@if ($errors->has('email'))
				<div class="alert alert-warning" role="alert">
					<span class="help-block">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
				</div>
				@endif
			</div>

			<input id="password" type="password" class="frm-ctrl" name="password" placeholder="@Lang('Password')" required>
			@if ($errors->has('password'))
			<div class="alert alert-warning" role="alert">
				<span class="help-block">
					<strong>{{ __($errors->first('password')) }}</strong>
				</span>
			</div>
			@endif
			
			<input id="password-confirm" type="password" class="frm-ctrl" name="password_confirmation" placeholder="@Lang('Repeat password')" required>
			@if ($errors->has('password_confirmation'))
			<div class="alert alert-warning" role="alert">
				<span class="help-block">
					<strong>{{ __($errors->first('password_confirmation')) }}</strong>
				</span>
			</div>
			@endif

			<div class="area-style">
				<select onchange='window.location.href = "{{ url('/locale') }}/" + this.value;'>
					@foreach ($_languages as $language)
                        <option value="{{$language->code}}" {{$language->code == app()->getLocale() ? 'selected' : ''}}>@lang($language->name)</option>
                    @endforeach
				</select>
			</div>
			
			<button type="submit" class="mt10">@Lang('Reset the password')</button>
			
			<div class="clear"></div>
			
			<p class="mt-10">@lang('acces-footer', ['url' => url('/politica-de-privacitat')])</p>
		</form>
	</div>
</section>
@endsection