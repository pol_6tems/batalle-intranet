@extends('layouts.registre_login')

@section('content')
<section class="area-container">
	<div class="area-content">
		<h2>@Lang('Private area')</h2>
		<form class="form-horizontal" method="POST" action="{{ route('login') }}">
			{{ csrf_field() }}
			<img src="{{asset('images/BATALLE-INTRANET-logo-VERMELL.png')}}" alt="BATALLE-INTRANET-logo-VERMELL">
			<h3>@Lang('Login')</h3>
			@if (session('message'))
				<div class="alert alert-info">{{ session('message') }}</div>
				<br/>
			@endif
			<div class="col-md-6">
				<!--<input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>-->
				<input type="text" name="dni" id="dni" class="frm-ctrl" placeholder="" value="@Lang('ID card')" onBlur="if (this.value == ''){this.value = '@Lang('ID card')'; }" onFocus="if (this.value == '@Lang('ID card')') {this.value = ''; }" required>

				@if ($errors->has('dni'))
				<div class="alert alert-warning" role="alert">
					<span class="help-block">
						<strong>{{ __($errors->first('dni')) }}</strong>
					</span>
				</div>
				@endif
			</div>

			<input id="password" type="password" class="frm-ctrl" name="password" placeholder="@Lang('Access key')" required>
			@if ($errors->has('password'))
			<div class="alert alert-warning" role="alert">
				<span class="help-block">
					<strong>{{ __($errors->first('password')) }}</strong>
				</span>
			</div>
			@endif

			<div class="area-style">
				<select onchange='window.location.href = "{{ url('/locale') }}/" + this.value;'>
					@foreach ($_languages as $language)
                        <option value="{{$language->code}}" {{$language->code == app()->getLocale() ? 'selected' : ''}}>@lang($language->name)</option>
                    @endforeach
				</select>
			</div>
			<div class="checkbox">
				<input type="checkbox" id="check1" name="remember" {{ old('remember') ? 'checked' : '' }}>
				<label for="check1">@Lang('Remember me')</label>
				<a href="{{ route('password.request') }}" class="has-resTxt">@Lang('Forgot Your Password?')</a>
			</div>			
			<button type="submit" class="mt10">@Lang('Enter')</button>
			<div class="clear"></div>
			<p class="mt-10">@lang('acces-footer', ['url' => url('/politica-de-privacitat')])</p>
		</form>
	</div>
</section>
@endsection